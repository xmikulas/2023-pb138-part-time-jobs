/*
  Warnings:

  - You are about to drop the column `wage` on the `Offering` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Offering" DROP COLUMN "wage";
