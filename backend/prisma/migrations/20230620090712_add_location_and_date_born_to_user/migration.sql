/*
  Warnings:

  - Added the required column `dateBorn` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `location` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "User" ADD COLUMN     "dateBorn" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "location" TEXT NOT NULL;
