import { Role } from "@prisma/client";

export type ApiResponse<T> = {
    status: 'success';
    data: T;
    message: string;
} | {
    status: 'failure',
    data: T,
    error: string;
};

declare module 'express-session'{
    interface SessionData { 
        user: {
            id: string,
            email: string,
            role: Role
        }
    }
}

export enum authType {
    ANONYMOUS,
    EMPLOYEE,
    EMPLOYER,
    ADMIN,
    SELF
}

export type Pagination = {
    skip: number;
    take: number;
  }