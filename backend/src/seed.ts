import { PrismaClient, Role, Stars } from '@prisma/client';

const prisma = new PrismaClient();

async function seed() {
    // Create users
    const user1 = await prisma.user.create({
    data: {
        name: 'John',
        surname: 'Doe',
        email: 'john@example.com',
        number: '1234567890',
        location: 'Praha',
        dateBorn: '1984-11-18T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        about: 'I am an experienced professional.',
        role: Role.EMPLOYEE,
    },
    });

    const user2 = await prisma.user.create({
    data: {
        name: 'Jane',
        surname: 'Smith',
        email: 'jane@example.com',
        number: '0987654321',
        location: 'Vienna',
        dateBorn: '1999-08-20T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        about: 'Looking for part-time work.',
        role: Role.EMPLOYEE,
    },
    });

    const user3 = await prisma.user.create({
    data: {
        name: 'David',
        surname: 'Johnson',
        email: 'david@example.com',
        number: '5555555555',
        location: 'Brno',
        dateBorn: '1993-06-04T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        about: 'Experienced employer seeking skilled professionals.',
        role: Role.EMPLOYER,
    },
    });

    // Create jobs
    const job1 = await prisma.job.create({
    data: {
        employerId: user3.id,
        title: 'Software Developer',
        description: 'Developing web applications using modern technologies.',
        wage: 5000,
        location: 'Brno',
        numOfHour: 40,
    },
    });

    const job2 = await prisma.job.create({
    data: {
        employerId: user3.id,
        title: 'Graphic Designer',
        description: 'Creating visually appealing designs for marketing campaigns.',
        wage: 3000,
        location: 'Office',
        numOfHour: 20,
    },
    });

    // Create job-to-user relationships
    await prisma.contract.create({
    data: {
        userId: user1.id,
        jobId: job1.id,
    },
    });

    await prisma.contract.create({
    data: {
        userId: user2.id,
        jobId: job2.id,
    },
    });

    // Create more sample data for other models
    const user4 = await prisma.user.create({
    data: {
        name: 'Sarah',
        surname: 'Johnson',
        email: 'sarah@example.com',
        number: '0987543210',
        location: 'Bratislava',
        dateBorn: '1996-05-25T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        about: 'Skilled employee with a passion for customer service.',
        role: Role.EMPLOYEE,
    },
    });

    const job3 = await prisma.job.create({
    data: {
        employerId: user3.id,
        title: 'Marketing Specialist',
        description: 'Planning and executing marketing strategies.',
        wage: 4000,
        location: 'Office',
        numOfHour: 35,
    },
    });

    await prisma.contract.create({
    data: {
        userId: user4.id,
        jobId: job3.id,
    },
    });

    const job4 = await prisma.job.create({
    data: {
        employerId: user3.id,
        title: 'Sales Representative',
        description: 'Selling products and services to potential customers.',
        wage: 3500,
        location: 'Field',
        numOfHour: 30,
    },
    });

    await prisma.contract.create({
    data: {
        userId: user1.id,
        jobId: job4.id,
    },
    });

    //// FREE ENTITIES

    // creating free employees
    
    //user5
    const user5 = await prisma.user.create({
        data: {
        name: 'Istvan',
        surname: 'Fekete',
        email: 'fekete@gmail.com',
        number: '+36987654210',
        location: 'Bratislava',
        dateBorn: '1989-04-10T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        expectedHours: 40,
        expectedWage: 2000,
        about: 'Pokosim travnik ale aj navarim segedin, plateny chcem byt vo forintoch',
        role: Role.EMPLOYEE,
        },
    });

    //user6
    const user6 = await prisma.user.create({
        data: {
        name: 'Robert',
        surname: 'Fikus',
        email: 'robert@fikus.com',
        number: '+421987654210',
        location: 'Topolcany',
        dateBorn: '1964-10-15T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        expectedHours: 4,
        expectedWage: 2000000,
        about: 'Za 4h rozkradnem ze vsetko',
        role: Role.EMPLOYEE,
        },
    });

    //user7
    await prisma.user.create({
        data: {
        name: 'Meky',
        surname: 'Zbirka',
        email: 'zbirka@hotmail.com',
        number: '+421987654210',
        location: 'Bratislava',
        dateBorn: '1952-10-21T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        expectedHours: 2,
        expectedWage: 1500,
        about: 'Koncertujem si na svoju s class kupe',
        role: Role.EMPLOYEE,
        },
    });

    //user8
    await prisma.user.create({
    data: {
        name: 'Magda',
        surname: 'suseda',
        email: 'cojeto@email.com',
        number: '+421987654210',
        location: 'Pezinok',
        dateBorn: '1954-03-23T00:00:00.000Z',
        password: '$2b$10$gcI9wxFG9Li2RPWMxC7DwOR4tStoU49YmB.BISW/s.pDsuHkNo.Be',
        expectedHours: 160,
        expectedWage: 10,
        about: 'Nemate cas si doma upatav? Nechajte to na mna!',
        role: Role.EMPLOYEE,
    },
  });


  // creating free jobs

  //job5
  await prisma.job.create({
    data: {
      employerId: user3.id,
      title: 'kosenie',
      description: 'Potrebujem pokosit travnik!',
      wage: 50,
      location: 'Bratislava',
      numOfHour: 2,
    },
  });

  //job6
  await prisma.job.create({
    data: {
      employerId: user3.id,
      title: 'Vodovod',
      description: 'Moja stara mama potrebuje opravit vodovodny kohutik',
      wage: 100,
      location: 'Kosice',
      numOfHour: 1,
    },
  });


    // offering1
    await prisma.offering.create({
        data: {
            userId: user6.id,
            title: 'Domy na kluc',
            description: 'Staviam domy na kluc uz 15 rokov. Moja partia ma 20r tradiciu, zacali sme ako partia elektrikarov.',
        }
    });

    // offering2
    await prisma.offering.create({
        data: {
            userId: user6.id,
            title: 'Vodovodne prace',
            description: 'Od malych oprav akymi su vodovodne kohutiky, az po celkove rekonstrukcie vody, odpadu a vrtanie studni.',
        }
    });

    // offering3
    await prisma.offering.create({
        data: {
            userId: user6.id,
            title: 'Murari',
            description: 'Vymurujeme vsetko, lacno, kvalitne a hlavne rychlo!',
        }
    });


    // offering4
    await prisma.offering.create({
        data: {
            userId: user5.id,
            title: 'React Native',
            description: 'I am an long years experienced in building React Native apps, curretly freelancing. Looking for interesting projects!',
        }
    });

    await prisma.review.create({
        data: {
            stars: Stars.FIVE,
            comment: 'One of the best guys I have met!',
            reviewerId: user3.id,
            reviewedId: user1.id
        }
    });

    await prisma.review.create({
        data: {
            stars: Stars.FIVE,
            comment: 'Always a pleasure to work with him',
            reviewerId: user3.id,
            reviewedId: user1.id
        }
    })
}



// now:
// user1 signed for job1, job4
// user2 signed for job2
// user3 created    job1, job2, job3, job4
// user4 signed for job3
// free users:      user7-user8
// free jobs:       job5, job6
// offers: user6 - of1, of2, of3
// offers: user5 - of4


seed()
  .catch((error) => {
    console.error(error);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
