import express from 'express';
import cors from 'cors';
import fileupload from 'express-fileupload'
import { config as configEnvVariables } from 'dotenv';
import { env } from 'process';
import type { ApiResponse } from './types/types';

import job from './routes/job';
import user from './routes/user';
import sign from './routes/sign';
import files from './routes/files'
import auth from './routes/auth';
import review from './routes/review';
import offer from './routes/offer';
import contract from './routes/contract';
import message from './routes/message';

import session from 'express-session';

const cookieParser = require('cookie-parser')

configEnvVariables();
const app = express();
const port = env.PORT ?? 3000;

// CORS middlware
app.use(cors({ credentials: true, origin: true }));

// JSON middleware
app.use(express.json());

// parse URL encoded strings
app.use(express.urlencoded({ extended: true }));

// parse cookies
app.use(cookieParser())

// uploading files
app.use(fileupload({createParentPath: true}))

// creating session 
app.use(session({
    secret: "coffee with butter",
    resave: false,
    cookie: {
        secure: false,
        httpOnly: false
    },
    saveUninitialized: true
}));

app.use(function(_, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:5173');
    //res.header('Access-Control-Allow-Origin', 'http://localhost:5174');
    res.header('Access-Control-Allow-Credentials', "true");
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });
// DO NOT MODIFY THE PRECEDING code ^^

app.use('/', job);
app.use('/', user);
app.use('/', sign);
app.use('/', files);
app.use('/', auth);
app.use('/', review);
app.use('/', offer);
app.use('/', contract);
app.use('/', message);


// DO NOT MODIFY THE FOLLOWING code:

// No route was taken - 404 - Resource (API endpoint) not found.
app.use((_req, res) => {
  const response: ApiResponse<{}> = {
    status: 'failure',
    data: {},
    error: 'No matching endpoint was found.',
  };

  return res.status(404).send(response);
});

if (env.NODE_ENV !== 'test') {
  app.listen(port, () => {
    console.log(
      `[${new Date().toISOString()}] RESTful API for this project on port ${port}`,
    );
  });
}

export default app;
