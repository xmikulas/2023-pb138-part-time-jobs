import { filterUserSchema, userCreateSchema, userFilterBodySchema, userIdSchema, userUpdateSchema } from '../schemas/userValidationSchemas';
import { MultipleUserReadData, UserCreateData, UserDeleteData, UserReadData, UserUpdateData } from '../repository/types/user';
import type { Request, Response } from 'express';
import create from '../repository/user/create';
import read from '../repository/user/read';
import update from '../repository/user/update';
import deleteUser from '../repository/user/delete';


const bcrypt = require('bcrypt');

export const createUserController = async (req: Request, res: Response) => {
    const userCreateData: UserCreateData = req.body;
    const result = userCreateSchema.safeParse(userCreateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    const {password, ...userdata} = result.data;
  
    return bcrypt.hash(password, 10, async function(err: Error, hash: string) {
      if (err !== undefined) {
          return res.status(400).send(err.message);
      }
      const user = await create({...userdata, password: hash});
      if (user.isErr) {
        return res.status(404).send({
          error: 'Database has returned an error'
        });
      }
      return res.status(201).send({ data: { ...user.value } });
  });
  
  }
  
  
  export const getSpecificUserController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
  
    const userReadData: UserReadData = {
      id: req.params.id,
    };
  
    const result = userIdSchema.safeParse(userReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const user = await read.one(userReadData);
    if (user.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({...user.value });
  }
  
  
  export const updateUserController = async (req: Request, res: Response) => {
    const userUpdateData: UserUpdateData = req.body;
    const result = userUpdateSchema.safeParse(userUpdateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    if (result.data.password) {
      const {password, ...userdata} = result.data;
      return bcrypt.hash(password, 10, async function(err: Error, hash: string) {
        if (err !== undefined) {
            return res.status(400).send(err.message);
        }
        const user = await update({...userdata, password: hash});
        
        if (user.isErr) {
            return res.status(404).send({
            error: 'Database has returned an error'
            });
        }
  
        return res.status(200).send({ data: { ...user.value } });
      });
    } else {
      const user = await update({...result.data});
        
      if (user.isErr) {
        return res.status(404).send({
        error: 'Database has returned an error'
         });
      }
      
      return res.status(200).send({ data: { ...user.value } });
    }
  }
  
  
  export const deleteUserController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
  
    const userDeleteData: UserDeleteData = {
      id: req.params.id,
    };
  
    const result = userIdSchema.safeParse(userDeleteData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const user = await deleteUser(userDeleteData);
    if (user.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ data: { ...user.value } });
  }
  
  
  export const filterIndependentUserController = async (req: Request, res: Response) => {
    const multipleUserReadData: MultipleUserReadData = req.query;
  
  
    const result = filterUserSchema.safeParse(multipleUserReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const users = await read.all(result.data, {skip: result.data.skip, take: result.data.take});
    if (users.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ ...users.value });
  }
  
  
  
  export const filterReactingUsersController = async (req: Request, res: Response) => {
  
    const result = await userFilterBodySchema.safeParseAsync({...req.params, ...req.query});
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const users = await read.reactions({ jobId: result.data.id }, {skip: result.data.skip, take: result.data.take});
    
    if (users.isErr) {
      return res.status(404).send({
        error: 'Record not found'
      });
    }
    return res.status(200).send({ data: { ...users.value } });
  }