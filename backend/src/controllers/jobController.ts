import { jobCreateSchema, jobIdSchema, jobUpdateSchema, jobsFilterSchema} from '../schemas/jobValidationSchemas'
import { JobCreateData, JobDeleteData, JobMultipleReadData, JobReadData, JobUpdateData } from '../repository/types/job';
import create from '../repository/job/create';
import read from '../repository/job/read';
import userReadModule from '../repository/user/read';
import update from '../repository/job/update';
import { deleteJob } from '../repository/job/delete';
import type { Request, Response } from 'express';



export const createJobController = async (req: Request, res: Response) => {
    const jobCreateData: JobCreateData = req.body;
    const result = jobCreateSchema.safeParse(jobCreateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    // only EMPLOYER can create jobs
    const employer = await userReadModule.one({id: jobCreateData.employerId})
  
    if (employer === null || employer.isErr) {
      return res.status(404).send({
        error: 'User does not exist'
      });
    }
  
    if (employer.value.role != "EMPLOYER") {
      return res.status(404).send({
        error: 'EMPLOYEE cannot create jobs'
      });
    }
  
  
    const job = await create(jobCreateData);
    if (job.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(201).send({ data: { ...job.value } });
  }
  
  
  export const getSpecificJobController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
  
    const jobReadData: JobReadData = {
      id: req.params.id,
    };
  
    const result = jobIdSchema.safeParse(jobReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const job = await read.one(jobReadData);
    if (job.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({ data: { ...job.value } });
  }
  
  
  export const updateJobController = async (req: Request, res: Response) => {
    const jobUpdateData: JobUpdateData =  {
        id: req.params.id,
        ...req.body
      };
    const result = jobUpdateSchema.safeParse(jobUpdateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    const job = await update(jobUpdateData);
    if (job.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ data: { ...job.value } });
  }
  
  
  export const deletesJobController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
  
    const jobDeleteData: JobDeleteData = {
      id: req.params.id,
    };
  
    const result = jobIdSchema.safeParse(jobDeleteData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const job = await deleteJob(jobDeleteData);
    if (job.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({ data: { ...job.value } });
  }
  
  
  export const readJobsController = async (req: Request, res: Response) => {
    const jobMultipleReadData: JobMultipleReadData = req.query;

    const result = jobsFilterSchema.safeParse(jobMultipleReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    const jobs = await read.all(result.data, {skip: result.data.skip, take: result.data.take});
  
    if (jobs.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ ...jobs.value });
  }