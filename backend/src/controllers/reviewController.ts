import { ReviewCreateData, ReviewDeleteData, ReviewReadMultipleData } from "../repository/types/review";
import { reviewCreateSchema, reviewDeleteSchema, reviewReadMultipleSchema, reviewReadSpecificSchema } from "../schemas/reviewValidationSchemas";
import { Request , Response} from 'express';
import userRead from "../repository/user/read";
import create from "../repository/review/create";
import read from "../repository/review/read";
import { deleteReview } from "../repository/review/delete";


export const createReviewController = async (req: Request, res: Response) => {
    const reviewCreateData: ReviewCreateData = req.body;
    const result = reviewCreateSchema.safeParse(reviewCreateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
    // check if reviewer is EMPLOYER
    const reviewer = await userRead.one({id: reviewCreateData.reviewerId})
  
    if (reviewer === null || reviewer.isErr) {
      return res.status(404).send({
        error: 'Reviewer does not exist'
      });
    }
  
    if (reviewer.value.role != "EMPLOYER") {
      return res.status(404).send({
        error: 'EMPLOYEE cannot create reviews'
      });
    }

    // check if reviewed is EMPLOYEE
    const reviewed = await userRead.one({id: reviewCreateData.reviewedId})
  
    if (reviewed === null || reviewed.isErr) {
      return res.status(404).send({
        error: 'Reviewed does not exist'
      });
    }
  
    if (reviewed.value.role != "EMPLOYEE") {
      return res.status(404).send({
        error: 'EMPLOYER cannot be reviewed'
      });
    }
  
    const review = await create(reviewCreateData);
  
    if (review.isErr) {
        return res.status(404).send({
          error: 'Database has returned an error'
        });
      }
    return res.status(200).send({ data: { ...review.value } });
}


export const getSpecificReviewController = async (req: Request, res: Response) => {    
    const result = reviewReadSpecificSchema.safeParse( {
        reviewedId: req.query.reviewedId,
        reviewerId: req.query.reviewerId
    });
    
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    
    const review = await read.one(result.data);
    if (review.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }

    return res.status(200).send({ data: { ...review.value } });
}

export const getMultipleReviewController = async (req: Request, res: Response) => {
    const reviewMultipleReadData: ReviewReadMultipleData = req.query;
    
    const result = reviewReadMultipleSchema.safeParse(reviewMultipleReadData);
        
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    
    const review = await read.all(reviewMultipleReadData);
    if (review.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }

    return res.status(200).send({...review.value });
}

export const deleteReviewController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
        return res.status(404).send('ID arg missing');
    }

    if (req.params.userId === undefined) {
        return res.status(404).send('UserID arg missing');
    }

    const reviewDeleteData: ReviewDeleteData = {
        id: req.params.id,
        userId: req.params.userId
    }

    const result = reviewDeleteSchema.safeParse(reviewDeleteData);
        
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    
    const review = await deleteReview(reviewDeleteData);
    if (review.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }

    return res.status(200).send({ data: { ...review.value } });
}
