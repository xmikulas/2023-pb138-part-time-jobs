import type { Request, Response } from "express";
import fs from "fs";
import path from "path";
import { documentSchema } from "../schemas/fileControllerSchema";
import { DocumentType } from "../repository/types/file";

const dotenv = require("dotenv").config().parsed;

const profileCvsPath = dotenv.CV_PATH;
const profilePicsPath = dotenv.PIC_PATH;

export const postFileController = async (req: Request, res: Response) => {
  const result = documentSchema.safeParse(req.params);
  if (!result.success) {
    return res.status(400).send({
      error: "Invalid request body",
      errorDesc: result.error.issues,
    });
  }

  const profileDoc = Array.isArray(req.files?.document)
    ? req.files?.document[0]
    : req.files?.document;

  if (profileDoc) {
    var rootPath = profilePicsPath;
    if (result.data.docType === DocumentType.CV) {
      rootPath = profileCvsPath;
    }

    profileDoc.mv(
      path.join(rootPath, result.data.userId, profileDoc.name),
      (err: any) => {
        if (err) {
          res.status(500).send({
            error: "Cannot upload profile document",
          });
          return;
        } else {
          res.status(200).send("ok");
        }
      }
    );
    removeOldDocument(
      result.data.userId,
      profileDoc.name,
      result.data.docType as DocumentType
    );
    return;
  } else {
    return res.status(400).send({ error: "No document" });
  }
};

export const deleteFileController = async (req: Request, res: Response) => {
  const result = documentSchema.safeParse(req.params);
  if (!result.success) {
    return res.status(400).send({
      error: "Invalid request body",
      errorDesc: result.error.issues,
    });
  }

  var rootPath = profilePicsPath;
  if (result.data.docType === DocumentType.CV) {
    rootPath = profileCvsPath;
  }

  fs.rm(path.join(rootPath, result.data.userId), { recursive: true }, (err) => {
    if (err) {
      res.status(500).send({
        error: "Cannot delete profile document",
        errorDesc: err.message,
      });
    } else {
      res.status(200).send("ok");
    }
  });
  return;
};

export const getFileController = async (req: Request, res: Response) => {
  const result = documentSchema.safeParse(req.params);
  if (!result.success) {
    return res.status(400).send({
      error: "Invalid request body",
      errorDesc: result.error.issues,
    });
  }

  var rootPath = profilePicsPath;
  if (result.data.docType === DocumentType.CV) {
    rootPath = profileCvsPath;
  }

  const profileDocumentPath = path.join(rootPath, result.data.userId);
  if (!fs.existsSync(profileDocumentPath)) {
    return res.status(404).send({
      error: "Profile document not found",
    });
  }

  const files = fs.readdirSync(profileDocumentPath);

  if (files.length === 0) {
    return res.status(404).send({
      error: "Profile document not found",
    });
  }

  const profileDocumentName = files[0];

  const profileFilePath = path.join(profileDocumentPath, profileDocumentName!);

  fs.readFile(path.join(profileFilePath), (err, data) => {
    if (err) {
      res.status(404).send("Document not found");
    } else {
      res.writeHead(200, {
        "Content-Type": getFileType(profileFilePath),
      });
      res.end(data);
    }
  });
  return;
};

const getFileType = (filePath: string) => {
  const extension = path.extname(filePath).toLowerCase();
  if (extension === ".jpg" || extension === ".jpeg") {
    return "image/jpeg";
  }
  if (extension === ".png") {
    return "image/png";
  }
  return "application/pdf";
};

const removeOldDocument = (
  id: string,
  newDocName: string,
  documentType: DocumentType
) => {
  var rootPath = profilePicsPath;
  if (documentType === DocumentType.CV) {
    rootPath = profileCvsPath;
  }
  const files = fs.readdirSync(path.join(rootPath, id));
  for (const file of files) {
    if (file !== newDocName) {
      fs.unlinkSync(path.join(rootPath, id, file));
    }
  }
};
