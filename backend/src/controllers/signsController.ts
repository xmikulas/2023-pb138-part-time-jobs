import create from '../repository/contract/create';
import read from '../repository/contract/read';
import readUser from '../repository/user/read';

import type { Request, Response } from 'express';
import { ContractCreateData } from '../repository/types/contract';
import { signFilterSchema, userSignSchema } from '../schemas/signValidationSchemas';


export const userSignController = async (req: Request, res: Response) => {
    const reactData: ContractCreateData = req.body;  
    const result = userSignSchema.safeParse(reactData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    // check if user is EMPLOYEE
    const user = await readUser.one({id: reactData.userId})
  
    if (user === null || user.isErr) {
      return res.status(404).send({
        error: 'User does not exist'
      });
    }
  
    if (user.value.role != "EMPLOYEE") {
      return res.status(404).send({
        error: 'EMPLOYER cannot take jobs'
      });
    }
  
    const reaction = await create(reactData);
  
    if (reaction.isErr) {
        return res.status(404).send({
          error: 'Database has returned an error'
        });
      }
    return res.status(200).send({ data: { ...reaction.value } });
  }
  
  
  
  export const signedUsersController = async (req: Request, res: Response) => {

    const result = await signFilterSchema.safeParseAsync({...req.params, ...req.query});
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const reaction = await read.all({userId: result.data.userId}, {skip: result.data.skip, take: result.data.take});
  
    if (reaction.isErr) {
        return res.status(404).send({
          error: 'Database has returned an error'
        });
      }
    return res.status(200).send({ ...reaction.value });
  
  }