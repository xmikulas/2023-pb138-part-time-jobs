import { LoginData, VerifyPasswordData } from '../repository/types/auth';
import { Request, Response } from 'express';
import client from '../client';
import read from '../repository/user/read';
import { loginSchema, verifySchema } from '../schemas/authValidationSchemas'

const bcrypt = require('bcrypt');

export const loginController = async (req: Request, res: Response) => {
    const loginData: LoginData = req.body;
    const result = loginSchema.safeParse(loginData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    const user = await client.user.findUnique({ where: { email: loginData.email}})
    if (!user) {
        res.status(404).send("User not found");
        return;
    }

    return bcrypt.compare(loginData.password, user.password, function(err: Error, result: boolean) {
        if (err !== undefined) {
            return res.status(400).send(err.message);
        }
        if (!result) {
            return res.status(401).send("Wrong password");
        }

        req.session.user = {
            id: user.id,
            email: user.email,
            role: user.role
        }
        return res.status(200).send("Login successful");
    });
}


export const logoutController = async (req: Request, res: Response) => {
    req.session.destroy(() => {});
    return res.status(200).send("Logged out");
}

export const loggedProfileController = async (req: Request, res: Response) => {
    if (!req.session.user) {
        return res.status(400).send("No user signed in");
    }
    const user = await read.one({ id: req.session.user.id });
    if (user.isErr) {
        return res.status(400).send("DB failed");
    }
    return res.status(200).send({ data: { ...user.value } });
}


export const verifyLoggedUserController = async (req: Request, res: Response) => {
    if (!req.session.user) {
        return res.status(400).send("No user signed in");
    }
    const user = await client.user.findUnique({ where: { id: req.session.user.id}})
    if (!user) {
        res.status(404).send("User not found");
        return;
    }

    const verifyData: VerifyPasswordData = req.body;
    const result = verifySchema.safeParse(verifyData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }

    return bcrypt.compare(verifyData.password, user.password, function(err: Error, result: boolean) {
        if (err !== undefined) {
            return res.status(400).send(err.message);
        }
        if (!result) {
            return res.status(401).send("Wrong password");
        }

        return res.status(200).send("Password verified");
    });
} 