import type { Request, Response } from 'express';
import { OfferingCreateData, OfferingDeleteData, OfferingMultipleReadData, OfferingReadData, OfferingUpdateData } from '../repository/types/offer';
import { offerCreateSchema, offerDeleteSchema, offerMultipleReadSchema, offerReadSchema, offerUpdateSchema } from '../schemas/offerValidationSchema';
import userRead from '../repository/user/read';
import create from '../repository/offer/create';
import read from '../repository/offer/read';
import { deleteOffering } from '../repository/offer/delete';
import update from '../repository/offer/update';


export const createOfferController = async (req: Request, res: Response) => {
    const offerCreateData: OfferingCreateData = req.body;
    const result = offerCreateSchema.safeParse(offerCreateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    // only EMPLOYEE can create offers
    const employee = await userRead.one({id: offerCreateData.userId})
  
    if (employee === null || employee.isErr) {
      return res.status(404).send({
        error: 'User does not exist'
      });
    }
  
    if (employee.value.role != "EMPLOYEE") {
      return res.status(404).send({
        error: 'EMPLOYER cannot create offers'
      });
    }
  
  
    const offer = await create(offerCreateData);
    if (offer.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(201).send({ data: { ...offer.value } });
}
  
  
export const getSpecificOfferController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
  
    const offerReadData: OfferingReadData = {
      offeringId: req.params.id,
    };
  
    const result = offerReadSchema.safeParse(offerReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const offer = await read.one(offerReadData);
    if (offer.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({ data: { ...offer.value } });
}
  

export const deleteOfferController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
    if (req.params.userId === undefined) {
        return res.status(404).send('User ID arg missing');
    }
  
    const offerDeleteData: OfferingDeleteData = {
      offeringId: req.params.id,
      userId: req.params.userId
    };
  
    const result = offerDeleteSchema.safeParse(offerDeleteData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const offer = await deleteOffering(offerDeleteData);
    if (offer.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({ data: { ...offer.value } });
}


export const updateOfferController = async (req: Request, res: Response) => {
    const offerUpdateData: OfferingUpdateData =  req.body;

    const result = offerUpdateSchema.safeParse(offerUpdateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    const offer = await update(offerUpdateData);
    if (offer.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ data: { ...offer.value } });
  }
  
  
export const getMultipleOffersController = async (req: Request, res: Response) => {
    const offerMultipleReadData: OfferingMultipleReadData = req.query;

    const result = offerMultipleReadSchema.safeParse(offerMultipleReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    const offers = await read.all(result.data, {skip: result.data.skip, take: result.data.take});
  
    if (offers.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error',
      });
    }
    return res.status(200).send({ ...offers.value });
}