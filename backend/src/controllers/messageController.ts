import type { Request, Response } from 'express';
import { MessageCreateData, MessageMultipleReadData } from '../repository/types/message';
import { messageCreateSchema, messageReadSchema } from '../schemas/messageValidationSchema';
import create from '../repository/message/create';
import read from '../repository/message/read';



export const createMessageController = async (req: Request, res: Response) => {
    const messageCreateData: MessageCreateData = req.body;
    const result = messageCreateSchema.safeParse(messageCreateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    const message = await create(messageCreateData);
    if (message.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(201).send({ data: { ...message.value } });
}

export const getMessagesController = async (req: Request, res: Response) => {
    const messageReadData: MessageMultipleReadData = req.query;

    const result = messageReadSchema.safeParse(messageReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    const messages = await read.all(result.data, {skip: result.data.skip, take: result.data.take});
  
    if (messages.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ ...messages.value });
  }