import type { Request, Response } from 'express';
import { ContractCreateData, ContractDeleteData, ContractMultipleReadData, ContractReadByIdData, ContractReadData, ContractUpdateData } from '../repository/types/contract';
import { contractCreateSchema, contractDeleteSchema, contractMultipleReadSchema, contractReadByIdSchema, contractReadSchema, contractUpdateSchema } from '../schemas/contractValidationSchema';
import create from '../repository/contract/create';
import read from '../repository/contract/read';
import update from '../repository/contract/update';
import { deleteContract } from '../repository/contract/delete';


export const createContractController = async (req: Request, res: Response) => {
    const contractCreateData: ContractCreateData = req.body;
    const result = contractCreateSchema.safeParse(contractCreateData);

    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }

    const existingContract = await read.one(contractCreateData);
    if (existingContract.isOk) {
        return res.status(201).send({ ...existingContract.value });
    }

    const contract = await create(contractCreateData);
    if (contract.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(201).send({ ...contract.value });
  }

  export const getContractByIdController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
        return res.status(404).send('ID arg missing');
      }
    
      const contractReadData: ContractReadByIdData = {
        id: req.params.id,
      };
    
      const result = contractReadByIdSchema.safeParse(contractReadData);
    
      if (!result.success) {
        return res.status(400).send({
          error: 'Invalid argument',
          errorDesc: result.error.issues
        });
      }
    
      const contract = await read.byId(contractReadData);
      if (contract.isErr) {
        return res.status(404).send({
          error: 'Database has returned an error'
        });
      }
    
      return res.status(200).send({ data: { ...contract.value } });
}

export const getSpecificContractController = async (req: Request, res: Response) => {
    const contractReadData: ContractReadData = req.query;
    const result = contractReadSchema.safeParse(contractReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const contract = await read.one(contractReadData);
    if (contract.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({ data: { ...contract.value } });
}
  
  
export const updateContractController = async (req: Request, res: Response) => {
    const contractUpdateData: ContractUpdateData = req.body;

    const result = contractUpdateSchema.safeParse(contractUpdateData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid request body',
        errorDesc: result.error.issues
      });
    }
  
    const contract = await update(contractUpdateData);
    if (contract.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ data: { ...contract.value } });
}
  
  
export const deleteContractController = async (req: Request, res: Response) => {
    if (req.params.id === undefined) {
      return res.status(404).send('ID arg missing');
    }
  
    const contractDeleteData: ContractDeleteData = {
      id: req.params.id,
    };
  
    const result = contractDeleteSchema.safeParse(contractDeleteData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
  
    const contract = await deleteContract(contractDeleteData);
    if (contract.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
  
    return res.status(200).send({ data: { ...contract.value } });
}
  

export const readContractsController = async (req: Request, res: Response) => {
    const contractMultipleReadData: ContractMultipleReadData = req.query;

    const result = contractMultipleReadSchema.safeParse(contractMultipleReadData);
  
    if (!result.success) {
      return res.status(400).send({
        error: 'Invalid argument',
        errorDesc: result.error.issues
      });
    }
    const contracts = await read.all(result.data, {skip: result.data.skip, take: result.data.take});
  
    if (contracts.isErr) {
      return res.status(404).send({
        error: 'Database has returned an error'
      });
    }
    return res.status(200).send({ ...contracts.value });
}