import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { Contract } from "@prisma/client";
import { ContractCreateData } from "../types/contract";

/**
 * Create a repository call that creates a Contract.
 *
 * @param data object containing necessary data to create a new Contract record
 * @returns - On success: the created Contract record
 *          - On failure: a generic error
 */

const create = async (data: ContractCreateData): Promise<Result<Contract, Error>> => {
  try {
    const contract = await client.contract.create({
        data: {
            ...data
        },
      });
      return Result.ok(contract);
  } catch (e) {
    return genericError;
  }
};
  
export default create;