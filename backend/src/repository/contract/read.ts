import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { ContractReadByIdData, ContractReadData } from "../types/contract";
import { Contract, Prisma } from "@prisma/client";
import { PaginationOptions, multipleReadResponse } from "../types/common";

/**
 * Read repository call that finds a job.
 *
 * @param data object containing necessary data to find a Contract record
 * @returns - On success: Contract record
 *          - On failure: a generic error
 */

const specific = async (data: ContractReadData): Promise<Result<Contract, Error>> => {
  try {
    const contract = await client.contract.findFirst({
        where: {
            userId: data.userId,
            jobId: data.jobId,
            offerId: data.offerId
        },
        select: {
            id: true,
            userId: true,
            jobId: true,
            offerId: true,
            createdAt: true,
            settledHours: true,
            settledWage: true,
            approvedEmployer: true,
            approvedEmployee: true,
        }
    });
    if (contract === null) {
        throw new Error('The specified relation does not exist!');
    }
    return Result.ok(contract);
  } catch (e) {
    return genericError;
  }
};


// get a single contract by its ID value
const byId = async (data: ContractReadByIdData): Promise<Result<Contract, Error>> => {
    try {
        const contract = await client.contract.findFirst({
            where: {
                id: data.id
            },
            select: {
                id: true,
                userId: true,
                jobId: true,
                offerId: true,
                createdAt: true,
                settledHours: true,
                settledWage: true,
                approvedEmployer: true,
                approvedEmployee: true,
                user: true,
                offer: true,
            },
        });
        if (contract === null) {
            throw new Error('The specified relation does not exist!');
        }
        return Result.ok(contract);
      } catch (e) {
        return genericError;
      }
};


//make multiple read statement
const multiple = async (data: ContractReadData,
                        paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<Contract>, Error>> => {
  try {
    const { skip, take } = paginationOptions;
    const where: Prisma.ContractWhereInput = {
        jobId: data.jobId,
        offerId: data.offerId
    }
    const [count, contract] = await client.$transaction([
        client.contract.count({where}),
        client.contract.findMany({
        where, 
        select: {
          id: true,
          userId: true,
          jobId: true,
          offerId: true,
          createdAt: true,
          settledHours: true,
          settledWage: true,
          approvedEmployer: true,
          approvedEmployee: true,
            user: {
              select: {
                id: true,
                name: true,
                surname: true,
                email: true,
                number: true,
                about: true,
                createdAt: true,
                expectedHours: true,
                expectedWage: true,
              },
            },
        },
        skip,
        take,
    })])
    if (contract === null) {
        throw new Error('The specified relation does not exist!');
    }
    return Result.ok({array: contract, count: count});
  } catch (e) {
    if (e instanceof Error) {
      return Result.err(e);
    }
    return genericError;
  }
};




export default {
    all: multiple,
    one: specific,
    byId: byId
};
