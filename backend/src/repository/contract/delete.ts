import { Result } from "@badrap/result";
import client from "../../client";
import { genericError } from "../types";
import read from "./read";
import { ContractDeleteData, ContractMultipleDeleteData } from "../types/contract";
import { Contract } from "@prisma/client";

/**
 * Delete, a repository call that deletes a Contract.
 *
 * @param data object containing id to delete appropriate Contract record
 * @returns - On success: job record deletedAt is set to deletedAt Date
 *          - On failure: a generic error
 */

export const deleteContract = async (data: ContractDeleteData): Promise<Result<Contract, Error>> => {
    try {
        
        read.byId({id: data.id}) // throws error if nonexisting or deleted
        const deletedContract = await client.contract.delete({
            where: {
                id: data.id
            }
        });

        return Result.ok(deletedContract);
    } catch (e) {
        return genericError;
    }
};


export const deleteMultipleContract = async (data: ContractMultipleDeleteData): Promise<Result<number, Error>> => {
    try {
        
        read.one({userId: data.userId}) // throws error if nonexisting or deleted

        const deletedContract = await client.contract.deleteMany({
            where: {
                userId: data.userId
            }
        });

        return Result.ok(deletedContract.count);
    } catch (e) {
        return genericError;
    }
};
