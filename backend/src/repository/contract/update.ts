import { Result } from "@badrap/result";
import { ContractUpdateData } from "../types/contract";
import { Contract } from "@prisma/client";
import { genericError } from "../types";
import client from "../../client";
import read from "./read";


const update = async (data: ContractUpdateData): Promise<Result<Contract, Error>> => {
    try {
        await read.byId({id: data.id}) // throws error if nonexisting or deleted
        const updatedContract = await client.contract.update({
            where: {
              id: data.id,
            },
            data,
          });
        return Result.ok(updatedContract);
    } catch (e) {
        if (e instanceof Error) {
            return Result.err(e);
          }
          return genericError;
    }
}

export default update;