import { Result } from "@badrap/result";
import { JobUpdateData } from "../types/job";
import { Job } from "@prisma/client";
import { genericError } from "../types";
import client from "../../client";
import read from "./read";



const update = async (data: JobUpdateData): Promise<Result<Job, Error>> => {
    try {

        const job = await read.one({id: data.id})
        if (job.isErr || job === undefined) {
          throw new Error("Nonexisting or deleted");
        }
        if (job.value.employerId !== data.employerId) {
          throw new Error("Wrong job ownership");
        }
        
        const updatedJob = await client.job.update({
            where: {
              id: data.id,
            },
            data,
          });
        return Result.ok(updatedJob);
    } catch (e) {
        if (e instanceof Error) {
            return Result.err(e);
          }
          return genericError;
    }
}


export default update;