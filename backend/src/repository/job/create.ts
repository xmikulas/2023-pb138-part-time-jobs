import { genericError } from "../types";
import { Result } from '@badrap/result';
import { JobCreateData } from "../types/job";
import client from "../../client";
import { Job } from "@prisma/client";

/**
 * Create a repository call that creates a job.
 *
 * @param data object containing necessary data to create a new job record
 * @returns - On success: the created job record
 *          - On failure: a generic error
 */

const create = async (data: JobCreateData): Promise<Result<Job, Error>> => {
  try {
    const job = await client.job.create({
        data: {
          ...data
        },
    });
    return Result.ok(job);
  
  } catch (e) {
    if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
  }
};
  
export default create;
