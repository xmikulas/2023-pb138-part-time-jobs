// import { genericError } from "../types";
import { Result } from '@badrap/result';
import { JobMultipleReadData, JobReadData } from "../types/job";
import client from "../../client";
import { Job, Prisma } from "@prisma/client";

import { genericError } from '../types';
import { PaginationOptions, multipleReadResponse } from '../types/common';

/**
 * Read repository call that finds a job.
 *
 * @param data object containing necessary data to find a job record
 * @returns - On success: job record
 *          - On failure: a generic error
 */

const specific = async (data: JobReadData): Promise<Result<Job, Error>> => {
  try {
    const job = await client.job.findUnique({
        where: {
            id: data.id,
        },
        select: {
            id: true,
            employerId: true,
            title: true,
            description: true,
            wage: true,
            location: true,
            numOfHour: true,
            validTill: true,
            createdAt: true,
            editedAt: true,
            deletedAt: true
        }
    });
    if (job === null) {
        throw new Error('The specified job does not exist!');
    }
    if (job?.deletedAt != null) {
        throw new Error('The specified job has already been deleted!');
    }
    return Result.ok(job);
  } catch (e) {
    if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
  }
};



const multiple = async (data: JobMultipleReadData,
                        paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<Job>, Error>> => {
    try {
      const currentTime = new Date();
      const { skip, take } = paginationOptions;
      const where: Prisma.JobWhereInput = {
        deletedAt: null,
        OR: [
          {
            validTill: {
              gte: currentTime,
            },
          },
          {
            validTill: null,
          },
        ],
        employerId: {
          equals: data.employerId?.toString() || undefined,
        },
        wage: {
          gte: data.wageFrom,
          lte: data.wageTo
        },
        numOfHour: {
          gte: data.hoursFrom,
          lte: data.hoursTo
        },
        location: {
          contains: data.location?.toString() || undefined,
          mode: 'insensitive',
        },
        title: {
          contains: data.title?.toString() || undefined,
          mode: 'insensitive',
        },
        description: {
          contains: data.description?.toString() || undefined,
          mode: 'insensitive',
        }
      };
      const [count, jobs] = await client.$transaction([
        client.job.count({where}),
        client.job.findMany({
            where,
            select: {
                id: true,
                employerId: true,
                title: true,
                description: true,
                wage: true,
                location: true,
                numOfHour: true,
                validTill: true,
                createdAt: true,
                editedAt: true,
                deletedAt: true
            },
            skip,
            take,
        })
      ])
      if (jobs === null) {
        throw new Error('The specified jobs not exist!');
    }
      return Result.ok({array: jobs, count: count});
    } catch (e) {
      if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
    }
}


export default {
    all: multiple,
    one: specific
};
