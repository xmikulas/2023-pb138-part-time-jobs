import { Result } from "@badrap/result";
import { Job } from "@prisma/client";
import client from "../../client";
import { genericError } from "../types";
import { JobDeleteData, JobMultipleDeleteData } from "../types/job";
import read from "./read";

/**
 * Delete, a repository call that deletes a job.
 *
 * @param data object containing id to delete appropriate job record
 * @returns - On success: job record deletedAt is set to deletedAt Date
 *          - On failure: a generic error
 */

export const deleteJob = async (data: JobDeleteData): Promise<Result<Job, Error>> => {
    try {
        
        read.one({id: data.id}) // throws error if nonexisting or deleted

        const deletedAt = new Date();
        const updatedJob = await client.job.update({
            where: {
                id: data.id,
            },
            data: {
                deletedAt,
            }
        });

        return Result.ok(updatedJob);
    } catch (e) {
        if (e instanceof Error) {
            return Result.err(e);
          }
          return genericError;
    }
};


export const deleteMultipleJob = async (data: JobMultipleDeleteData): Promise<Result<number, Error>> => {
    try {
        const deletedAt = new Date();
        const updatedJob = await client.job.updateMany({
            where: {
                employerId: data.employerId
            },
            data: {
                deletedAt,
            }
        });

        return Result.ok(updatedJob.count);
    } catch (e) {
        if (e instanceof Error) {
            return Result.err(e);
          }
          return genericError;
    }
};
