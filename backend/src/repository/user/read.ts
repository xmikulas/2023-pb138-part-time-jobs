import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { Prisma, User } from "@prisma/client";
import { MultipleUserReadData, ReactedUserReadData, UserReadData, omitPassword } from "../types/user";
import { PaginationOptions, multipleReadResponse } from "../types/common";

/**
 * Read repository call that finds a job.
 *
 * @param data object containing necessary data to find a user record
 * @returns - On success: user record
 *          - On failure: a generic error
 */

const specific = async (data: UserReadData): Promise<Result<omitPassword<User>, Error>> => {
  try {
    const user = await client.user.findUnique({
        where: {
            id: data.id,
        },
        select: {
          id: true,
          name: true,
          surname: true,
          email: true,
          number: true,
          about: true,
          createdAt: true,
          deletedAt: true,
          role: true,
          dateBorn: true,
          location: true,
          expectedHours: true,
          expectedWage: true,
        }
    });
    if (user === null) {
        throw new Error('The specified user does not exist!');
    }
    if (user?.deletedAt != null) {
        throw new Error('The specified user has already been deleted!');
    }
    return Result.ok(user);
  } catch (e) {
    if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
  }
};


const reactions = async (data: ReactedUserReadData,
  paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<omitPassword<User>>, Error>> => {
    try {
      const { skip, take } = paginationOptions;
      const where: Prisma.UserWhereInput = {
        deletedAt: null,
        jobs: {
          some: {
            jobId: data.jobId,
          },
        }
      };

      const include: Prisma.UserInclude = {
        jobs: {
          where: {
            jobId: data.jobId,
          },
          orderBy: {
            createdAt: 'asc',
          },
        }
      };
      const [count, users] = await client.$transaction([
        client.user.count({where}),
        client.user.findMany({
            where,
            include,
            skip,
            take,
          })
      ]);
      if (users === null) {
          throw new Error('The specified user does not exist!');
      }
      return Result.ok({array: users, count: count});
    } catch (e) {
      if (e instanceof Error) {
          return Result.err(e);
        }
        return genericError;
    }
  };


const multiple = async (data: MultipleUserReadData,
                        paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<omitPassword<User>>, Error>> => {
try {
    const { skip, take } = paginationOptions;
    const where: Prisma.UserWhereInput = {
      deletedAt: null,
      expectedWage: {
          gte: data.wageFrom,
          lt: data.wageTo
      },
      expectedHours: {
          gte: data.hoursFrom,
          lt: data.hoursTo
      },
      location: {
        contains: data.location?.toString() || undefined,
        mode: 'insensitive',
      },
      name: {
        contains: data.name?.toString() || undefined,
        mode: 'insensitive',
      },
      role: {
        equals: 'EMPLOYEE'
      }
    }

    const [count, users] = await client.$transaction([
      client.user.count({where}),
      client.user.findMany({
        where,
        select: {
            id: true,
            name: true,
            surname: true,
            email: true,
            number: true,
            about: true,
            createdAt: true,
            deletedAt: true,
            role: true,
            dateBorn: true,
            location: true,
            expectedHours: true,
            expectedWage: true,
        },
        skip,
        take
      })
    ]);
    if (users === null) {
        throw new Error('The specified user does not exist!');
    }
    return Result.ok({array: users, count: count});
} catch (e) {
    if (e instanceof Error) {
        return Result.err(e);
    }
    return genericError;
}
};


export default {
    all: multiple,
    reactions: reactions,
    one: specific,
};