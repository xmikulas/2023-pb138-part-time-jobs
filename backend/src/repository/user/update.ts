import { Result } from "@badrap/result";
import { User } from "@prisma/client";
import { genericError } from "../types";
import client from "../../client";
import read from "./read";
import { UserUpdateData } from "../types/user";



const update = async (data: UserUpdateData): Promise<Result<User, Error>> => {
  try {

    read.one({id: data.id}) // throws error if nonexisting or deleted
    
    const updatedUser = await client.user.update({
      where: {
        id: data.id,
      },
      data,
    });
    return Result.ok(updatedUser);
  } catch (e) {
    if (e instanceof Error) {
      return Result.err(e);
    }
    return genericError;
  }
}

export default update;