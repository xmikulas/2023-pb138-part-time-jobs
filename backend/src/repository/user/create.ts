import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { User } from "@prisma/client";
import { UserCreateData } from "../types/user";

/**
 * Create a repository call that creates a user.
 *
 * @param data object containing necessary data to create a new user record
 * @returns - On success: the created user record
 *          - On failure: a generic error
 */

const create = async (data: UserCreateData): Promise<Result<User, Error>> => {
  try {
    const user = await client.user.create({
      data: data,
    });
    return Result.ok(user);
  } catch (e) {
    if (e instanceof Error) {
      return Result.err(e);
    }
    return genericError;
  }
};

export default create;