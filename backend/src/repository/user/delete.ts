import { Result } from "@badrap/result";
import { User } from "@prisma/client";
import client from "../../client";
import { genericError } from "../types";
import { UserDeleteData } from "../types/user";
import read from "./read";
import {deleteMultipleJob} from "../job/delete"
import { deleteMultipleContract } from "../contract/delete";


/**
 * Delete, a repository call that deletes a User.
 *
 * @param data object containing id to delete appropriate User record along with it's roles and posted Jobs
 * @returns - On success: User record deletedAt is set to deletedAt Date
 *          - On failure: a generic error
 */


const deleteUser = async (data: UserDeleteData): Promise<Result<User, Error>> => {
		try {
				read.one({id: data.id}) // throws error if User is already deleted or nonexistent

				const deletedAt = new Date();
				const updatedUser = await client.user.update({
						where: {
								id: data.id,
						},
						data: {
								deletedAt,
						}
				});

				deleteMultipleContract({userId: data.id})
				// if/when delete also the job
				deleteMultipleJob({employerId: data.id})

				return Result.ok(updatedUser);
		} catch (e) {
      if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
		}
	};
	
	export default deleteUser;