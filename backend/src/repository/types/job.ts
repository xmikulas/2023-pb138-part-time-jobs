export type JobReadData = {
    id: string;
    
};

export type JobUpdateData =
{
  id: string;
  employerId: string;
} & {
    description?: string;
    wage?: number;
    location?: string;
    numOfHour?: number;
    validTill?: Date;
}


export type JobCreateData = {
    employerId: string;
    title: string;
    description: string;
    wage: number;
    location: string;
    numOfHour: number;
    validTill?: Date;
};


export type JobDeleteData = {
    id: string;
  };


export type JobMultipleDeleteData = {
    employerId: string;
}


export type JobMultipleReadData = {
    wageFrom?: number;
    wageTo?: number;
    hoursFrom?: number;
    hoursTo?: number;
    location?: String;
    title?: String;
    description?: String;
    employerId?: String;
    skip?: number;
    take?: number;
}