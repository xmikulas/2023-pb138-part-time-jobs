
export type MessageCreateData = {
    senderId: string;
    contractId: string;
    content: string;
};

export type MessageMultipleReadData = {
    firstParticipantId?: string;
    secondParticipantId?: string;
    contractId?: string;
}