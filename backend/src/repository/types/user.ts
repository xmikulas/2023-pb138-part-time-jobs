import { Role } from "@prisma/client";



export type UserCreateData = {
    name: string;
    surname: string;
    email: string;
    number: string;
    about: string;
    role: Role;
    dateBorn: string;
    location: string;
    expectedHours?: number;
    expectedWage?: number;
    password: string;
};

export type UserDeleteData = {
    id: string;
};


export type UserUpdateData = {
    id: string;
  } & {
    name?: string;
    surname?: string;
    email?: string;
    number?: string;
    about?: string;
    role?: Role;
    password?: string;
    expectedHours?: number | null;
    expectedWage?: number | null;
    location?: string;
    dateBorn?: string;
};

export type UserCreateResult = {
    id: string;
}

export type UserReadData = {
    id: string;
}


export type MultipleUserReadData = {
    wageFrom?: number | undefined;
    wageTo?: number | undefined;
    hoursFrom?: number | undefined;
    hoursTo?: number | undefined;
    dateBorn?: Date;
    location?: string;
    name?: string;
    skip?: number | undefined;
    take?: number | undefined;
}


export type ReactedUserReadData = {
    jobId: string
}

export type omitPassword<T> = Omit<T ,'password'>