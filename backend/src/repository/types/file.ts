

export type Document = {
    userId: String;
    docType: DocumentType;
  };
  
export enum DocumentType {
    CV = "CV",
    PIC = "PIC",
  }