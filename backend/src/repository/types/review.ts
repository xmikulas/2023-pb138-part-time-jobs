import { Stars } from "@prisma/client";


export type ReviewCreateData = {
    reviewerId: string;
    reviewedId: string;
    comment: string;
    stars: Stars;
};


export type ReviewDeleteData = {
    id: string;
    userId: string;
};


export type ReviewReadMultipleData = {
    reviewerId?: string;
    reviewedId?: string;
    skip?: number;
    take?: number;
}

export type ReviewReadSpecificData = {
    reviewerId: string;
    reviewedId: string;
}