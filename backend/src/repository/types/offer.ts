export type OfferingCreateData = {
    userId: string;
    title: string;
    description: string;
};

export type OfferingMultipleReadData = {
    userId?: string;
}

export type OfferingDeleteData = {
    offeringId: string,
    userId: string
}

export type OfferingReadData = {
    offeringId: string
}

export type OfferingUpdateData = {
    userId: string;
    id: string;
    title?: string;
    description?: string;
}