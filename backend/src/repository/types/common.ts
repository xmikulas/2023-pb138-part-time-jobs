
export type PaginationOptions = {
    skip?: number;
    take?: number;
  }

export type multipleReadResponse<T> = {
    array: T[];
    count: number;
}