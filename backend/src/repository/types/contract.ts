export type ContractCreateData = {
    userId: string;
    jobId?: string;
    offerId?: string;
}

export type ContractReadData = {
    userId?: string;
    jobId?: string;
    offerId?: string;
}

export type ContractMultipleReadData = {
    jobId?: string;
    offerId?: string;
}

export type ContractReadByIdData = {
    id: string;
}

export type ContractDeleteData = {
    id: string;
}

export type ContractMultipleDeleteData = {
    userId: string;
}

export type ContractUpdateData = {
    id: string;
    settledHours?: number;
    settledWage?: number;
    approvedEmployer?: boolean;
    approvedEmployee?: boolean;
}