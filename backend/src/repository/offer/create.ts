import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { Offering } from "@prisma/client";
import { OfferingCreateData } from "../types/offer";

/**
 * Create a repository call that creates a Offering.
 *
 * @param data object containing necessary data to create a new Offering record
 * @returns - On success: the created Offering record
 *          - On failure: a generic error
 */

const create = async (data: OfferingCreateData): Promise<Result<Offering, Error>> => {
  try {
    const contract = await client.offering.create({
        data: {
            ...data
        },
      });
      return Result.ok(contract);
  } catch (e) {
    return genericError;
  }
};
  
export default create;