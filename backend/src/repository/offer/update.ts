import { Result } from "@badrap/result";
import { genericError } from "../types";
import client from "../../client";
import read from "./read";
import { OfferingUpdateData } from "../types/offer";
import { Offering } from "@prisma/client";



const update = async (data: OfferingUpdateData): Promise<Result<Offering, Error>> => {
    try {

        const offer = await read.one({offeringId: data.id})
        if (offer.isErr || offer === undefined) {
            throw new Error("Nonexisting or deleted offer");
        }
        if (offer.value.userId !== data.userId) {
            throw new Error("Wrong offer ownership");
        }
        
        const updatedOffer = await client.offering.update({
            where: {
              id: data.id,
            },
            data: {
                ...data,
                editedAt: new Date()},
          });
        return Result.ok(updatedOffer);
    } catch (e) {
        if (e instanceof Error) {
            return Result.err(e);
          }
          return genericError;
    }
}


export default update;