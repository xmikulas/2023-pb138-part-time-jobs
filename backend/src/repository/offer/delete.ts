import { Result } from "@badrap/result";
import client from "../../client";
import { genericError } from "../types";
import { OfferingDeleteData } from "../types/offer";
import { Offering } from "@prisma/client";

/**
 * Delete, a repository call that deletes a Offering.
 *
 * @param data object containing id to delete appropriate Offering record
 * @returns - On success: Offering record which was deleted
 *          - On failure: a generic error
 */

export const deleteOffering = async (data: OfferingDeleteData): Promise<Result<Offering, Error>> => {
    try {
        const deleted = await client.offering.delete({
            where: {
                id: data.offeringId,
            }
        });

        return Result.ok(deleted);
    } catch (e) {
        return genericError;
    }
};