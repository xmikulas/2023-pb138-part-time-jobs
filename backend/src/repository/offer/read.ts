import { Result } from '@badrap/result';
import { OfferingReadData, OfferingMultipleReadData } from "../types/offer";
import client from "../../client";
import { Offering, Prisma } from "@prisma/client";

import { genericError } from '../types';
import { PaginationOptions, multipleReadResponse } from '../types/common';

/**
 * Read repository call that finds a Offering.
 *
 * @param data object containing id to find a Offering record
 * @returns - On success: Offering record
 *          - On failure: a generic error
 */

const specific = async (data: OfferingReadData): Promise<Result<Offering, Error>> => {
  try {
    const offering = await client.offering.findUnique({
        where: {
            id: data.offeringId,
        },
        select: {
            id: true,
            title: true,
            description: true,
            userId: true,
            editedAt: true,
            deletedAt: true,
            user: true,
            contract: true
        }
    });
    if (offering === null) {
        throw new Error('The specified offering does not exist!');
    }
    if (offering?.deletedAt != null) {
        throw new Error('The specified offering has already been deleted!');
    }
    return Result.ok(offering);
  } catch (e) {
    if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
  }
};



const multiple = async (data: OfferingMultipleReadData,
                        paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<Offering>, Error>> => {
    try {
      const { skip, take } = paginationOptions;
      const where: Prisma.OfferingWhereInput = {
        deletedAt: null,
        userId: data.userId
      };
      const [count, offerings] = await client.$transaction([
        client.offering.count({where}),
        client.offering.findMany({
            where,
            select: {
                id: true,
                title: true,
                description: true,
                editedAt: true,
                deletedAt: true,
                userId: true,
                user: true,
                contract: {
                  select: {
                    id: true,
                    user: true
                  }
                }
            },
            skip,
            take,
        })
      ])
      if (offerings === null) {
        throw new Error('The specified jobs not exist!');
    }
      return Result.ok({array: offerings, count: count});
    } catch (e) {
      if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
    }
}


export default {
    all: multiple,
    one: specific
};