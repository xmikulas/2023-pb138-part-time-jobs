import { Result } from "@badrap/result";
import { Review } from "@prisma/client";
import client from "../../client";
import { genericError } from "../types";
import { ReviewDeleteData } from "../types/review";

/**
 * Delete, a repository call that deletes a Review.
 *
 * @param data object containing id to delete appropriate Review record
 * @returns - On success: Review record deleted
 *          - On failure: a generic error
 */

export const deleteReview = async (data: ReviewDeleteData): Promise<Result<Review, Error>> => {
    try {
        const deletedReview = await client.review.delete({
            where: {
                id: data.id,
            }
        });

        return Result.ok(deletedReview);
    } catch (e) {
        if (e instanceof Error) {
            return Result.err(e);
          }
          return genericError;
    }
};