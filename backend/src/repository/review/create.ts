import { genericError } from "../types";
import { Result } from '@badrap/result';
import { ReviewCreateData } from "../types/review";
import client from "../../client";
import { Review } from "@prisma/client";

/**
 * Create a repository call that creates a Review.
 *
 * @param data object containing necessary data to create a new Review record
 * @returns - On success: the created Review record
 *          - On failure: a generic error
 */

const create = async (data: ReviewCreateData): Promise<Result<Review, Error>> => {
  try {
    const review = await client.review.create({
        data: {
          ...data
        },
    });
    return Result.ok(review);
  
  } catch (e) {
    if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
  }
};
  
export default create;
