import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { Prisma, Review } from "@prisma/client";
import { PaginationOptions, multipleReadResponse } from "../types/common";
import { ReviewReadSpecificData, ReviewReadMultipleData } from "../types/review";

/**
 * Read repository call that finds a Review.
 *
 * @param data object containing necessary data to find a Review record
 * @returns - On success: Review record
 *          - On failure: a generic error
 */

const specific = async (data: ReviewReadSpecificData): Promise<Result<Review, Error>> => {
  try {
    const review = await client.review.findFirst({
        where: {
            reviewedId: data.reviewedId,
            reviewerId: data.reviewerId
            
        },
        select: {
            id: true,
            stars: true,
            comment: true,
            createdAt: true,
            reviewerId: true,
            reviewedId: true,
            reviewer: {
                select: {
                    name: true,
                    surname: true,
                    email: true,
                }
            }
        }
    });
    if (review === null) {
        throw new Error('The specified review not exist!');
    }
    return Result.ok(review);
  } catch (e) {
    return genericError;
  }
};
  

//make multiple read statement
const multiple = async (data: ReviewReadMultipleData,
                        paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<Review>, Error>> => {
  try {
    const where: Prisma.ReviewWhereInput = {
        reviewerId: data.reviewerId,
        reviewedId: data.reviewedId
    }

    const { skip, take } = paginationOptions;
    const [count, review] = await client.$transaction([
        client.review.count({where}),
        client.review.findMany({
        where,
        select: {
            id: true,
            stars: true,
            comment: true,
            createdAt: true,
            reviewerId: true,
            reviewedId: true,
            reviewer: {
                select: {
                    name: true,
                    surname: true,
                    email: true,
                }
            }
        },
        skip,
        take,
    })])
    if (review === null) {
        throw new Error('The specified Reviews does not exist!');
    }
    return Result.ok({array: review, count: count});
  } catch (e) {
    if (e instanceof Error) {
      return Result.err(e);
    }
    return genericError;
  }
};


export default {
    all: multiple,
    one: specific,
};