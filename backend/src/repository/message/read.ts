import { Result } from '@badrap/result';
import { MessageMultipleReadData } from "../types/message";
import client from "../../client";
import { Message, Prisma } from "@prisma/client";

import { genericError } from '../types';
import { PaginationOptions, multipleReadResponse } from '../types/common';



const multiple = async (data: MessageMultipleReadData,
                        paginationOptions: PaginationOptions = {}): Promise<Result<multipleReadResponse<Message>, Error>> => {
    try {
      const { skip, take } = paginationOptions;
      const where: Prisma.MessageWhereInput = {
        contractId: data.contractId
      };
      const [count, messages] = await client.$transaction([
        client.message.count({where}),
        client.message.findMany({
            where,
            select: {
                id: true,
                senderId: true,
                contractId: true,
                content: true,
                createdAt: true,
            },
            orderBy: {
                createdAt: 'asc',
            },
            skip,
            take,
        })
      ])
      if (messages === null) {
        throw new Error('The specified messages not exist!');
    }
      return Result.ok({array: messages, count: count});
    } catch (e) {
      if (e instanceof Error) {
        return Result.err(e);
      }
      return genericError;
    }
}


export default {
    all: multiple
};