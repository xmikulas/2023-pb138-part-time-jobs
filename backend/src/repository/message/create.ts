import { genericError } from "../types";
import { Result } from '@badrap/result';
import client from "../../client";
import { Message } from "@prisma/client";
import { MessageCreateData } from "../types/message";

/**
 * Create a repository call that creates a Message.
 *
 * @param data object containing necessary data to create a new Message record
 * @returns - On success: the created Message record
 *          - On failure: a generic error
 */

const create = async (data: MessageCreateData): Promise<Result<Message, Error>> => {
  try {
    const message = await client.message.create({
        data: {
            ...data
        },
      });
      return Result.ok(message);
  } catch (e) {
    return genericError;
  }
};
  
export default create;


