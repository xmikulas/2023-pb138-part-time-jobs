import { z } from 'zod';

export const loginSchema = z.object({
    email: z.string().email("This is not a valid email."),
    password: z.string().min(8)
})

export const verifySchema = z.object({
    password: z.string().min(8)
})