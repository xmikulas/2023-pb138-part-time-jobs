import { z } from "zod";


export const documentSchema = z.object({
    userId: z.string(),
    docType: z.enum(["CV", "PIC"]),
  });
  