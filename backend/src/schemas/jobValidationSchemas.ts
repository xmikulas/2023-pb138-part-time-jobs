import { z } from 'zod';


export const jobCreateSchema = z.object({
    employerId: z.string(),
    title: z.string(),
    description: z.string(),
    wage: z.number(),
    location: z.string(),
    numOfHour: z.number(),
    validTill: z.date().optional()
  });
  

  export const jobIdSchema = z.object({
    id: z.string()
  });
  

  export const jobUpdateSchema = z.object({
    id: z.string(),
    description: z.string().optional(),
    wage: z.number().optional(),
    location: z.string().optional(),
    numOfHour: z.number().optional(),
    validTill: z.date().optional()
  })
  

  
  export const jobsFilterSchema = z.object({
    wageFrom: z.coerce.number().optional(),
    wageTo: z.coerce.number().optional(),
    hoursFrom: z.coerce.number().optional(),
    hoursTo: z.coerce.number().optional(),
    location: z.string().optional(),
    title: z.string().optional(),
    description: z.string().optional(),
    employerId: z.string().optional(),
    skip: z.coerce.number().min(0).default(0),
    take: z.coerce.number().min(1).max(100).default(12)
  })