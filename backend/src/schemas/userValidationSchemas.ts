import { z } from 'zod';


export const userCreateSchema = z.object({
    name: z.string(),
    surname: z.string(),
    email: z.string().email("This is not a valid email."),
    number: z.string().max(16),
    about: z.string(),
    dateBorn: z.string().datetime(),
    location: z.string(),
    role: z.enum(['EMPLOYEE', 'EMPLOYER']),
    password: z.string()
  });
  

  export const userIdSchema = z.object({
    id: z.string()
  })
  
  export const userUpdateSchema = z.object({
    id: z.string(),
    name: z.string().optional(),
    surname: z.string().optional(),
    email: z.string().email("This is not a valid email.").optional(),
    number: z.string().max(16).optional(),
    about: z.string().optional(),
    role: z.enum(['EMPLOYEE', 'EMPLOYER']).optional(),
    password: z.string().optional(),
    expectedWage: z.number().nullable().optional(),
    expectedHours: z.number().nullable().optional(),
    dateBorn: z.string().datetime().optional()
  })
  
  
  export const filterUserSchema = z.object({
    name: z.string().optional(),
    wageFrom: z.coerce.number().optional(),
    wageTo: z.coerce.number().optional(),
    hoursFrom: z.coerce.number().optional(),
    hoursTo: z.coerce.number().optional(),
    location: z.string().optional(),
    dateBorn: z.date().optional(),
    skip: z.coerce.number().min(0).default(0),
    take: z.coerce.number().min(1).max(100).default(12)
  })


  export const userFilterBodySchema = z.object({
    id: z.string(),
    skip: z.coerce.number().min(0).default(0),
    take: z.coerce.number().min(1).max(100).default(12),
  })

