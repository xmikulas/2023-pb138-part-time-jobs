import { z } from 'zod';


export const offerCreateSchema = z.object({
    userId: z.string(),
    title: z.string(),
    description: z.string(),
  });

export const offerReadSchema = z.object({
    offeringId: z.string()
})

export const offerMultipleReadSchema = z.object({
    userId: z.string().optional(),
    skip: z.coerce.number().optional().default(0),
    take: z.coerce.number().optional().default(100)
})

export const offerDeleteSchema = z.object({
    offeringId: z.string(),
    userId: z.string()
})

export const offerUpdateSchema = z.object({
    userId: z.string(),
    id: z.string(),
    title: z.string().optional(),
    description: z.string().optional()
})