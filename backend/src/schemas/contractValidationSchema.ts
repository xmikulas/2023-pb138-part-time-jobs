import { z } from 'zod';


export const contractCreateSchema = z.object({
    userId: z.string(),
    jobId: z.string().optional(),
    offerId: z.string().optional()
}).refine(
  data => (data.jobId !== undefined && data.offerId === undefined) || (data.jobId === undefined && data.offerId !== undefined),
  'Exactly one of jobId and offerId should be filled in.',
);

export const contractReadSchema = z.object({
    userId: z.string().optional(),
    jobId: z.string().optional(),
    offerId: z.string().optional()
}).refine(
  data => (data.jobId !== undefined && data.offerId === undefined) || (data.jobId  === undefined && data.offerId !== undefined),
  'Exactly one of jobId and offerId should be filled in.',
);

export const contractReadByIdSchema = z.object({
    id: z.string()
})

export const contractMultipleReadSchema = z.object({
    jobId: z.string().optional(),
    offerId: z.string().optional(),
    skip: z.number().optional().default(0),
    take: z.number().optional().default(100)
}).refine(
  data => (data.jobId !== undefined && data.offerId === undefined) || (data.jobId === undefined && data.offerId !== undefined),
  'Exactly one of jobId and offerId should be filled in.',
);

export const contractUpdateSchema = z.object({
    id: z.string(),
    settledHours: z.number().optional(),
    settledWage: z.number().optional(),
    approvedEmployer: z.boolean().optional(),
    approvedEmployee: z.boolean().optional()
})

export const contractDeleteSchema = z.object({
    id: z.string()
})