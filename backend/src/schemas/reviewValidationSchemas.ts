import { z } from 'zod';


export const reviewCreateSchema = z.object({
    reviewerId: z.string(),
    reviewedId: z.string(),
    comment: z.string(),
    stars: z.enum(['ONE', 'TWO', 'THREE', 'FOUR', 'FIVE'])
})

export const reviewReadSpecificSchema = z.object({
    reviewerId: z.string(),
    reviewedId: z.string()
})

export const reviewReadMultipleSchema = z.object({
    reviewerId: z.string().optional(),
    reviewedId: z.string().optional(),
    skip: z.coerce.number().min(0).default(0),
    take: z.coerce.number().min(1).max(100).default(12)
})

export const reviewDeleteSchema = z.object({
    id: z.string()
})