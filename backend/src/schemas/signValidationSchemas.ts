import { z } from 'zod';


export const signFilterSchema = z.object({
    userId: z.string(),
    skip: z.coerce.number().min(0).default(0),
    take: z.coerce.number().min(1).max(100).default(12),
  })


export const userSignSchema = z.object({
    userId: z.string(),
    jobId: z.string()
})

