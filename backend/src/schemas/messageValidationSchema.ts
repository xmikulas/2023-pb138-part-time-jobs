import { z } from 'zod';


export const messageCreateSchema = z.object({
    senderId: z.string(),
    contractId: z.string(),
    content: z.string()
})

export const messageReadSchema = z.object({
    firstParticipantId: z.string(),
    secondParticipantId: z.string(),
    contractId: z.string(),
    skip: z.number().optional().default(0),
    take: z.number().optional().default(100)
})