import { NextFunction, Request, Response } from "express"
import { authType } from "./types/types"
import { Role } from "@prisma/client";

const authenticate = (
    requirement: authType,
    checkOwnership?: (req: Request) => boolean) => 
    (req: Request, res: Response, next: NextFunction) => {
    if (requirement === authType.ANONYMOUS) {
        next();
        return;
    }

    if (req.session.user && req.session.user.role === Role.ADMIN) {
        next();
        return;
    }

    if (requirement === authType.SELF && checkOwnership && checkOwnership(req)) {
        next();
        return;
    }

    if (requirement === authType.EMPLOYEE && req.session.user && req.session.user.role === Role.EMPLOYEE) {
        next();
        return;
    }

    if (requirement === authType.EMPLOYER && req.session.user && req.session.user.role === Role.EMPLOYER) {
        next();
        return;
    }

    return res.status(403).send("Forbidden");
}

export default authenticate;