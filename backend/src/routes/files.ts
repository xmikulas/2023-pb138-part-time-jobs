import type { Request } from 'express';
import { FileArray } from 'express-fileupload';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import config  from  '../../../common/endponts.json';
import { deleteFileController, getFileController, postFileController } from '../controllers/fileController'


declare module 'express' {
    interface Request {
      files?: FileArray;
    }
}


const express = require('express');


const file = express.Router();
const checkOwnership = (req: Request) => req.params.userId === req.session.user?.id;


// posts a file
file.post(config.files.files, authenticate(authType.SELF, checkOwnership), postFileController);

// deletes a file
file.delete(config.files.files, authenticate(authType.SELF, checkOwnership), deleteFileController);

// gets u a document
file.get(config.files.files, authenticate(authType.ANONYMOUS), getFileController);




export default file;