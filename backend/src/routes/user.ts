import type { Request } from 'express';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import { createUserController, deleteUserController, filterIndependentUserController, filterReactingUsersController, getSpecificUserController, updateUserController } from '../controllers/userController';
import config  from  '../../../common/endponts.json';

const express = require('express');

const user = express.Router();

const checkOwnershipOnUpdate = (req : Request) => req.session.user?.id === req.body.id;
const checkOwnership = (req : Request) => req.params.id === req.session.user?.id;


// create user
user.post(config.user.user, authenticate(authType.ANONYMOUS), createUserController);

// get specific user
user.get(config.user.byId, authenticate(authType.ANONYMOUS), getSpecificUserController);

// update user
user.patch(config.user.user, authenticate(authType.SELF, checkOwnershipOnUpdate), updateUserController);

// deletes user
user.delete(config.user.byId, authenticate(authType.SELF, checkOwnership), deleteUserController);

// find users by wage and hours
user.get(config.user.users, authenticate(authType.ANONYMOUS), filterIndependentUserController)

// find users who reacted to offer
user.get(config.user.reactions, authenticate(authType.EMPLOYER), filterReactingUsersController);


export default user;