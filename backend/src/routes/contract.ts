import type { Request } from 'express';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import config  from  '../../../common/endponts.json';
import { createContractController, deleteContractController, getContractByIdController, getSpecificContractController, readContractsController, updateContractController } from '../controllers/contractController';

const express = require('express');
const contract = express.Router();


const checkOwnership = (req: Request) => req.body.userId === req.session.user?.id;

// create contract
contract.post(config.contract.contract, authenticate(authType.SELF, checkOwnership), createContractController);

// get specific contract by jobId/offerId and userId
contract.get(config.contract.contract, authenticate(authType.ANONYMOUS), getSpecificContractController);

// get specific contract by id
contract.get(config.contract.byId, authenticate(authType.ANONYMOUS), getContractByIdController);

// update contract
contract.patch(config.contract.contract, authenticate(authType.ANONYMOUS), updateContractController);

// deletes single contract
contract.delete(config.contract.delete, authenticate(authType.ANONYMOUS), deleteContractController);

// read multiple contracts
contract.get(config.contract.multiple, authenticate(authType.ANONYMOUS), readContractsController)


export default contract;