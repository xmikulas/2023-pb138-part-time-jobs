import type { Request } from 'express';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import { userSignController, signedUsersController } from '../controllers/signsController';
import config  from  '../../../common/endponts.json';

const express = require('express');


const checkOwnershipBody = (req: Request) => req.body.userId === req.session.user?.id;
const checkOwnershipParam = (req: Request) => req.params.userId === req.session.user?.id;

const sign = express.Router();


// signs logged user to job
sign.post(config.sign.sign, authenticate(authType.SELF, checkOwnershipBody), userSignController)

// retrieve all signed jobs for employee === me
sign.get(config.sign.list, authenticate(authType.SELF, checkOwnershipParam), signedUsersController)

export default sign;