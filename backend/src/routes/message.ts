import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import config  from  '../../../common/endponts.json';
import type { Request } from 'express';
import { createMessageController, getMessagesController } from '../controllers/messageController';


const express = require('express');
const message = express.Router();

const checkOwnershipOnPost = (req: Request) => req.body.senderId === req.session.user?.id;
const checkOwnershipOnRead = (req: Request) => (req.query.firstParticipantId === req.session.user?.id) || (req.query.secondParticipantId === req.session.user?.id);

// create offer

message.post(config.message.message, authenticate(authType.SELF, checkOwnershipOnPost), createMessageController);

// read multiple offers with filtering
message.get(config.message.message, authenticate(authType.SELF, checkOwnershipOnRead), getMessagesController);

export default message;