import type { Request } from 'express';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import { createJobController, deletesJobController, getSpecificJobController, readJobsController, updateJobController } from '../controllers/jobController';
import config  from  '../../../common/endponts.json';

const express = require('express');
const job = express.Router();


const checkOwnershipOnUpdate = (req: Request) => req.body.employerId === req.session.user?.id;
const checkOwnership = (req: Request) => req.body.employerId === req.session.user?.id;
const checkOwnershipOnDeleteSingle = (req: Request) => req.params.id !== undefined


// create job
job.post(config.job.job, authenticate(authType.SELF, checkOwnership), createJobController);

// get specific job by id
job.get(config.job.byId, authenticate(authType.ANONYMOUS), getSpecificJobController);

// update job
job.patch(config.job.byId, authenticate(authType.SELF, checkOwnershipOnUpdate), updateJobController);

// deletes single job
job.delete(config.job.byId, authenticate(authType.SELF, checkOwnershipOnDeleteSingle), deletesJobController);

// read multiple jobs with filtering
job.get(config.job.jobs, authenticate(authType.ANONYMOUS), readJobsController)


export default job;