import type { Request } from 'express';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import config  from  '../../../common/endponts.json';
import { createOfferController, deleteOfferController, getMultipleOffersController, getSpecificOfferController, updateOfferController } from '../controllers/offerController';

const express = require('express');
const offer = express.Router();

const checkOwnership = (req: Request) => req.body.userId === req.session.user?.id;
const checkOwnershipOnDeleteSingle = (req: Request) => req.params.userId !== undefined && req.params.userId === req.session.user?.id;


// create offer
offer.post(config.offer.offer, authenticate(authType.SELF, checkOwnership), createOfferController);

// get specific offer by id
offer.get(config.offer.byId, authenticate(authType.ANONYMOUS), getSpecificOfferController);

// deletes single offer
offer.delete(config.offer.delete, authenticate(authType.SELF, checkOwnershipOnDeleteSingle), deleteOfferController);

// update offer
offer.patch(config.offer.offer, authenticate(authType.SELF, checkOwnership), updateOfferController);

// read multiple offers
offer.get(config.offer.offer, authenticate(authType.ANONYMOUS), getMultipleOffersController);

export default offer;