// import type { Request } from 'express';
import authenticate from '../authMiddleware';
import { authType } from '../types/types';
import config  from  '../../../common/endponts.json';

import { Request } from 'express';
import { createReviewController, deleteReviewController, getMultipleReviewController, getSpecificReviewController } from '../controllers/reviewController';

const express = require('express');


const checkOwnershipBody = (req: Request) => req.body.reviewerId === req.session.user?.id;
const checkOwnershipParam = (req: Request) => req.params.userId === req.session.user?.id;

const review = express.Router();


// create a review
review.post(config.review.review, authenticate(authType.SELF, checkOwnershipBody), createReviewController)

// fetch a specific review
review.get(config.review.review, authenticate(authType.ANONYMOUS), getSpecificReviewController)

// fetch multiple reviews
review.get(config.review.readMultiple, authenticate(authType.ANONYMOUS), getMultipleReviewController)

// delete review by id
review.delete(config.review.delete, authenticate(authType.SELF, checkOwnershipParam), deleteReviewController)

// retrieve all signed jobs for employee === me
// review.get(config.review.review, authenticate(authType.SELF, checkOwnershipParam), signedUsersController)

export default review;