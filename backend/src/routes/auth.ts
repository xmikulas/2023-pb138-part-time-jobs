import { loggedProfileController, loginController, logoutController, verifyLoggedUserController } from '../controllers/authController';
import config  from  '../../../common/endponts.json';


const express = require('express');

const auth = express.Router();

// login
auth.post(config.auth.login, loginController);

// logout
auth.post(config.auth.logout, logoutController);

// get currently logged user
auth.get(config.auth.profile, loggedProfileController)

// verify logged user 
auth.post(config.auth.verify, verifyLoggedUserController)



export default auth