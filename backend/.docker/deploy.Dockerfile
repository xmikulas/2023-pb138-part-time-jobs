FROM node:16

# Set the working directory to /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install
RUN npm install --save-dev typescript

# Bundle app source
COPY . .

# Expose the desired port
EXPOSE 3000

# Set the start command
# CMD [ "npm run start" ]
CMD [ "sh", "-c", "yes | npm run migrate -- --force && npm run seed && npm run start" ]