import { atom } from 'recoil';

export const isAuthenticatedAtom = atom<boolean>({
    key: 'isAuthenticated',
    default: false,
});

export const isEmployer = atom<boolean>({
    key: 'isEmployer',
    default: false,
});

export const userIdAtom = atom<string>({
    key: 'userId',
    default: ''
})

