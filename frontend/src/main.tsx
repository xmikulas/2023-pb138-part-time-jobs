import React from 'react';
import 'normalize.css';
import App from './App';
import './assets/globalStyles.css';
import { createRoot } from 'react-dom/client';

const domNode = document.createElement("div");
const root = createRoot(domNode);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
document.body.appendChild(domNode);
