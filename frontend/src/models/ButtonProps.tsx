import WorkIcon from "@mui/icons-material/Work";
import WorkHistoryIcon from "@mui/icons-material/WorkHistory";
import MessageIcon from "@mui/icons-material/Message";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import PersonIcon from "@mui/icons-material/Person";
import ArticleIcon from "@mui/icons-material/Article";
import AssignmentIcon from "@mui/icons-material/Assignment";

export const loggedOutUserButtonProps = [
  {
    text: "Jobs",
    icon: <WorkIcon />,
    path: "/jobs",
  },
  {
    text: "Offers",
    icon: <ArticleIcon />,
    path: "/offers",
  },
];

export const employeeButtonProps = [
  {
    text: "Jobs",
    icon: <WorkIcon />,
    path: "/jobs",
  },
  {
    text: "Offers",
    icon: <ArticleIcon />,
    path: "/offers",
  },
  {
    text: "Applied Jobs",
    icon: <WorkHistoryIcon />,
    path: "/applied-jobs",
  },
  {
    text: "My Offers",
    icon: <AssignmentIcon />,
    path: "/my-offers",
  },
];

export const employerButtonProps = [
  {
    text: "Jobs",
    icon: <WorkIcon />,
    path: "/jobs",
  },
  {
    text: "Offers",
    icon: <ArticleIcon />,
    path: "/offers",
  },
  {
    text: "My Postings",
    icon: <ArticleIcon />,
    path: "/my-postings",
  },
  {
    text: "Employees",
    icon: <PersonIcon />,
    path: "/employees",
  },
];
