import { Fab, Grid, Pagination, PaginationItem } from "@mui/material";
import JobCard from "../components/JobComponents/JobCard";
import { useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import AddJobDialog from "../components/JobComponents/AddJobDialog";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../recoilAtoms";
import axios from "axios";
import { useQuery, useQueryClient } from "react-query";
import config from "../../../common/endponts.json";
import { JobFilterType } from "../types/Filter";
import { Job } from "../types/Job";
import { PaginationType } from "../types/Pagination";
import { parseJobFilter, parsePagination } from "../utils/QueryParameters";
import JobFilter from "../components/JobComponents/JobFilter";

const emptyFilter = {
  wageFrom: undefined,
  wageTo: undefined,
  hoursFrom: undefined,
  hoursTo: undefined,
  location: undefined,
  title: undefined,
  description: undefined,
  employerId: undefined,
};

const MyPosting = () => {
  const queryClient = useQueryClient();
  const jobsPerPage = 4;

  const [currentPage, setCurrentPage] = useState(1);
  const [openJobDialog, setOpenJobDialog] = useState(false);
  const userId = useRecoilValue(userIdAtom);
  const [totalPages, setTotalPages] = useState(1);
  const [pagination, setPagination] = useState({
    skip: (currentPage - 1) * jobsPerPage,
    take: jobsPerPage,
  });

  const [filter, setFilter] = useState(emptyFilter);

  const getMyPostings = async (
    filter: JobFilterType,
    pagination: PaginationType
  ) => {
    const paginationParams = parsePagination(pagination);
    const filterParams =
      parseJobFilter(filter) !== "" ? "&" + parseJobFilter(filter) : "";
    const employerParams = "&employerId=" + userId;
    const queryParams = "?" + paginationParams + filterParams + employerParams;
    return await axios.get<{ array: Job[]; count: number }>(
      config.serverAddress + config.job.jobs + queryParams
    );
  };

  const query = useQuery(
    ["getMyPostings", filter, pagination],
    () => getMyPostings(filter, pagination),
    {
      enabled: true,
      onSuccess: () => {
        setTotalPages(
          Math.ceil(
            query.data?.data?.count ? query.data?.data?.count / jobsPerPage : 1
          )
        );
      },
    }
  );

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
    setPagination({
      skip: (value - 1) * jobsPerPage,
      take: jobsPerPage,
    });
    queryClient.invalidateQueries(["getMyPostings"]);
  };

  const handleOpenAddJobModal = () => {
    setOpenJobDialog(true);
  };

  const handleCloseAddJobModal = () => {
    setOpenJobDialog(false);
    queryClient.invalidateQueries(["getMyPostings"]);
  };

  return (
    <>
      <JobFilter filter={filter} setFilter={setFilter} />
      <div style={{ paddingBottom: "50px" }}>
        <Grid container spacing={3} paddingTop={5} paddingX={10}>
          {query.data?.data.array.map((job, index) => (
            <Grid key={index} item xs={12} sm={6} md={5} lg={4} xl={3}>
              <JobCard job={job} key={index} canApply={false} />
            </Grid>
          ))}
        </Grid>
      </div>
      <Fab
        size="large"
        onClick={handleOpenAddJobModal}
        sx={{
          position: "fixed",
          bottom: "20px",
          right: "20px",
          backgroundColor: "var(--primaryColor)",
          "&:hover": {
            backgroundColor: "var(--hoverColor)",
          },
        }}
      >
        <AddIcon sx={{ color: "white" }} />
      </Fab>
      <AddJobDialog open={openJobDialog} onClose={handleCloseAddJobModal} />
      <div
        style={{ display: "flex", justifyContent: "center", paddingBottom: 50 }}
      >
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          showFirstButton
          showLastButton
          siblingCount={1}
          renderItem={(item) => <PaginationItem component="button" {...item} />}
        />
      </div>
    </>
  );
};
export default MyPosting;
