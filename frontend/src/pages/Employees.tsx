import { Grid, Pagination, PaginationItem } from "@mui/material";
import EmployeeCard from "../components/EmployeeComponents/EmployeeCard";
import { useState } from "react";
import EmployeeFilterBar from "../components/EmployeeComponents/EmployeeFilterBar";
import axios from "axios";
import { User } from "../types/User";
import config from "../../../common/endponts.json";
import { UserFilterType } from "../types/Filter";
import { PaginationType } from "../types/Pagination";
import { useQuery, useQueryClient } from "react-query";
import { parsePagination, parseUserFilter } from "../utils/QueryParameters";

const emptyFilter = {
  wageFrom: undefined,
  wageTo: undefined,
  hoursFrom: undefined,
  hoursTo: undefined,
  location: undefined,
  name: undefined,
};

const Employees = () => {
  const employeesPerPage = 4;
  const queryClient = useQueryClient();

  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [pagination, setPagination] = useState({
    skip: (currentPage - 1) * employeesPerPage,
    take: employeesPerPage,
  });
  const [filter, setFilter] = useState<UserFilterType>({});

  const getEmployees = async (
    filter: UserFilterType,
    pagination: PaginationType
  ) => {
    const paginationParams = parsePagination(pagination);
    const filterParams =
      parseUserFilter(filter) !== "" ? "&" + parseUserFilter(filter) : "";
    const queryParams = "?" + paginationParams + filterParams;
    return await axios.get<{ array: User[]; count: number }>(
      config.serverAddress + config.user.users + queryParams
    );
  };

  const query = useQuery(
    ["getEmployees", filter, pagination],
    () => getEmployees(filter, pagination),
    {
      enabled: true,
      onSuccess: () => {
        setTotalPages(
          Math.ceil(
            query.data?.data?.count
              ? query.data?.data?.count / employeesPerPage
              : 1
          )
        );
      },
    }
  );

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
    setPagination({
      skip: (value - 1) * employeesPerPage,
      take: employeesPerPage,
    });
    queryClient.invalidateQueries("getEmployees");
  };

  return (
    <>
      <EmployeeFilterBar setFilter={setFilter} />
      <div style={{ paddingBottom: "50px" }}>
        <Grid container spacing={3} paddingTop={5} paddingX={10}>
          {query.isSuccess &&
            query.data.data.array.map((employee, index) => (
              <Grid key={index} item xs={12} sm={6} md={5} lg={4} xl={3}>
                <EmployeeCard
                  user={employee}
                  key={index}
                />
              </Grid>
            ))}
        </Grid>
      </div>
      <div
        style={{ display: "flex", justifyContent: "center", paddingBottom: 50 }}
      >
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          showFirstButton
          showLastButton
          siblingCount={1}
          renderItem={(item) => <PaginationItem component="button" {...item} />}
        />
      </div>
    </>
  );
};

export default Employees;
