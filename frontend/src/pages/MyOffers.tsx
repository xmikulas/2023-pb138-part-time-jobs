import { Fab, Grid, Pagination, PaginationItem } from "@mui/material";
import OfferCard from "../components/OfferComponents/OfferCard";
import AddIcon from "@mui/icons-material/Add";
import OfferDialog from "../components/OfferComponents/OfferDialog";
import { useState } from "react";
import config from "../../../common/endponts.json";
import axios from "axios";
import { useQueryClient, useQuery } from "react-query";
import { Offering } from "../types/Offering";
import { PaginationType } from "../types/Pagination";
import { parsePagination } from "../utils/QueryParameters";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../recoilAtoms";

const MyOffers = () => {
  const [openDialog, setOpenDialog] = useState(false);
  const queryClient = useQueryClient();
  const offersPerPage = 4;
  const [currentPage, setCurrentPage] = useState(1);
  const [pagination, setPagination] = useState({
    skip: (currentPage - 1) * offersPerPage,
    take: offersPerPage,
  });
  const [totalPages, setTotalPages] = useState(1);
  const userId = useRecoilValue(userIdAtom)

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
    setPagination({
      skip: (value - 1) * offersPerPage,
      take: offersPerPage,
    });
    queryClient.invalidateQueries(["getMyOffers" + userId]);
  };

  const getOffers = async (pagination: PaginationType) => {
    const queryParams = "?" + parsePagination(pagination) + "&userId=" + userId;
    console.log(
      config.serverAddress + config.offer.offer + queryParams
    );
    return await axios.get<{ array: Offering[]; count: number }>(
      config.serverAddress + config.offer.offer + queryParams
    );
  };

  const query = useQuery(
    ["getMyOffers" + userId, pagination],
    () => getOffers(pagination),
    {
      onSuccess: () => {
        setTotalPages(
          Math.ceil(
            query.data?.data?.count ? query.data?.data?.count / offersPerPage : 1
          )
        );
      },
    }
  );
  return (
    <>
      <Grid container spacing={3} paddingY={5} paddingX={10}>
        {query.isSuccess &&
          query.data.data.array.map(
            (offer, index: React.Key | null | undefined) => (
              <Grid key={index} item xs={12} sm={6} md={5} lg={4} xl={3}>
                <OfferCard
                  key={index}
                  offer={offer}
                />
              </Grid>
            )
          )}
      </Grid>
      <OfferDialog
        open={openDialog}
        onClose={() => setOpenDialog(false)}
        offer={null}
      />
      <Fab
        size="large"
        onClick={() => setOpenDialog(true)}
        sx={{
          position: "fixed",
          bottom: "20px",
          right: "20px",
          backgroundColor: "var(--primaryColor)",
          "&:hover": {
            backgroundColor: "var(--hoverColor)",
          },
        }}
      >
        <AddIcon sx={{ color: "var(--whiteColor)" }} />
      </Fab>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          paddingBottom: 50,
        }}
      >
        <Pagination
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            showFirstButton
            showLastButton
            siblingCount={1}
            renderItem={(item) => (
              <PaginationItem component="button" {...item} />
            )}
          />
      </div>
    </>
  );
};

export default MyOffers;
