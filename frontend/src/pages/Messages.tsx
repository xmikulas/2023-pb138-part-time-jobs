import { Stack } from "@mui/material";
import MessageBox from "../components/MessageComponents/MessageBox";
import MessageSidebar from "../components/MessageComponents/MessageSidebar";
import { useParams } from "react-router-dom";
import { Contract } from "../types/Contract";
import axios from "axios";
import config from "../../../common/endponts.json";
import { useQuery, useQueryClient } from "react-query";

const Messages = () => {
  const queryClient = useQueryClient();
  const { contractId } = useParams();

  const getUser = async () => {
    return await axios.get<{ data: Contract }>(
      config.serverAddress + config.contract.contract + "/" + contractId
    );
  };
  const query = useQuery("getContract", getUser, {
    staleTime: 1000 * 60 * 10,
    cacheTime: 1000 * 60 * 60,
    enabled: contractId !== undefined,
    onSettled: () => {
      queryClient.invalidateQueries("getContractor");
      queryClient.invalidateQueries("getContractMessages");
    },
  });

  return (
    <>
      {query.isSuccess && query.data.data.data.user && (
        <Stack
          direction={"row"}
          justifyContent={"space-between"}
          position={"absolute"}
          bottom={0}
          top={60}
          left={0}
          right={0}
        >
          <MessageSidebar contract={query.data.data.data}></MessageSidebar>
          <MessageBox contract={query.data.data.data}></MessageBox>
        </Stack>
      )}
      {query.isError && "Error"}
    </>
  );
};

export default Messages;
