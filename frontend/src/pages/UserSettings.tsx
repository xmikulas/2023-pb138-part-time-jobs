import { Box, Card, Grid, Typography } from "@mui/material";
import axios from "axios";
import { useQuery } from "react-query";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../recoilAtoms";
import ChangePasswordForm from "../components/UserSettingsComponents/ChangePasswordForm";
import UserInfoForm from "../components/UserSettingsComponents/UserInfoForm";
import { ErrorMessage } from "../components/GeneralComponents/ErrorMessage";
import ProfilePictureForm from "../components/UserSettingsComponents/ProfilePictureForm";
import { User } from "../types/User";
import config from "../../../common/endponts.json";
import UploadCvForm from "../components/UserSettingsComponents/UploadCvForm";

const UserSettings = () => {
  const userId = useRecoilValue(userIdAtom);
  // ulozit token do local storage

  const getUser = async () => {
    return await axios.get<{ data: User }>(
      config.serverAddress + config.auth.profile
    );
  };
  const query = useQuery("getUserSettings", getUser, {
    staleTime: 1000 * 60 * 10,
    cacheTime: 1000 * 60 * 60,
    enabled: userId !== "",
  });

  return (
    <div>
      {(query.isError || (!query.isLoading && !query.data?.data?.data?.id)) && (
        <ErrorMessage text="Unable to get user" />
      )}

      {query.status === "loading" && <Typography>Loading...</Typography>}

      {query.status === "success" && query.data?.data?.data.id && (
        <Grid container p={1} display={"flex"} justifyContent={"space-evenly"}>
          <Grid item xs={5}>
            <Card
              sx={{ p: 2, display: "flex", flexDirection: "column", gap: 2 }}
            >
              <ProfilePictureForm userId={query.data?.data?.data.id} />
              <UploadCvForm userId={query.data?.data?.data.id} />
              <ChangePasswordForm userId={query.data?.data?.data.id} />
            </Card>
          </Grid>
          <Grid item xs={5}>
            <Card sx={{ p: 5, display: "flex", flexDirection: "column" }}>
              <UserInfoForm user={query.data?.data.data} />
            </Card>
          </Grid>
        </Grid>
      )}
    </div>
  );
};

export default UserSettings;
