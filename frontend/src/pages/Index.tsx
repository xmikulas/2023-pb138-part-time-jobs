import { Routes, Route, Navigate } from "react-router-dom";
import { useRecoilValue, useSetRecoilState } from "recoil";
import axios from "axios";
import Jobs from "./Jobs";
import Employees from "./Employees";
import AppliedJobs from "./AppliedJobs";
import MyPosting from "./MyPosting";
import UserSettings from "./UserSettings";
import { isAuthenticatedAtom, isEmployer, userIdAtom } from "../recoilAtoms";
import HeaderBar from "./../components/Header";
import EmployeePage from "./EmployeePage";
import { User } from "../types/User";
import { useQuery } from "react-query";
import config from "../../../common/endponts.json";
import Offers from "./Offers";
import MyOffers from "./MyOffers";
import Messages from "./Messages";
import { Component, ReactNode } from "react";

const Index = () => {
  const setIsAuthenticated = useSetRecoilState(isAuthenticatedAtom);
  const loggedIn = useRecoilValue(isAuthenticatedAtom);
  const userId = useRecoilValue(userIdAtom);

  const setIsEmployer = useSetRecoilState(isEmployer);
  const setUserId = useSetRecoilState(userIdAtom);

  const getUser = async () => {
    return await axios.get<{ data: Partial<User> }>(
      config.serverAddress + config.auth.profile
    );
  };
  const query = useQuery(["getCurrentUser"], getUser, {
    staleTime: 1000 * 60,
    onError: () => {
      setUserId("");
      setIsAuthenticated(false);
    },
    onSuccess(data) {
      setIsAuthenticated(true);
      setUserId(data.data.data.id!);
      setIsEmployer(data.data.data.role === "EMPLOYER");
    },
  });

  type childProp = {
    children: JSX.Element;
  };

  const PrivateRoute = ({ children }: childProp) => {
    if (loggedIn) {
      return children;
    }
    return <Navigate to="/" />;
  };

  return (
    <>
      <HeaderBar />
      <Routes>
        <Route path="*" element={<Navigate to="/jobs" replace={true} />} />
        <Route path="/" element={<Navigate to="/jobs" replace={true} />} />
        {/* this is optional, but Jobs are default route and also /jobs route */}
        <Route path="/jobs" element={<Jobs />} />
        <Route
          path="/applied-jobs"
          element={
            <PrivateRoute>
              <AppliedJobs />
            </PrivateRoute>
          }
        />
        <Route path="/offers" element={<Offers />} />
        <Route
          path="/employees"
          element={
            <PrivateRoute>
              <Employees />
            </PrivateRoute>
          }
        />
        <Route
          path="/my-offers"
          element={
            <PrivateRoute>
              <MyOffers />
            </PrivateRoute>
          }
        />
        <Route
          path="/my-postings"
          element={
            <PrivateRoute>
              <MyPosting />
            </PrivateRoute>
          }
        />
        {/* na toto :id potom asi pouzit toto: const { id } = useParams(); */}
        <Route path="/employee/:id" element={<EmployeePage />}></Route>
        <Route
          path="/user-settings"
          element={
            <PrivateRoute>
              <UserSettings />
            </PrivateRoute>
          }
        />
        <Route
          path="/messages/:contractId"
          element={
            <PrivateRoute>
              <Messages />
            </PrivateRoute>
          }
        />
      </Routes>
    </>
  );
};

export default Index;
