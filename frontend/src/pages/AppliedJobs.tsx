import { Grid, Pagination, PaginationItem } from "@mui/material";
import JobCard from "../components/JobComponents/JobCard";
import { useState } from "react";
import axios from "axios";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../recoilAtoms";
import { PaginationType } from "../types/Pagination";
import { SignedJob } from "../types/Job";
import config from "../../../common/endponts.json";
import { useQuery, useQueryClient } from "react-query";
import { parsePagination } from "../utils/QueryParameters";

const AppliedJobs = () => {
  const queryClient = useQueryClient();
  const [currentPage, setCurrentPage] = useState(1);
  const userId = useRecoilValue(userIdAtom);
  const jobsPerPage = 4;
  const [pagination, setPagination] = useState({
    skip: (currentPage - 1) * jobsPerPage,
    take: jobsPerPage,
  });
  const [totalPages, setTotalPages] = useState(1);

  const getJobs = async (pagination: PaginationType) => {
    const queryParams = "?" + parsePagination(pagination);
    return await axios.get<{ array: SignedJob[]; count: number }>(
      config.serverAddress + config.sign.getList + userId + queryParams
    );
  };

  const query = useQuery(
    ["getAppliedJobs", pagination],
    () => getJobs(pagination),
    {
      enabled: userId !== "",
      onSuccess: () => {
        const count = query.data?.data?.array.filter((job) => job.job?.employerId !== undefined).length
        setTotalPages(
          Math.ceil(
            count ? count / jobsPerPage : 1
          )
        );
      },
    }
  );

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
    setPagination({
      skip: (value - 1) * jobsPerPage,
      take: jobsPerPage,
    });
    queryClient.invalidateQueries(["getAppliedJobs"]);
  };
  return (
    <>
      <div style={{ paddingBottom: "50px" }}>
        <Grid container spacing={3} paddingTop={5} paddingX={10}>
          {query.data?.data.array.filter((job) => job.job?.employerId !== undefined).map((job, index) => (
            <Grid key={index} item xs={12} sm={6} md={5} lg={4} xl={3}>
              {job.job && <JobCard
                job={job.job}
                key={index}
                canApply={false}
              />}
              
            </Grid>
          ))}
        </Grid>
      </div>
      <div
        style={{ display: "flex", justifyContent: "center", paddingBottom: 50 }}
      >
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          showFirstButton
          showLastButton
          siblingCount={1}
          renderItem={(item) => <PaginationItem component="button" {...item} />}
        />
      </div>
    </>
  );
};

export default AppliedJobs;
