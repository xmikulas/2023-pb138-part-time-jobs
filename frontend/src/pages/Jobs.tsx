import React, { useState } from 'react';
import Grid from '@mui/material/Grid';
import JobCard from '../components/JobComponents/JobCard';
import { Pagination, PaginationItem } from '@mui/material';
import JobFilter from '../components/JobComponents/JobFilter';
import axios from 'axios';
import config  from  '../../../common/endponts.json';
import { useQuery, useQueryClient } from 'react-query';
import { JobFilterType } from '../types/Filter';
import { PaginationType } from '../types/Pagination';
import { Job } from '../types/Job';
import { parseJobFilter, parsePagination } from '../utils/QueryParameters';


const Jobs = () => {
  const jobsPerPage = 4;
  const queryClient = useQueryClient()

  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [pagination, setPagination] = useState({
      skip: (currentPage - 1) * jobsPerPage,
      take: jobsPerPage
  })

  const [filter, setFilter] = useState({
    wageFrom: undefined,
    wageTo: undefined,
    hoursFrom: undefined,
    hoursTo: undefined,
    location: undefined,
    title: undefined,
    description: undefined,
    employerId: undefined
  });

  const getJobs = async (filter: JobFilterType, pagination: PaginationType) => {
    const queryParams = "?" + parseJobFilter(filter) + "&" + parsePagination(pagination)
    return await axios.get<{array: Job[], count: number}>(config.serverAddress + config.job.jobs + queryParams);
  }

  const query = useQuery(["getFilteredJobs", filter, pagination], () => getJobs(filter, pagination),
    { enabled: true,
    onSuccess: () => {
      setTotalPages(Math.ceil(query.data?.data?.count ? query.data?.data?.count / jobsPerPage: 1))
    } },);


  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setCurrentPage(value);
    setPagination({
        skip: (value - 1) * jobsPerPage,
        take: jobsPerPage
    })
    queryClient.invalidateQueries(["getFilteredJobs"]);
  };

  const handleClose = () => {
    queryClient.invalidateQueries(["getFilteredJobs"]);
  }


  return (
    <><div style={{ paddingBottom: '50px' }}>
      <JobFilter
      filter={filter}
      setFilter={setFilter}></JobFilter>

      {query.isError && "error"}
      {query.isSuccess && 
      <Grid container spacing={3} paddingTop={5} paddingX={10}>
        {query.data?.data.array.map((job: Job, index: React.Key | null | undefined) => (
          <Grid key={index} item xs={12} sm={6} md={5} lg={4} xl={3}>
            <JobCard
              job={job}
              key={index}
              canApply={true} />
          </Grid>
        ))}
      </Grid> }

      
    </div><div style={{ display: 'flex', justifyContent: 'center', paddingBottom: 50 }}>
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          showFirstButton
          showLastButton
          siblingCount={1}
          renderItem={(item) => <PaginationItem component="button" {...item} />} />
      </div></>
  )
}



export default Jobs;