import { Grid, Pagination, PaginationItem } from "@mui/material";
import OfferCard from "../components/OfferComponents/OfferCard";
import { useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import axios from "axios";
import { PaginationType } from "../types/Pagination";
import config from "../../../common/endponts.json";
import { parsePagination } from "../utils/QueryParameters";
import { Offering } from "../types/Offering";

const Offers = () => {
  const queryClient = useQueryClient();
  const offersPerPage = 4;
  const [currentPage, setCurrentPage] = useState(1);
  const [pagination, setPagination] = useState({
    skip: (currentPage - 1) * offersPerPage,
    take: offersPerPage,
  });
  const [totalPages, setTotalPages] = useState(1);

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
    setPagination({
      skip: (value - 1) * offersPerPage,
      take: offersPerPage,
    });
    queryClient.invalidateQueries(["getAllOffers"]);
  };

  const getOffers = async (pagination: PaginationType) => {
    const queryParams = "?" + parsePagination(pagination);
    return await axios.get<{ array: Offering[]; count: number }>(
      config.serverAddress + config.offer.offer + queryParams
    );
  };

  const query = useQuery(
    ["getAllOffers", pagination],
    () => getOffers(pagination),
    {
      onSuccess: () => {
        setTotalPages(
          Math.ceil(
            query.data?.data?.count
              ? query.data?.data?.count / offersPerPage
              : 1
          )
        );
      },
    }
  );

  return (
    <>
      <Grid container spacing={3} paddingY={5} paddingX={10}>
        {query.isSuccess &&
          query.data.data.array &&
          query.data.data.array.map(
            (offer, index: React.Key | null | undefined) => (
              <Grid key={index} item xs={12} sm={6} md={5} lg={4} xl={3}>
                <OfferCard offer={offer} key={index} />
              </Grid>
            )
          )}
      </Grid>
      <Grid>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            paddingBottom: 50,
          }}
        >
          <Pagination
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            showFirstButton
            showLastButton
            siblingCount={1}
            renderItem={(item) => (
              <PaginationItem component="button" {...item} />
            )}
          />
        </div>
      </Grid>
    </>
  );
};

export default Offers;
