import {
  Card,
  CardHeader,
  IconButton,
  Grid,
  CardContent,
  Typography,
  Box,
  Hidden,
  useMediaQuery,
  createTheme,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { useQuery } from "react-query";
import { User } from "../types/User";
import config from "../../../common/endponts.json";
import UserReviews from "../components/EmployeeComponents/UserReviews";
import UserCard from "../components/EmployeeComponents/UserCard";

const theme = createTheme();

const EmmployeePage = () => {
  const { id } = useParams();

  const getUser = async () => {
    return await axios.get<User>(config.serverAddress + config.user.user + id);
  };

  const query = useQuery("getSpecificUser", getUser, {
    staleTime: 1000 * 60,
    cacheTime: 1000 * 60 * 10,
    onSuccess: () => {
      console.log("success");
    },
    onError: () => {
      console.log("errors");
    },
  });


  const isMdAndLarger = useMediaQuery(theme.breakpoints.up("md"));

  if (query.data?.data.id === null) {
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Typography variant="h4" color="error" p={10}>
          User not found
        </Typography>
      </div>
    );
  }

  return (
    <>
      {query.data?.data !== undefined ? (
        <Box sx={{ backgroundColor: "grey" }}>
          <Grid container justifyContent={"space-evenly"}>
            <Grid item xs={15} sm={15} md={5} lg={4} p={2}>
              <UserCard user={query?.data.data} />
            </Grid>
            <Grid item xs={12} sm={15} md={7} lg={8} p={2}>
              <Card
                sx={{
                  p: 5,
                  ...(isMdAndLarger && {
                    minHeight: 400,
                  }),
                  ...(!isMdAndLarger && {
                    minHeight: 100,
                  }),
                  overflow: "auto",
                }}
              >
                <CardHeader title="About me" />

                <CardContent>
                  <Typography variant="body1" color="initial" align="justify">
                    {query?.data?.data.about}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} p={2}>
              <Card
                sx={{
                  p: 5,
                  ...(isMdAndLarger && {
                    minHeight: 270,
                  }),
                  ...(!isMdAndLarger && {
                    minHeight: 100,
                  }),
                }}
              >
                <CardHeader title="Ratings" />
                <UserReviews employee={query.data?.data} />
              </Card>
            </Grid>
          </Grid>
        </Box>
      ) : (
        "User not found"
      )}
    </>
  );
};

export default EmmployeePage;
