import { JobFilterType, UserFilterType } from "../types/Filter";
import { PaginationType } from "../types/Pagination";

export const parseJobFilter = (filter: JobFilterType) => {
  const queryParams = [];
  for (const [key, value] of Object.entries(filter)) {
    if (value !== undefined) {
      queryParams.push(`${key}=${encodeURIComponent(value)}`);
    }
  }
  return queryParams.join("&");
};

export const parseUserFilter = (filter: UserFilterType) => {
  const queryParams = [];
  for (const [key, value] of Object.entries(filter)) {
    if (value !== undefined) {
      queryParams.push(`${key}=${encodeURIComponent(value)}`);
    }
  }
  return queryParams.join("&");
};

export const parsePagination = (pagination: PaginationType) => {
  const queryParams = [];
  for (const [key, value] of Object.entries(pagination)) {
    queryParams.push(`${key}=${value}`);
  }
  return queryParams.join("&");
};
