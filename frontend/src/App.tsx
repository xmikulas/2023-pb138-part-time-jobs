import Index from "./pages/Index";
import { BrowserRouter as Router } from "react-router-dom";
import { RecoilRoot } from "recoil";
import { QueryClient, QueryClientProvider } from "react-query";
import { ThemeProvider, createTheme } from "@mui/material";
import { ReactQueryDevtools } from "react-query/devtools";

const theme = createTheme({
  palette: {
    primary: {
      main: "#FF8000",
      contrastText: "#fff",
    },
    secondary: {
      main: "#2a2825",
    },
    common: {
      black: "#2a2825",
      white: "#ffffff",
    },
    error: {
      main: "#cd4631",
    },
    success: {
      main: "#cd4631",
    },
    background: {
      default: "#2a2825",
    },
  },
});

const App = () => {
  const queryClient = new QueryClient();

  return (
    <RecoilRoot>
      <Router>
        <QueryClientProvider client={queryClient}>
          <ThemeProvider theme={theme}>
            <Index />
          </ThemeProvider>
        </QueryClientProvider>
      </Router>
    </RecoilRoot>
  );
};

export default App;
