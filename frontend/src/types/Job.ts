export type Job = {
  id: string;
  location: string;
  title: string;
  numOfHour: number;
  description: string;
  wage: number;
  employerId: string;
};

export type SignedJob = {
  id: string;
  jobId: string;
  job: Job;
};
