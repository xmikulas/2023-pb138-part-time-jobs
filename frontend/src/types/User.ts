export type UserUpdateType = {
    id: string
    name: string
    surname: string
    email: string
    number: string
    about: string
    expectedWage: number | undefined
    expectedHours: number | undefined
    dateBorn: Date
}

export type UserChangePasswordType = {
    id: string
    oldPassword: string
    newPassword: string
}

export type User = {
    id: string;
    name: string;
    surname: string;
    email: string;
    number: string;
    about: string;
    createdAt: Date;
    location: string;
    dateBorn: Date;
    deletedAt: Date | undefined;
    role: string;
    expectedWage: number | undefined;
    expectedHours: number | undefined;
    password: string;
}