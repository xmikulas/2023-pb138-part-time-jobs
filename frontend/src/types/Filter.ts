export type JobFilterType = {
  wageFrom: number | undefined;
  wageTo: number | undefined;
  hoursFrom: number | undefined;
  hoursTo: number | undefined;
  location: string | undefined;
  title: string | undefined;
};

export type UserFilterType = {
  wageFrom?: number | undefined;
  wageTo?: number | undefined;
  hoursFrom?: number | undefined;
  hoursTo?: number | undefined;
  location?: string | undefined;
  name?: string | undefined;
};
