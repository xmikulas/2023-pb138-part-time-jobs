import { Job } from "./Job";
import { Message } from "./Message";
import { Offering } from "./Offering";
import { User } from "./User";

export type ContractCreateType = {
  userId: string;
  jobId?: string | undefined;
  offerId?: string | undefined;
};
export type Contract = {
  id: string;
  userId: string;
  offer: Offering | undefined;
  user: User;
  job: Job | undefined;
  contract: Message[];
  settledHours: number | undefined;
  settledWage: number | undefined;
  approvedEmployer: boolean | undefined;
  approvedEmployee: boolean | undefined;
};
