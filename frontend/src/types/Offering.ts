import { Contract } from "./Contract";
import { User } from "./User";

export type Offering = {
  id: string;
  title: string;
  description: string;
  userId: string;
  user: User;
  contract: Contract[];
};

export type CreateOfferType = {
  title: string;
  description: string;
  userId: string;
};
