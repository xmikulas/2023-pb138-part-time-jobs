export type Message = {
  id: string;
  senderId: string;
  contractId: string;
  content: string;
};

export type CreateMessage = {
    senderId: string;
    contractId: string;
    content: string;
  };
  
