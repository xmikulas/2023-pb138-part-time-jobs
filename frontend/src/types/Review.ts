import { User } from "./User";

export type ReviewCreateData = {
  reviewerId: string;
  reviewedId: string;
  comment: string;
  stars: number;
};

export type Review = {
    reviewerId: string;
    reviewedId: string;
    comment: string;
    stars: string;
    reviewer: Partial<User>
  };

export const numberToStars: { [key: number]: string } = {
  1: "ONE",
  2: "TWO",
  3: "THREE",
  4: "FOUR",
  5: "FIVE",
};

export const starsToNumber: { [key: string]: number } = {
  ONE: 1,
  TWO: 2,
  THREE: 3,
  FOUR: 4,
  FIVE: 5,
};
