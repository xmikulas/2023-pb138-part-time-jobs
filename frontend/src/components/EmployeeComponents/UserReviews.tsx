import {
  CardContent,
  Grid,
  Card,
  CardHeader,
  Avatar,
  IconButton,
  Rating,
  Typography,
  Pagination,
  PaginationItem,
  Box,
} from "@mui/material";
import { useQuery, useQueryClient } from "react-query";
import { parsePagination } from "../../utils/QueryParameters";
import { PaginationType } from "../../types/Pagination";
import axios from "axios";
import { useState } from "react";
import config from "../../../../common/endponts.json";
import { Review, starsToNumber } from "../../types/Review";
import { User } from "../../types/User";

interface UserReviewsProps {
  employee: User;
}

const UserReviews: React.FC<UserReviewsProps> = ({ employee }) => {
  const queryClient = useQueryClient();
  const reviewsPerPage = 6;

  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [pagination, setPagination] = useState({
    skip: (currentPage - 1) * reviewsPerPage,
    take: reviewsPerPage,
  });

  const getReviews = async (pagination: PaginationType) => {
    const queryParams =
      "?" + parsePagination(pagination) + "&reviewedId=" + employee.id;
    return await axios.get<{ array: Review[]; count: number }>(
      config.serverAddress + config.review.readMultiple + queryParams
    );
  };

  const query = useQuery(
    ["getFilteredReviews", pagination],
    () => getReviews(pagination),
    {
      enabled: true,
      onSuccess: () => {
        console.log("reviews:" + query.data?.data.array);
        setTotalPages(
          Math.ceil(
            query.data?.data?.count
              ? query.data?.data?.count / reviewsPerPage
              : 1
          )
        );
      },
    }
  );

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
    setPagination({
      skip: (value - 1) * reviewsPerPage,
      take: reviewsPerPage,
    });
    queryClient.invalidateQueries(["getFilteredReviews"]);
  };

  return (
    <>
      <Box>
        <Grid container justifyContent={"space-evenly"} gap={2}>
          {query.data?.data?.array &&
            query.data?.data?.array.map((review, index) => (
              <Grid key={index} item {...{ xs: 13, sm: 5, md: 5, lg: 3 }}>
                <Card>
                  <CardHeader
                    avatar={<Avatar src=""></Avatar>}
                    action={<IconButton aria-label=""></IconButton>}
                    title={review.reviewer.name + " " + review.reviewer.surname}
                  />
                  <CardContent>
                    <Rating value={starsToNumber[review.stars]} readOnly />
                    <Typography variant="body2" color="initial">
                      {review.comment}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
        </Grid>
      </Box>
      <Box sx={{ display: "flex", justifyContent: "center", pt: 7 }}>
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          showFirstButton
          showLastButton
          siblingCount={1}
          renderItem={(item) => <PaginationItem component="button" {...item} />}
        />
      </Box>
    </>
  );
};

export default UserReviews;
