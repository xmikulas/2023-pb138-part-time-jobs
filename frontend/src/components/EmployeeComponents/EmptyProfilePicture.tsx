import { styled } from '@mui/system';
import { Avatar } from '@mui/material';

const Container = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '25vh',
  width: '25vh',
  borderRadius: '50%',
  overflow: 'hidden',
});

  
const ProfilePicture= () => {
    return (
      <Container>
        <Avatar/>
      </Container>
    );
  };
  
  export default ProfilePicture;