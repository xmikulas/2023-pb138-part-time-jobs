import { Dispatch, SetStateAction, useState } from "react";
import { Toolbar, Box, Button, TextField, InputAdornment } from "@mui/material";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import { Clear } from "@mui/icons-material";
import { UserFilterType } from "../../types/Filter";

interface EmployeeFilterBarProps extends UserFilterType {
  setFilter: Dispatch<SetStateAction<UserFilterType>>;
}

const textFieldStyles = {
  flex: 1,
  width: "100%",
  backgroundColor: "white",
  "& .MuiFilledInput-root": {
    "&:hover fieldset": {
      borderColor: "var(--primaryColor)",
    },
  },
  "& .MuiFilledInput-underline:after": {
    borderBottomColor: "var(--primaryColor)",
  },
};

const buttonStyle = {
  heigth: 50,
  backgroundColor: "var(--primaryColor)",
  "&:hover": { backgroundColor: "var(--hoverColor)" },
};

const EmployeeFilterBar = (props: EmployeeFilterBarProps) => {
  const [name, setName] = useState("");
  const [wageFrom, setWageFrom] = useState("");
  const [wageTo, setWageTo] = useState("");
  const [location, setLocation] = useState("");
  const [durationFrom, setDurationFrom] = useState("");
  const [durationTo, setDurationTo] = useState("");

  function updateFilter() {
    const filter = {
      name: name,
      wageFrom: wageFrom === "" ? undefined : Number(wageFrom),
      wageTo: wageTo === "" ? undefined : Number(wageTo),
      location: location,
      hoursFrom: durationFrom === "" ? undefined : Number(durationFrom),
      hoursTo: durationTo === "" ? undefined : Number(durationTo),
    };
    props.setFilter(filter);
  }

  function clearFilters() {
    setName("");
    setWageFrom("");
    setWageTo("");
    setLocation("");
    setDurationFrom("");
    setDurationTo("");
    const filter = {
      name: undefined,
      location: undefined,
      wageFrom: undefined,
      wageTo: undefined,
      hoursFrom: undefined,
      hoursTo: undefined,
    };
    props.setFilter(filter);
  }

  function filtersEmpty() {
    return (
      name === "" &&
      wageFrom === "" &&
      wageTo === "" &&
      durationFrom === "" &&
      durationTo === "" &&
      location === ""
    );
  }

  return (
    <>
      <Toolbar sx={{ backgroundColor: "var(--secondaryColor)", padding: 2 }}>
        <Box
          sx={{
            display: "flex",
            flexDirection: {
              xs: "column",
              sm: "column",
              md: "column",
              lg: "row",
            },
            gap: "10px",
            width: "100%",
          }}
        >
          <TextField
            label="Name"
            variant="filled"
            value={name}
            onChange={(e) => setName(e.target.value)}
            sx={{
              flex: 1,
              backgroundColor: "white",
            }}
          />
          <Box
            sx={{
              display: "flex",
              flex: 1,
              gap: "10px",
            }}
          >
            <TextField
              label="Wage From"
              variant="filled"
              value={wageFrom}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">kč/h</InputAdornment>
                ),
              }}
              sx={textFieldStyles}
              onChange={(e) => setWageFrom(e.target.value)}
            />
            <TextField
              label="Wage To"
              variant="filled"
              value={wageTo}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">kč/h</InputAdornment>
                ),
              }}
              sx={textFieldStyles}
              onChange={(e) => setWageTo(e.target.value)}
            />
          </Box>
          <TextField
            label="Location"
            variant="filled"
            value={location}
            onChange={(e) => setLocation(e.target.value)}
            sx={{
              flex: 1,
              backgroundColor: "white",
            }}
          />
          <Box
            sx={{
              display: "flex",
              flex: 1,
              gap: "10px",
            }}
          >
            <TextField
              label="Duration From"
              variant="filled"
              value={durationFrom}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">hours</InputAdornment>
                ),
              }}
              sx={textFieldStyles}
              onChange={(e) => setDurationFrom(e.target.value)}
            />
            <TextField
              label="Duration To"
              variant="filled"
              value={durationTo}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">hours</InputAdornment>
                ),
              }}
              sx={textFieldStyles}
              onChange={(e) => setDurationTo(e.target.value)}
            />
          </Box>
          <Button
            variant="contained"
            onClick={updateFilter}
            sx={{
              flex: "0 0 auto",
              backgroundColor: "var(--primaryColor)",
              "&:hover": { backgroundColor: "var(--hoverColor)" },
            }}
          >
            <FilterAltIcon />
            Filter
          </Button>
          <Button
            variant="contained"
            onClick={clearFilters}
            disabled={filtersEmpty()}
            sx={buttonStyle}
          >
            <Clear />
            Clear
          </Button>
        </Box>
      </Toolbar>
    </>
  );
};

export default EmployeeFilterBar;
