import {
  Card,
  CardHeader,
  Avatar,
  CardContent,
  Grid,
  Typography,
  Box,
  Chip,
  Rating,
  CardActions,
  Button,
} from "@mui/material";
import { id } from "date-fns/locale";
import { FC, useState } from "react";
import ReviewDialog from "../ReviewDialog/ReviewDialog";
import config from "../../../../common/endponts.json";
import { User } from "../../types/User";
import MailIcon from "@mui/icons-material/Mail";
import CallIcon from "@mui/icons-material/Call";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import axios from "axios";
import { useQuery } from "react-query";
import { Review, starsToNumber } from "../../types/Review";

interface UserReviewsProps {
  user: User;
}

const UserCard: FC<UserReviewsProps> = ({ user }) => {
  const [openRatingDialog, setOpenRatingDialog] = useState(false);
  const [canGiveReview, setCanGiveReview] = useState(true);
  const [averageRating, setAverageRating] = useState(1);

  const getReviews = async () => {
    const queryParams = "?" + "&reviewedId=" + user.id;
    return await axios.get<{ array: Review[]; count: number }>(
      config.serverAddress + config.review.readMultiple + queryParams
    );
  };

  const query = useQuery(["getUserSpecificReviews"], getReviews, {
    enabled: true,
    onSuccess: () => {
      if (query.data?.data) {
        const reviews = query.data?.data.array
          .map((review) => starsToNumber[review.stars])
          .reduce((accumulator, currentValue) => accumulator + currentValue, 0);
        setAverageRating(reviews ? reviews / query.data.data.count : 0);
      }
    },
  });

  const getCv = async () => {
    await axios.get(
      config.serverAddress + config.files.getFiles + user.id + "/type/CV"
    );
  };

  const cvQuery = useQuery("getCV" + user.id, getCv, { retry: false });

  const handleCvDownload = () => {
    window.open(
      config.serverAddress + config.files.getFiles + user.id + "/type/CV"
    );
  };

    return ( <>
    <Card sx={{ p: 5, height: 400 }}>
    <CardHeader
      avatar={
        <Avatar
          sx={{ height: 100, width: 100 }}
          src={
            config.serverAddress +
            config.files.getFiles +
            user.id +
            "/type/PIC"
          }
          />
        }
          title={user.name + " " + user.surname}
          subheader={user.location}
        />
        <CardContent>
          <Grid container direction="row" alignItems="center" gap={1}>
            <Grid item>
              <MailIcon />
            </Grid>
            <Grid item>
              <Typography variant="body2" color="initial">
                {user.email}
              </Typography>
            </Grid>
          </Grid>
          <Grid container direction="row" alignItems="center" gap={1}>
            <Grid item>
              <CallIcon />
            </Grid>
            <Grid item>
              <Typography variant="body2" color="initial">
                {user.number}
              </Typography>
            </Grid>
          </Grid>
          <Box p={4} display={"flex"} justifyContent={"center"}>
            <Chip
              icon={<MonetizationOnIcon />}
              label={user.expectedWage + " kč/h"}
            />
          </Box>
          <Box
            display={"flex"}
            flexDirection={"column"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <Typography variant="h6" color="initial">
              Average rating:
            </Typography>
            <Rating value={averageRating} readOnly />
          </Box>
        </CardContent>
        <div style={{ flexGrow: 1 }}></div>
        <CardActions sx={{ justifyContent: "center" }}>
          <Button
            disabled={!canGiveReview}
            variant="contained"
            onClick={() => setOpenRatingDialog(true)}
          >
            Rate
          </Button>
          <Button
            variant="contained"
            onClick={handleCvDownload}
            disabled={cvQuery.status !== "success"}
          >
            CV
          </Button>
        </CardActions>
        <ReviewDialog
          open={openRatingDialog}
          onClose={() => setOpenRatingDialog(false)}
          name={`${user.name} ${user.surname}`}
          employeeId={user.id}
        />
      </Card>
    </>
  );
};

export default UserCard;
