import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  Typography,
} from "@mui/material";
import PaidIcon from "@mui/icons-material/Paid";
import AccessTimeFilledIcon from "@mui/icons-material/AccessTimeFilled";
import { useNavigate } from "react-router-dom";
import { User } from "../../types/User";
import config from "../../../../common/endponts.json";
import { useQueryClient } from "react-query";

interface JobCardProps {
  user: User;
}

const EmployeeCard: React.FC<JobCardProps> = ({ user }) => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const navigateToEmployee = () => {
    queryClient.invalidateQueries("getSpecificUser");
    navigate("/employee/" + user.id);
  };

  return (
    <Card
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "var(--thirdColor)",
        border: "2px solid black",
        height: "400px",
      }}
    >
      <CardHeader
        avatar={
          <Avatar
            variant="square"
            sx={{ height: "100px", width: "100px" }}
            src={
              config.serverAddress +
              config.files.getFiles +
              user.id +
              "/type/PIC"
            }
          />
        }
        titleTypographyProps={{ variant: "h6" }}
        title={user.name + " " + user.surname}
        subheader={`Location: ${user.location}`}
      />
      <CardContent
        sx={{
          flexGrow: 1,
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        <Box>
          <Typography variant="body2" color="text.secondary" align="justify">
            {user.about}
          </Typography>
        </Box>
        <Grid
          container
          justifyContent="space-evenly"
          sx={{ padding: "15px 0px" }}
        >
          <Chip
            icon={<PaidIcon />}
            label={
              user.expectedWage !== null ? `${user.expectedWage}kč/h` : "N/A"
            }
            sx={{ fontSize: "20px" }}
          />
          <Chip
            icon={<AccessTimeFilledIcon />}
            label={
              user.expectedHours !== null ? `${user.expectedHours}h` : "N/A"
            }
            sx={{ fontSize: "20px" }}
          />
        </Grid>
      </CardContent>
      <CardActions>
        <Button
          variant="contained"
          sx={{
            ":hover": { backgroundColor: "var(--hoverColor)" },
            marginLeft: "auto",
          }}
          onClick={navigateToEmployee}
        >
          View more
        </Button>
      </CardActions>
    </Card>
  );
};

export default EmployeeCard;
