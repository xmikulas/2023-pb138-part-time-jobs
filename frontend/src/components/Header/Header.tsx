import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import {
  Box,
  Button,
  Collapse,
  Stack,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import AccountCard from "../AccountCard";
import {
  loggedOutUserButtonProps,
  employeeButtonProps,
  employerButtonProps,
} from "../../models/ButtonProps";
import { useLocation, useNavigate } from "react-router-dom";
import { useState } from "react";
import LoginDialog from "../AuthDialogs/LoginDialog";
import RegisterDialog from "../AuthDialogs/RegisterDialog";
import { useRecoilValue } from "recoil";
import { isAuthenticatedAtom, isEmployer } from "../../recoilAtoms";

const getCurrentRoleButtonProps = (loggedIn: boolean, isEmployer: boolean) => {
  if (!loggedIn) {
    return loggedOutUserButtonProps;
  }
  return isEmployer ? employerButtonProps : employeeButtonProps;
};

const Header = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const theme = useTheme();
  const isMdAndLarger = useMediaQuery(theme.breakpoints.up("lg"));
  const [collapseMenu, setCollapseMenu] = useState(false);

  const [openLoginDialog, setOpenLoginDialog] = useState(false);
  const [openRegisterDialog, setOpenRegisterDialog] = useState(false);
  const loggedIn = useRecoilValue(isAuthenticatedAtom);
  const employer = useRecoilValue(isEmployer);

  const handleNavigation = (path: string) => {
    navigate(path);
  };

  return (
    <>
      <AppBar position="sticky" color="secondary">
        <Toolbar>
          <Grid container display={"flex"}>
            <Grid item xs={4} sm={4} md={4}>
              <Typography
                variant="h4"
                color="primary"
                fontFamily={"Permanent Marker"}
              >
                Jobio
              </Typography>
            </Grid>

            {!isMdAndLarger && (
              <Grid item xs={5} sm={6}>
                <Button onClick={() => setCollapseMenu(!collapseMenu)}>
                  Show Menu
                </Button>
              </Grid>
            )}
            {isMdAndLarger && (
              <Grid item xs={6} display={"flex"} alignSelf={"center"} gap={1}>
                {getCurrentRoleButtonProps(loggedIn, employer).map(
                  ({ text, icon, path }, index) => (
                    <Button
                      key={index}
                      startIcon={icon}
                      onClick={() => handleNavigation(path)}
                      variant={
                        location.pathname.startsWith(path)
                          ? "contained"
                          : "outlined"
                      }
                    >
                      {text}
                    </Button>
                  )
                )}
              </Grid>
            )}
            <Grid
              item
              xs={3}
              sm={2}
              md={2}
              display={"flex"}
              justifyContent={"end"}
            >
              {loggedIn ? (
                <AccountCard />
              ) : (
                <>
                  <Button onClick={() => setOpenLoginDialog(true)}>
                    Login
                  </Button>
                  <Button onClick={() => setOpenRegisterDialog(true)}>
                    Register
                  </Button>
                </>
              )}
            </Grid>

            <Collapse in={collapseMenu} timeout={"auto"} unmountOnExit>
              <Stack minWidth={window.innerWidth}>
                {!isMdAndLarger &&
                  getCurrentRoleButtonProps(loggedIn, employer).map(
                    ({ text, icon, path }, index) => (
                      <Button
                        key={index}
                        startIcon={icon}
                        onClick={() => handleNavigation(path)}
                        fullWidth
                        variant={
                          location.pathname.startsWith(path)
                            ? "contained"
                            : "outlined"
                        }
                      >
                        {text}
                      </Button>
                    )
                  )}
              </Stack>
            </Collapse>
          </Grid>
        </Toolbar>
        <LoginDialog
          open={openLoginDialog}
          onClose={() => setOpenLoginDialog(false)}
        />
        <RegisterDialog
          open={openRegisterDialog}
          onClose={() => setOpenRegisterDialog(false)}
        />
      </AppBar>
    </>
  );
};

export default Header;
