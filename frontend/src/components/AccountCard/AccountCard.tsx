import { useState } from "react";

import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";

import LogoutIcon from "@mui/icons-material/Logout";
import PersonIcon from "@mui/icons-material/Person";
import { Avatar, Box, Menu, MenuItem } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { isAuthenticatedAtom, userIdAtom } from "../../recoilAtoms";
import { useRecoilValue, useSetRecoilState } from "recoil";
import axios from "axios";
import React from "react";
import ProfilePictureAvatar from "../GeneralComponents/ProfilePicAvatar";
import { useMutation, useQuery, useQueryClient } from "react-query";
import config from "../../../../common/endponts.json";
import { User } from "../../types/User";

const AccountCard = () => {
  const queryClient = useQueryClient();

  const navigate = useNavigate();

  const setIsAuthenticated = useSetRecoilState(isAuthenticatedAtom);
  const setUserId = useSetRecoilState(userIdAtom);

  const userId = useRecoilValue(userIdAtom);
  // ulozit token do local storage

  const getUser = async () => {
    return await axios.get<{ data: User }>(
      config.serverAddress + config.user.user + userId
    );
  };

  const query = useQuery(["getCurrentUser"], getUser, {
    staleTime: 1000 * 60,
    cacheTime: 1000 * 60 * 10,
    enabled: userId !== "",
  });

  const getPhoto = async () => {
    await axios.get(
      config.serverAddress + config.files.getFiles + userId + "/type/PIC"
    );
  };

  const queryPhoto = useQuery("getPhoto", getPhoto, {
    retry: false,
    enabled: userId !== "" && query.data?.data !== undefined,
  });

  const profileClick = () => {
    navigate("/user-settings");
    handleClose();
  };

  const logout = async () =>
    await axios.post(config.serverAddress + config.auth.logout);
  const logoutMutation = useMutation(() => logout());

  const logoutClick = () => {
    logoutMutation.mutate(undefined, {
      onSettled: () => {
        queryClient.refetchQueries(["getCurrentUser", "getUserSettings"])
        setIsAuthenticated(false);
        setUserId("");
        handleClose();
      },
    });
  };

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      {query.isSuccess && (
        <>
          <Box display={"flex"} alignItems={"center"}>
            <Typography
              sx={{
                mr: 2,
                display: {
                  xs: "none",
                  sm: "inline",
                  md: "inline",
                  lg: "inline",
                },
              }}
            >
              {query.data?.data.data.name + " " + query.data?.data.data.surname}
            </Typography>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="end"
              onClick={handleClick}
            >
              {queryPhoto.isSuccess ? (
                <ProfilePictureAvatar
                  src={
                    config.serverAddress +
                    config.files.getFiles +
                    userId +
                    "/type/PIC"
                  }
                />
              ) : (
                <Avatar />
              )}
            </IconButton>
          </Box>

          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <MenuItem onClick={profileClick}>
              <PersonIcon />
              Profile
            </MenuItem>
            <MenuItem onClick={logoutClick}>
              <LogoutIcon />
              Logout
            </MenuItem>
          </Menu>
        </>
      )}
    </>
  );
};

export default AccountCard;
