import {
  Box,
  Button,
  Dialog,
  DialogTitle,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import EmailIcon from "@mui/icons-material/Email";
import KeyIcon from "@mui/icons-material/Key";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { useSetRecoilState } from "recoil";
import axios from "axios";
import { isAuthenticatedAtom, isEmployer, userIdAtom } from "../../recoilAtoms";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import config from "../../../../common/endponts.json";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import { useState } from "react";
import { User } from "../../types/User";

interface LoginDialogProps {
  open: boolean;
  onClose: () => void;
}

const loginValidationSchema = z.object({
  email: z.string().email(),
  password: z.string().nonempty(),
});
type LoginValidationSchema = z.infer<typeof loginValidationSchema>;
const LoginDialog: React.FC<LoginDialogProps> = ({ open, onClose }) => {
  const queryClient = useQueryClient();
  const setUserId = useSetRecoilState(userIdAtom);
  const setIsEmployer = useSetRecoilState(isEmployer);

  const [obfuscatedPassword, setObfuscatedPassword] = useState(true);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<LoginValidationSchema>({
    resolver: zodResolver(loginValidationSchema),
  });
  const setIsAuthenticated = useSetRecoilState(isAuthenticatedAtom);

  axios.defaults.withCredentials = true;
  //REACT QUERY
  const login = async (data: LoginValidationSchema) =>
    await axios.post(config.serverAddress + config.auth.login, data, {
      withCredentials: true,
    });
  //REACT QUERY
  const getUser = async () => {
    return await axios.get<{ data: Partial<User> }>(
      config.serverAddress + config.auth.profile
    );
  };
  const query = useQuery(["getCurrentUser"], getUser, {
    staleTime: 1000 * 60,
    onError: () => {
      setUserId("");
      setIsAuthenticated(false);
    },
    onSuccess(data) {
      setIsAuthenticated(true);
      setUserId(data.data.data.id!);
      setIsEmployer(data.data.data.role === "EMPLOYER");
    },
  });

  const loginMutation = useMutation((data: LoginValidationSchema) =>
    login(data)
  );

  const onSubmit = async (data: LoginValidationSchema) => {
    loginMutation.mutate(data, {
      onSuccess: () => {
        setIsAuthenticated(true);
        onClose();
        reset();
      },
      onError: () => {
        setIsAuthenticated(false);
        reset();
      },
      onSettled: () => {
        queryClient.invalidateQueries();
      },
    });
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Login</DialogTitle>
      <Box
        component={"form"}
        sx={{ display: "flex", flexDirection: "column", px: 5, gap: 2 }}
      >
        <TextField
          {...register("email")}
          id="email"
          label="Email Address"
          name="email"
          required
          error={Boolean(errors.email)}
          helperText={errors.email && errors.email.message}
          autoFocus
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          {...register("password")}
          id="password"
          label="Password"
          name="password"
          required
          error={Boolean(errors.password)}
          helperText={errors.password && errors.password.message}
          type={obfuscatedPassword ? "password" : "text"}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <KeyIcon />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  onClick={() => {
                    setObfuscatedPassword(!obfuscatedPassword);
                  }}
                >
                  {obfuscatedPassword ? (
                    <VisibilityOffIcon />
                  ) : (
                    <VisibilityIcon />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "end",
          gap: 3,
          p: 2,
        }}
      >
        {loginMutation.isError && <ErrorMessage text="Unable to login" />}
        {loginMutation.isLoading && <Typography>Sending data...</Typography>}
        <Button color="error" onClick={onClose}>
          Close
        </Button>
        <Button
          variant="contained"
          type="submit"
          onClick={handleSubmit(onSubmit)}
        >
          Login
        </Button>
      </Box>
    </Dialog>
  );
};

export default LoginDialog;
