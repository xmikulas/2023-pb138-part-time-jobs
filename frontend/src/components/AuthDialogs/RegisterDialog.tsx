import {
  Box,
  Button,
  Dialog,
  DialogTitle,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import EmailIcon from "@mui/icons-material/Email";
import KeyIcon from "@mui/icons-material/Key";
import PersonIcon from "@mui/icons-material/Person";
import CallIcon from "@mui/icons-material/Call";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import LocationCityIcon from "@mui/icons-material/LocationCity";
import AssignmentIcon from "@mui/icons-material/Assignment";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";

import axios from "axios";
import { z } from "zod";
import validator from "validator";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import config from "../../../../common/endponts.json";
import { useMutation, useQueryClient } from "react-query";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import { useState } from "react";

interface RegisterDialogProps {
  open: boolean;
  onClose: () => void;
}

const registerValidationSchema = z.object({
  name: z.string().nonempty(),
  surname: z.string().nonempty(),
  email: z.string().email(),
  number: z
    .string()
    .nonempty()
    .refine((value) => validator.isMobilePhone(value), "Invalid phone number"),
  role: z.enum(["EMPLOYER", "EMPLOYEE"]),
  about: z.string(),
  dateBorn: z.date(),
  location: z.string().nonempty(),
  password: z.string().nonempty(),
});

type RegisterValidationSchema = z.infer<typeof registerValidationSchema>;
const RegisterDialog: React.FC<RegisterDialogProps> = ({ open, onClose }) => {
  const queryClient = useQueryClient();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<RegisterValidationSchema>({
    resolver: zodResolver(registerValidationSchema),
  });
  const [obfuscatedPassword, setObfuscatedPassword] = useState(true);

  axios.defaults.withCredentials = true;
  const registerUser = async (data: RegisterValidationSchema) =>
    await axios.post(config.serverAddress + config.user.user, data, {
      withCredentials: true,
    });

  const registerUserMutation = useMutation((data: RegisterValidationSchema) =>
    registerUser(data)
  );

  const login = async (data: { email: string; password: string }) =>
    await axios.post(config.serverAddress + config.auth.login, data, {
      withCredentials: true,
    });

  const loginMutation = useMutation(
    (data: { email: string; password: string }) => login(data)
  );

  const handleRegister = (data: RegisterValidationSchema) => {
    registerUserMutation.mutate(data, {
      onSuccess: () => {
        loginMutation.mutate(
          { email: data.email, password: data.password },
          {
            onSettled: () => {
              queryClient.invalidateQueries();
            },
          }
        );
        reset();
        onClose();
      },
      onError: () => {
        reset();
      },
    });
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Register</DialogTitle>
      <Box
        component={"form"}
        sx={{ display: "flex", flexDirection: "column", px: 5, gap: 2 }}
      >
        <Box sx={{ display: "flex", flexDirection: "row", gap: 1 }}>
          <TextField
            {...register("name")}
            id="first-name"
            label="First name"
            required
            error={Boolean(errors.name)}
            helperText={errors.name && errors.name.message}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PersonIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            {...register("surname")}
            id="last-name"
            label="Last name"
            required
            error={Boolean(errors.surname)}
            helperText={errors.surname && errors.surname.message}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PersonIcon />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <TextField
          {...register("email")}
          id="email"
          label="Email"
          required
          error={Boolean(errors.email)}
          helperText={errors.email && errors.email.message}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          {...register("password")}
          id="password"
          label="Password"
          name="password"
          required
          error={Boolean(errors.password)}
          helperText={errors.password && errors.password.message}
          type={obfuscatedPassword ? "password" : "text"}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <KeyIcon />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  onClick={() => {
                    setObfuscatedPassword(!obfuscatedPassword);
                  }}
                >
                  {obfuscatedPassword ? (
                    <VisibilityOffIcon />
                  ) : (
                    <VisibilityIcon />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          {...register("number")}
          id="number"
          label="Phone number"
          required
          error={Boolean(errors.number)}
          helperText={errors.number && errors.number.message}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <CallIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          {...register("dateBorn", {
            setValueAs: (value) => (value ? new Date(value) : undefined),
          })}
          name="dateBorn"
          label="Date of birth"
          type="date"
          required
          error={Boolean(errors.dateBorn)}
          helperText={errors.dateBorn && errors.dateBorn.message}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <CalendarMonthIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          {...register("location")}
          id="location"
          label="Location"
          required
          error={Boolean(errors.location)}
          helperText={errors.location && errors.location.message}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <LocationCityIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          {...register("about")}
          id="description"
          label="Description"
          multiline
          rows={4}
          error={Boolean(errors.about)}
          helperText={errors.about && errors.about.message}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AssignmentIcon />
              </InputAdornment>
            ),
          }}
        />
        <Grid container display={"flex"} alignItems={"center"}>
          <Grid item xs={2}>
            <Typography variant="body1" color="initial">
              Role:
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <RadioGroup
              
              defaultValue="EMPLOYEE"
              name="role"
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-evenly",
              }}
            >
              <FormControlLabel {...register("role")}
                value="EMPLOYEE"
                control={<Radio />}
                label="Employee"
              />
              <FormControlLabel {...register("role")}
                value="EMPLOYER"
                control={<Radio />}
                label="Employer"
              />
              {Boolean(errors.role) && <ErrorMessage text={errors.role?.message}/>}
            </RadioGroup>
          </Grid>
        </Grid>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "end",
          gap: 3,
          p: 2,
        }}
      >
        {registerUserMutation.isError && (
          <ErrorMessage text="Registration failed" />
        )}
        <Button color="error" onClick={onClose}>
          Close
        </Button>
        <Button
          variant="contained"
          type="submit"
          onClick={handleSubmit(handleRegister)}
        >
          Register
        </Button>
      </Box>
    </Dialog>
  );
};

export default RegisterDialog;
