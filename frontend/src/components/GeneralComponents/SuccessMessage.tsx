import { Typography } from "@mui/material";
import { FC } from "react";

interface SuccessMessageProps {
  text: string | undefined;
}

export const SuccessMessage: FC<SuccessMessageProps> = ({ text }) => {
  return <Typography color="green">{text}</Typography>;
};
