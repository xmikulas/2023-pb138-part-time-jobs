import React from 'react';
import { styled } from '@mui/system';
import { Avatar } from '@mui/material';

const Container = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '5vh',
  width: '5vh',
  borderRadius: '50%',
  overflow: 'hidden',
});

const Image = styled('img')({
  width: '100%',
  height: '100%',
  objectFit: 'cover',
});

interface RoundedImageProps {
    src: string,
    alt?: string
  }
  
const ProfilePictureAvatar: React.FC<RoundedImageProps> = ({ src, alt }) => {
    return (
      <Avatar src={src} alt={alt}/>
    );
  };
  
  export default ProfilePictureAvatar;