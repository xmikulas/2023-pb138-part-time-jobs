import React from 'react';
import { styled } from '@mui/system';

const Container = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '25vh',
  width: '25vh',
  borderRadius: '50%',
  overflow: 'hidden',
});

const Image = styled('img')({
  width: '100%',
  height: '100%',
  objectFit: 'cover',
});

interface RoundedImageProps {
    src: string,
    alt?: string
  }
  
const ProfilePicture: React.FC<RoundedImageProps> = ({ src, alt }) => {
    return (
      <Container>
        <Image src={src} alt={alt} />
      </Container>
    );
  };
  
  export default ProfilePicture;