import { Typography } from "@mui/material";
import { FC } from "react";

interface ErrorMessageProps {
  text: string | undefined;
}

export const ErrorMessage: FC<ErrorMessageProps> = ({ text }) => {
  return <Typography color="red">{text}</Typography>;
};
