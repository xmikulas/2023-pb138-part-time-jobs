import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import { Rating, TextField, Typography } from "@mui/material";
import { useState } from "react";
import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import config from "../../../../common/endponts.json";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";
import { numberToStars } from "../../types/Review";

interface ReviewDialogProps {
  open: boolean;
  onClose: () => void;
  name: string;
  employeeId: string;
}

const ReviewDialog: React.FC<ReviewDialogProps> = ({
  open,
  onClose,
  name,
  employeeId,
}) => {
  const queryClient = useQueryClient();
  const [starsRating, setStarsRating] = useState(0);
  const [comment, setComment] = useState("");
  const userId = useRecoilValue(userIdAtom);

  const updateRating = (newRating: number | null) => {
    if (newRating === null || newRating > 5 || newRating < 1) {
      setStarsRating(1);
    } else {
      setStarsRating(newRating);
    }
  };

  const rate = async (data: { comment: string; stars: string }) =>
    await axios.post(
      config.serverAddress + config.review.review,
      { ...data, reviewerId: userId, reviewedId: employeeId },
      {
        withCredentials: true,
      }
    );
 
  const rateMutation = useMutation({mutationFn: (data: { comment: string; stars: string }) =>
    rate(data), onSuccess: () => {queryClient.invalidateQueries(["getFilteredReviews"]); queryClient.invalidateQueries(["getUserSpecificReviews"]);}}
  );

  const handleRate = () => {
    rateMutation.mutate({
      comment: comment,
      stars: numberToStars[starsRating],
    });
  };

  return (
    <>
      <Dialog open={open} onClose={onClose} fullWidth maxWidth={"sm"}>
        <DialogTitle>Set Review for {name}</DialogTitle>
        <DialogContent
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            gap: 2,
          }}
        >
          <TextField
            id="review-comment"
            label="Comment"
            multiline
            fullWidth
            rows={5}
            inputProps={{ maxLength: 200 }}
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            sx={{ mt: 2 }}
          />
          <Typography component="legend">Rating: </Typography>
          <Rating
            name="rating"
            max={5}
            value={starsRating}
            onChange={(_, newValue) => updateRating(newValue)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="error">
            Cancel
          </Button>
          <Button onClick={() => {handleRate(); onClose();}} variant="contained" color="primary">
            Post rating
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ReviewDialog;
