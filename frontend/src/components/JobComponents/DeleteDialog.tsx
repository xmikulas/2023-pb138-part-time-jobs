import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import config from "../../../../common/endponts.json";

interface DeleteJobDialogProps {
  id: string;
  open: boolean;
  onClose: () => void;
  onDelete: () => void;
}

const DeleteJobDialog: React.FC<DeleteJobDialogProps> = ({
  id,
  open,
  onClose,
  onDelete,
}) => {
  const queryClient = useQueryClient();
  const deleteJob = async () =>
    await axios.delete(config.serverAddress + config.job.job + id);

    const mutation = useMutation(() => deleteJob(),
        { onSuccess: () => { queryClient.invalidateQueries(["getFilteredJobs"]);
                             queryClient.invalidateQueries(["getMyPostings"]);
        }}
    );

  const handleDelete = () => {
    mutation.mutate(undefined, {
      onSuccess: () => {
        onClose();
        onDelete();
      },
    });
  };
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogContent>
        <Typography variant="h6" color="initial">
          Delete this job ?
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button
          size="large"
          variant="contained"
          onClick={onClose}
          sx={{
            backgroundColor: "var(--fourthColor)",
            ":hover": { backgroundColor: "var(--hoverColor)" },
          }}
        >
          Cancel
        </Button>
        <Button
          size="large"
          variant="contained"
          onClick={handleDelete}
          sx={{
            backgroundColor: "var(--primaryColor)",
            ":hover": { backgroundColor: "var(--hoverColor)" },
          }}
        >
          Yes
        </Button>
      </DialogActions>
      {mutation.isIdle && mutation.isError && (
        <ErrorMessage text="Operation was not succesful" />
      )}
    </Dialog>
  );
};
export default DeleteJobDialog;
