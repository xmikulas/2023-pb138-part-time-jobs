import { Dispatch, SetStateAction, useState } from "react";
import { Toolbar, Box, Button, TextField, InputAdornment } from "@mui/material";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import { Clear } from "@mui/icons-material";
import { useQueryClient } from "react-query";
import { JobFilterType } from "../../types/Filter";

const textFieldStyles = {
  flex: 1,
  width: "100%",
  backgroundColor: "white",
  "& .MuiFilledInput-root": {
    "&:hover fieldset": {
      borderColor: "var(--primaryColor)",
    },
  },
  "& .MuiFilledInput-underline:after": {
    borderBottomColor: "var(--primaryColor)",
  },
};

const buttonStyle = {
  heigth: 50,
  backgroundColor: "var(--primaryColor)",
  "&:hover": { backgroundColor: "var(--hoverColor)" },
};

interface JobFilterBarProps extends JobFilterType {
  setFilter: Dispatch<SetStateAction<JobFilterType>>;
}

function JobFilter(props: JobFilterBarProps) {
  const queryClient = useQueryClient();
  const [title, setTitle] = useState("");
  const [wageFrom, setWageFrom] = useState("");
  const [wageTo, setWageTo] = useState("");
  const [hoursFrom, setHoursFrom] = useState("");
  const [hoursTo, setHoursTo] = useState("");
  const [location, setLocation] = useState("");

  const updateFilter = () => {
    const filter = {
      wageFrom: wageFrom === "" ? undefined : Number(wageFrom),
      wageTo: wageTo === "" ? undefined : Number(wageTo),
      location: location,
      hoursFrom: hoursFrom === "" ? undefined : Number(hoursFrom),
      hoursTo: hoursTo === "" ? undefined : Number(hoursTo),
      title,
    };
    console.log(filter);
    props.setFilter(filter);
    queryClient.invalidateQueries(["getFilteredJobs"]);
  };

  const clearFilters = () => {
    setTitle("");
    setHoursFrom("");
    setHoursTo("");
    setWageFrom("");
    setWageTo("");
    setLocation("");
    const filter = {
      wageFrom: undefined,
      wageTo: undefined,
      location: undefined,
      hoursFrom: undefined,
      hoursTo: undefined,
      title: undefined,
    };
    props.setFilter(filter);
  };

  const filtersEmpty = () => {
    return (
      title === "" &&
      wageFrom === "" &&
      wageTo === "" &&
      hoursFrom === "" &&
      hoursTo === "" &&
      location === ""
    );
  };
  return (
    <Toolbar sx={{ backgroundColor: "var(--secondaryColor)", padding: 2 }}>
      <Box
        sx={{
          display: "flex",
          flexDirection: {
            xs: "column",
            sm: "column",
            md: "column",
            lg: "row",
          },
          gap: "10px",
          width: "100%",
        }}
      >
        <TextField
          label="Title"
          variant="filled"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          sx={{
            flex: 1,
            backgroundColor: "white",
          }}
        />

        <Box
          sx={{
            display: "flex",
            flex: 1,
            gap: "10px",
          }}
        >
          <TextField
            label="Wage From"
            variant="filled"
            value={wageFrom}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">kč/h</InputAdornment>
              ),
            }}
            sx={textFieldStyles}
            onChange={(e) => setWageFrom(e.target.value)}
          />
          <TextField
            label="Wage To"
            variant="filled"
            value={wageTo}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">kč/h</InputAdornment>
              ),
            }}
            sx={textFieldStyles}
            onChange={(e) => setWageTo(e.target.value)}
          />
        </Box>
        <TextField
          label="Location"
          variant="filled"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
          sx={{
            flex: 1,
            backgroundColor: "white",
          }}
        />
        <Box
          sx={{
            display: "flex",
            flex: 1,
            gap: "10px",
          }}
        >
          <TextField
            label="Duration From"
            variant="filled"
            value={hoursFrom}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">hours</InputAdornment>
              ),
            }}
            sx={textFieldStyles}
            onChange={(e) => setHoursFrom(e.target.value)}
          />
          <TextField
            label="Duration To"
            variant="filled"
            value={hoursTo}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">hours</InputAdornment>
              ),
            }}
            sx={textFieldStyles}
            onChange={(e) => setHoursTo(e.target.value)}
          />
        </Box>
        <Button variant="contained" onClick={updateFilter} sx={buttonStyle}>
          <FilterAltIcon />
          Filter
        </Button>
        <Button
          variant="contained"
          onClick={clearFilters}
          disabled={filtersEmpty()}
          sx={buttonStyle}
        >
          <Clear />
          Clear
        </Button>
      </Box>
    </Toolbar>
  );
}

export default JobFilter;
