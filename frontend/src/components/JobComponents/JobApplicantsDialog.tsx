import React, { useState } from "react";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import config from "../../../../common/endponts.json";
import Avatar from "@mui/material/Avatar";
import axios from "axios";
import { useQuery } from "react-query";
import { PaginationType } from "../../types/Pagination";
import { parsePagination } from "../../utils/QueryParameters";
import { User } from "../../types/User";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";
import { Job } from "../../types/Job";
import { useNavigate } from "react-router-dom";

interface JobApplicantsDialogProps {
  open: boolean;
  onClose: () => void;
  job: Job;
}

const JobApplicantsDialog: React.FC<JobApplicantsDialogProps> = ({
  open,
  onClose,
  job,
}) => {
  const navigate = useNavigate();
  const userId = useRecoilValue(userIdAtom);

  const getMyPostings = async (pagination: PaginationType) => {
    const paginationParams = "?" + parsePagination(pagination);
    return await axios.get<{ data: { array: User[]; count: number } }>(
      config.serverAddress +
        config.user.getReactions +
        job.id +
        paginationParams
    );
  };

  const query = useQuery(
    ["getReactions"],
    () => getMyPostings({ take: 100, skip: 0 }),
    {
      enabled: userId !== "",
    }
  );

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle sx={{ display: "flex", alignItems: "center" }}>
        {`${job.title}'s applicants`}
      </DialogTitle>
      <DialogContent
        sx={{
          maxHeight: 300,
          overflowY: "auto",
          p: 3,
          display: "flex",
          flexDirection: "column",
          gap: 2,
        }}
      >
        {query.data?.data.data.array &&
        query.data.data.data.array.length > 0 ? (
          query.data.data.data.array.map((employee, index) => (
            <Box
              key={index}
              sx={{
                display: "flex",
                alignItems: "center",
                width: 350,
                justifyContent: "space-between",
                p: 2,
                border: "1px solid #ccc",
              }}
            >
              <Avatar
                src={
                  config.serverAddress +
                  config.files.getFiles +
                  employee.id +
                  "/type/PIC"
                }
                alt={`${employee.name}  ${employee.surname}`}
                sx={{ width: "48px", height: "48px", mr: "16px" }}
              />
              <Box
                display={"flex"}
                flexDirection={"column"}
                justifyContent={"center"}
              >
                <Typography variant="h6" align="left">
                  {employee.name + " " + employee.surname}
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  Age: {2023 - new Date(employee.dateBorn).getFullYear()}
                </Typography>
              </Box>
              <Button
                variant="contained"
                onClick={() => navigate(`/employee/${employee.id}`)}
              >
                Profile
              </Button>
            </Box>
          ))
        ) : (
          <Typography variant="h6" align="left">
            No applicants
          </Typography>
        )}
      </DialogContent>
      <DialogActions>
        <Button color="error" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default JobApplicantsDialog;
