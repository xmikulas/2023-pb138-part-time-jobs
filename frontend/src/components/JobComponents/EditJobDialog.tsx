import React from "react";
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  TextField,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import PlaceIcon from "@mui/icons-material/Place";
import WorkIcon from "@mui/icons-material/Work";
import WatchLaterIcon from "@mui/icons-material/WatchLater";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import InfoIcon from "@mui/icons-material/Info";
import axios from "axios";
import { userIdAtom } from "../../recoilAtoms";
import { useRecoilValue } from "recoil";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import config from "../../../../common/endponts.json";
import { useQueryClient, useMutation } from "react-query";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";

const LabeledTextFieldStyle = {
  display: "flex",
  alignItems: "center",
  p: 1,
  gap: 2,
};

const editJobValidationSchema = z.object({
  title: z.string().nonempty(),
  location: z.string().nonempty(),
  wage: z.number().positive(),
  description: z.string(),
  numOfHour: z.number().positive(),
});

type EditJobValidationSchema = z.infer<typeof editJobValidationSchema>;

interface EditJobDialogProps {
  id: string;
  open: boolean;
  onClose: () => void;
  job: EditJobValidationSchema;
  onSuccess: () => void;
}

const EditJobDialog: React.FC<EditJobDialogProps> = ({
  id,
  open,
  onClose,
  job,
  onSuccess,
}) => {
  const queryClient = useQueryClient();
  const userId = useRecoilValue(userIdAtom);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<EditJobValidationSchema>({
    resolver: zodResolver(editJobValidationSchema),
    defaultValues: { ...job } || {},
  });

    const editJob = async (job: EditJobValidationSchema) => 
        await axios.patch(config.serverAddress + config.job.job + id, {...job, employerId: userId});

    const mutation = useMutation({mutationFn: (job: EditJobValidationSchema) => editJob(job),
        onSuccess: () => { queryClient.invalidateQueries(["getFilteredJobs"]);  queryClient.invalidateQueries(["getMyPostings"]);}}
    );

  const handleEditJob = (data: EditJobValidationSchema) => {
    mutation.mutate(data);
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Edit Job</DialogTitle>
      <DialogContent>
        <Box sx={LabeledTextFieldStyle}>
          <WorkIcon />
          <TextField
            {...register("title")}
            label="Title"
            name="title"
            fullWidth
            error={Boolean(errors.title)}
            helperText={errors.title && errors.title.message}
            sx={{ width: "100%" }}
          />
        </Box>
        <Box sx={LabeledTextFieldStyle}>
          <PlaceIcon />
          <TextField
            {...register("location")}
            label="Location"
            name="location"
            fullWidth
            error={Boolean(errors.location)}
            helperText={errors.location && errors.location.message}
            sx={{ width: "100%" }}
          />
        </Box>
        <Box sx={LabeledTextFieldStyle}>
          <WatchLaterIcon />
          <TextField
            {...register("numOfHour", { valueAsNumber: true })}
            name="numOfHour"
            label="Duration"
            type="number"
            fullWidth
            error={Boolean(errors.numOfHour)}
            helperText={errors.numOfHour && errors.numOfHour.message}
            sx={{ width: "100%" }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">hours</InputAdornment>
              ),
            }}
          />
        </Box>
        <Box sx={LabeledTextFieldStyle}>
          <MonetizationOnIcon />
          <TextField
            {...register("wage", { valueAsNumber: true })}
            name="wage"
            label="Wage"
            type="wage"
            fullWidth
            error={Boolean(errors.wage)}
            helperText={errors.wage && errors.wage.message}
            sx={{ width: "100%" }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">kč/h</InputAdornment>
              ),
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            alignItems: "center",
            gap: 20,
            width: 500,
            padding: 5,
          }}
        >
          <InfoIcon />
          <TextField
            {...register("description")}
            label="Description"
            name="description"
            fullWidth
            multiline
            rows={4}
            error={Boolean(errors.description)}
            helperText={errors.description && errors.description.message}
            inputProps={{ maxLength: 380 }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            justifyContent: "space-evenly",
          }}
        >
          <Button color="error" onClick={onClose}>
            Close
          </Button>
          <Button
            size="medium"
            variant="contained"
            onClick={() => {handleSubmit(handleEditJob)(); onClose()}}
            sx={{
              backgroundColor: "var(--primaryColor)",
              ":hover": { backgroundColor: "var(--hoverColor)" },
            }}
          >
            Edit
          </Button>
        </Box>
        <div>
          {mutation.isIdle && mutation.isError && (
            <ErrorMessage text="Operation was not succesful" />
          )}
          {mutation.isIdle && mutation.isSuccess && (
            <ErrorMessage text="Saved" />
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default EditJobDialog;
