import React from "react";
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  InputAdornment,
  TextField,
} from "@mui/material";
import PlaceIcon from "@mui/icons-material/Place";
import WorkIcon from "@mui/icons-material/Work";
import WatchLaterIcon from "@mui/icons-material/WatchLater";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import InfoIcon from "@mui/icons-material/Info";
import { z } from "zod";
import axios, { AxiosError } from "axios";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import { useQueryClient, useMutation } from "react-query";
import config from "../../../../common/endponts.json";

interface AddJobDialogProps {
  open: boolean;
  onClose: () => void;
}

const jobCreateSchema = z.object({
  title: z.string().nonempty(),
  description: z.string().nonempty(),
  wage: z.number().positive(),
  location: z.string().nonempty(),
  numOfHour: z.number().positive(),
  validTill: z.date().optional(),
});

type JobValidationSchema = z.infer<typeof jobCreateSchema>;

const AddJobDialog: React.FC<AddJobDialogProps> = ({ open, onClose }) => {
  const queryClient = useQueryClient();
  const userId = useRecoilValue(userIdAtom);

  const createJob = async (newJob: JobValidationSchema) =>
    await axios.post(config.serverAddress + config.job.job, {
      employerId: userId,
      ...newJob,
    });

  const addJobMutation = useMutation(
    (newJob: JobValidationSchema) => createJob(newJob),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["getMyPostings"]);
      },
      onError: (error: AxiosError) => {
        console.log(error.response?.data);
      },
    }
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<JobValidationSchema>({
    resolver: zodResolver(jobCreateSchema),
  });

  const addJob = (data: JobValidationSchema) => {
    addJobMutation.mutate(data, {
      onSuccess: () => {
        onClose();
        console.log("suces");
      },
      onError: () => {
        console.log("error");
      },
    });
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Add Job</DialogTitle>
      <DialogContent sx={{ display: "flex", flexDirection: "column" }}>
        <Box sx={{ display: "flex", alignItems: "center", gap: 2, padding: 1 }}>
          <WorkIcon />
          <TextField
            {...register("title")}
            name="title"
            label="Title"
            fullWidth
            error={Boolean(errors.title)}
            helperText={errors.title && errors.title.message}
            sx={{ width: "100%" }}
          />
        </Box>
        <Box sx={{ display: "flex", alignItems: "center", gap: 2, padding: 1 }}>
          <PlaceIcon />
          <TextField
            {...register("location")}
            name="location"
            label="Location"
            fullWidth
            error={Boolean(errors.location)}
            helperText={errors.location && errors.location.message}
            sx={{ width: "100%" }}
          />
        </Box>
        <Box sx={{ display: "flex", alignItems: "center", gap: 2, padding: 1 }}>
          <WatchLaterIcon />
          <TextField
            {...register("numOfHour", { valueAsNumber: true })}
            name="numOfHour"
            label="Duration"
            fullWidth
            type="number"
            error={Boolean(errors.numOfHour)}
            helperText={errors.numOfHour && errors.numOfHour.message}
            sx={{ width: "100%" }}
          />
        </Box>
        <Box sx={{ display: "flex", alignItems: "center", gap: 2, padding: 1 }}>
          <MonetizationOnIcon />
          <TextField
            {...register("wage", { valueAsNumber: true })}
            name="wage"
            label="Wage"
            fullWidth
            type="number"
            error={Boolean(errors.wage)}
            helperText={errors.wage && errors.wage.message}
            sx={{ width: "100%" }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="start">kč/h</InputAdornment>
              ),
            }}
          />
        </Box>
        <Box sx={{ display: "flex", alignItems: "center", gap: 2, padding: 1 }}>
          <InfoIcon />
          <TextField
            {...register("description")}
            name="description"
            label="Description"
            rows={4}
            fullWidth
            inputProps={{ maxLength: 200 }}
            error={Boolean(errors.description)}
            helperText={errors.description && errors.description.message}
            sx={{ width: "100%" }}
          />
        </Box>
        <Button variant="contained" onClick={handleSubmit(addJob)}>
          Add job
        </Button>
        {addJobMutation.isError && <ErrorMessage text="Unable to create job" />}
      </DialogContent>
    </Dialog>
  );
};

export default AddJobDialog;
