import React, { useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  Divider,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Chip from "@mui/material/Chip";
import PaidIcon from "@mui/icons-material/Paid";
import WorkHistoryIcon from "@mui/icons-material/WorkHistory";
import CloseIcon from "@mui/icons-material/Close";
import LocationCityIcon from "@mui/icons-material/LocationCity";
import Button from "@mui/material/Button";
import DeleteDialog from "./DeleteDialog";
import EditJobDialog from "./EditJobDialog";
import { useRecoilValue } from "recoil";
import { isAuthenticatedAtom, isEmployer, userIdAtom } from "../../recoilAtoms";
import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import { SuccessMessage } from "../GeneralComponents/SuccessMessage";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import config from "../../../../common/endponts.json";

const buttonStyle = {
  backgroundColor: "var(--primaryColor)",
  ":hover": { backgroundColor: "var(--hoverColor)" },
};

interface JobDialogProps {
  open: boolean;
  onClose: () => void;
  job: {
    id: string;
    location: string;
    numOfHour: number;
    title: string;
    description: string;
    wage: number;
    employerId: string;
  };
  canApply: boolean;
}

type ApplyJob = {
  userId: string;
  jobId: string;
};

const JobDialog: React.FC<JobDialogProps> = ({
  open,
  onClose,
  job,
  canApply,
}) => {
  const queryClient = useQueryClient();
  const [openInnerDialog, setOpenInnerDialog] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const loggedIn = useRecoilValue(isAuthenticatedAtom);
  const employer = useRecoilValue(isEmployer);
  const userId = useRecoilValue(userIdAtom);

  const handleCloseInnerDialog = () => {
    setOpenInnerDialog(false);
    onClose();
  };

  axios.defaults.withCredentials = true;
  const apply = async (data: ApplyJob) =>
    await axios.post(config.serverAddress + config.sign.sign, data, {
      withCredentials: true,
    });

  const applyMutation = useMutation((data: ApplyJob) => apply(data));

  const handleApplyJob = () => {
    applyMutation.mutate(
      { userId: userId, jobId: job.id },
      {
        onSuccess() {
          queryClient.invalidateQueries("getReactions");
        },
      }
    );
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>
        Job Details
        <IconButton
          color="inherit"
          onClick={onClose}
          aria-label="close"
          sx={{ position: "absolute", right: 8, top: 8 }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={{ minHeight: 200, minWidth: 300, maxWidth: 400 }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: 20,
          }}
        >
          <Avatar
            variant="square"
            sx={{ height: "100px", width: "100px" }}
            src={
              config.serverAddress +
              config.files.getFiles +
              job.employerId +
              "/type/PIC"
            }
          />
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "end",
              gap: 10,
            }}
          >
            <Chip
              icon={<LocationCityIcon />}
              label={job.location}
              sx={{ fontSize: "20px" }}
            />
            <Chip
              icon={<WorkHistoryIcon />}
              label={`${job.numOfHour} hours`}
              sx={{ fontSize: "20px" }}
            />
            <Chip
              icon={<PaidIcon />}
              label={`${job.wage}kč/h`}
              sx={{ fontSize: "20px" }}
            />
          </div>
        </div>
        <Typography variant="h5">{job.title}</Typography>
        <Divider />
        <Typography variant="body2" color="text.secondary" align="justify">
          {job.description}
        </Typography>
      </DialogContent>
      <DialogActions sx={{ backgroundColor: "var(--secondaryColor)" }}>
        <Grid container justifyContent="center" spacing={2}>
          {userId === job.employerId && (
            <>
              <Grid item>
                <Button
                  size="large"
                  variant="contained"
                  onClick={() => setOpenDeleteDialog(true)}
                  sx={buttonStyle}
                >
                  Delete
                </Button>
              </Grid>
              <Grid item>
                <Button
                  size="large"
                  variant="contained"
                  onClick={() => setOpenEditDialog(true)}
                  sx={buttonStyle}
                >
                  Edit
                </Button>
              </Grid>
            </>
          )}
          {!employer && loggedIn && canApply && (
            <Grid item>
              <Button
                size="large"
                variant="contained"
                disabled={employer || !loggedIn}
                onClick={() => setOpenInnerDialog(true)}
                sx={buttonStyle}
              >
                Apply
              </Button>
            </Grid>
          )}
        </Grid>
      </DialogActions>
      <Dialog open={openInnerDialog} onClose={handleCloseInnerDialog}>
        <DialogTitle sx={{ display: "flex", flexDirection: "row" }}>
          <Typography variant="h6" color="initial">
            {`You are applying for \n ${job.title}`}
          </Typography>
        </DialogTitle>
        <DialogContent>
          {applyMutation.isError && (
            <ErrorMessage text="You have already applied for a job!" />
          )}
          {applyMutation.isSuccess && (
            <SuccessMessage text="You have successfully applied for a job!" />
          )}
        </DialogContent>
        <DialogActions>
          <Grid container justifyContent="center" spacing={2}>
            <Grid item>
              <Button
                size="medium"
                variant="contained"
                onClick={handleCloseInnerDialog}
                sx={buttonStyle}
              >
                Close
              </Button>
            </Grid>
            <Grid item>
              <Button
                size="medium"
                variant="contained"
                onClick={handleApplyJob}
                sx={buttonStyle}
              >
                Send
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
      {openDeleteDialog && (
        <DeleteDialog
          id={job.id}
          open={openDeleteDialog}
          onClose={() => setOpenDeleteDialog(false)}
          onDelete={onClose}
        />
      )}
      {openEditDialog && (
        <EditJobDialog
          id={job.id}
          open={openEditDialog}
          onClose={() => setOpenEditDialog(false)}
          onSuccess={onClose}
          job={job}
        />
      )}
    </Dialog>
  );
};

export default JobDialog;
