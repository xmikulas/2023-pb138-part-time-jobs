import { useState } from "react";
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import PaidIcon from "@mui/icons-material/Paid";
import JobApplicantsDialog from "./JobApplicantsDialog";
import { useRecoilValue } from "recoil";
import { isEmployer, userIdAtom } from "../../recoilAtoms";
import JobDialog from "./JobDialog";
import config from "../../../../common/endponts.json";
import { useQueryClient } from "react-query";

interface JobCardProps {
  job: {
    id: string;
    location: string;
    numOfHour: number;
    title: string;
    description: string;
    employerId: string;
    wage: number;
  };
  canApply: boolean;
}

const JobCard: React.FC<JobCardProps> = ({ job, canApply }) => {
  const queryClient = useQueryClient();
  const [openViewMore, setOpenViewMore] = useState(false);
  const [openApplicants, setOpenApplicants] = useState(false);
  const userId = useRecoilValue(userIdAtom);
  const employer = useRecoilValue(isEmployer);

  const handleCloseApplicantsModal = () => {
    queryClient.invalidateQueries("getReactions");
    setOpenApplicants(false);
  };

  return (
    <Card
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "var(--thirdColor)",
        border: "2px solid black",
        height: "400px",
      }}
    >
      {job?.employerId && (
        <>
          <CardHeader
            avatar={
              <Avatar
                variant="square"
                sx={{ height: "100px", width: "100px" }}
                src={
                  config.serverAddress +
                  config.files.getFiles +
                  job.employerId +
                  "/type/PIC"
                }
              />
            }
            titleTypographyProps={{ variant: "h6" }}
            title={`Location: ${job.location}`}
            subheader={`Duration: ${job.numOfHour} hours`}
          />
          <CardContent
            sx={{
              flexGrow: 1,
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <div>
              <Typography variant="h5">{job.title}</Typography>
              <Divider />
              <Typography
                variant="body2"
                color="text.secondary"
                align="justify"
              >
                {job.description}
              </Typography>
            </div>
            <div>
              <Grid
                container
                justifyContent="center"
                sx={{ padding: "15px 0px" }}
              >
                <Chip
                  icon={<PaidIcon />}
                  label={`${job.wage}kč/h`}
                  sx={{
                    fontSize: "20px",
                  }}
                />
              </Grid>
            </div>
          </CardContent>
          <JobDialog
            open={openViewMore}
            onClose={() => setOpenViewMore(false)}
            job={job}
            canApply={canApply}
          />
          <JobApplicantsDialog
            open={openApplicants}
            onClose={handleCloseApplicantsModal}
            job={job}
          />
          <CardActions>
            {job.employerId === userId && employer && (
              <Button
                variant="contained"
                sx={{
                  backgroundColor: "var(--primaryColor)",
                  ":hover": { backgroundColor: "var(--hoverColor)" },
                  marginLeft: "auto",
                }}
                onClick={() => setOpenApplicants(true)}
              >
                Applicants
              </Button>
            )}
            <Button
              variant="contained"
              sx={{
                backgroundColor: "var(--primaryColor)",
                ":hover": { backgroundColor: "var(--hoverColor)" },
                marginLeft: "auto",
              }}
              onClick={() => setOpenViewMore(true)}
            >
              View more
            </Button>
          </CardActions>
        </>
      )}
    </Card>
  );
};

export default JobCard;
