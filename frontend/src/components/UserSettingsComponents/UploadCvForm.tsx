import {
  Box,
  Button,
  Card,
  CardHeader,
  CardContent,
  CardActions,
} from "@mui/material";
import { FC, useState } from "react";
import axios from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import config from "../../../../common/endponts.json";
import { SuccessMessage } from "../GeneralComponents/SuccessMessage";

interface UploadCvFormProps {
  userId: string;
}

const UploadCvForm: FC<UploadCvFormProps> = ({ userId }) => {
  const queryClient = useQueryClient();

  const [notUploaded, setNotUploaded] = useState(false);
  const [uploaded, setUploaded] = useState(false);
  const [selectedFile, setSelectedFile] = useState<File | undefined>(undefined);

  const uploadFile = async (file: File) => {
    const formData = new FormData();
    formData.append("document", file);

    return await axios.post(
      config.serverAddress + config.files.getFiles + userId + "/type/CV",
      formData,
      { headers: { "Content-Type": "multipart/form-data" } }
    );
  };

  const uploadFileMutation = useMutation(uploadFile, {
    onError: () => {
      setNotUploaded(true);
      setUploaded(false);
      queryClient.invalidateQueries("getCV");
    },
    onSuccess: () => {
      setUploaded(true);
      setNotUploaded(false);
    },
  });

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    setSelectedFile(file);
  };

  const uploadFileAction = async () => {
    if (selectedFile) {
      uploadFileMutation.mutateAsync(selectedFile);
    }
  };

  const deleteCV = async () =>
    await axios.delete(
      config.serverAddress + config.files.getFiles + userId + "/type/CV"
    );

  const deleteCvMutation = useMutation(() => deleteCV(), {
    onSuccess: () => {
      setNotUploaded(false);
      setUploaded(false);
      queryClient.invalidateQueries(["getCV"]);
    },
  });

  const deleteProfilePictureAction = () => {
    deleteCvMutation.mutate(undefined);
  };

  const getCv = async () => {
    await axios.get(
      config.serverAddress + config.files.getFiles + userId + "/type/CV"
    );
  };

  const query = useQuery("getCV", getCv, { retry: false });

  return (
    <form>
      <Card
        sx={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <CardHeader title="CV" sx={{ alignSelf: "center" }} />
        <CardContent sx={{ display: "flex", flexDirection: "column" }}>
          <Box sx={{ alignSelf: "center", p: 2 }}>
            <Button
              variant={selectedFile ? "contained" : "outlined"}
              component="label"
            >
              {selectedFile ? "CV Selected" : "Select CV"}
              <input type="file" hidden onChange={handleFileChange} />
            </Button>
          </Box>
        </CardContent>
        <CardActions sx={{ display: "flex", justifyContent: "center" }}>
          <Button
            variant="contained"
            disabled={!selectedFile}
            onClick={uploadFileAction}
          >
            Upload
          </Button>
          <Button
            variant="contained"
            disabled={query.status !== "success"}
            onClick={deleteProfilePictureAction}
          >
            Delete
          </Button>
        </CardActions>
      </Card>
      {notUploaded && <ErrorMessage text="Upload not succesful" />}
      {uploaded && <SuccessMessage text="Upload succesful" />}
    </form>
  );
};

export default UploadCvForm;
