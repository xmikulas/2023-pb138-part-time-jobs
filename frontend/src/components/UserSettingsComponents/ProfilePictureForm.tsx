import {
  Box,
  Button,
  Card,
  Typography,
  CardHeader,
  Avatar,
  CardContent,
  CardActions,
} from "@mui/material";
import { FC, useState } from "react";
import axios from "axios";
import { useMutation, useQuery, useQueryClient } from "react-query";

import config from "../../../../common/endponts.json";

interface ProfilePictureFormProps {
  userId: string;
}

const ProfilePictureForm: FC<ProfilePictureFormProps> = ({ userId }) => {
  const queryClient = useQueryClient();

  const [notUploaded, setNotUploaded] = useState(false);
  const [selectedFile, setSelectedFile] = useState<File | undefined>(undefined);

  const uploadFile = async (file: File) => {
    const formData = new FormData();
    formData.append("document", file);

    return await axios.post(
      config.serverAddress + config.files.getFiles + userId + "/type/PIC",
      formData,
      { headers: { "Content-Type": "multipart/form-data" } }
    );
  };

  const uploadFileMutation = useMutation(uploadFile, {
    onError: () => {
      setNotUploaded(true);
    },
    onSuccess: () => {
      setNotUploaded(false);
    },
  });

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    setSelectedFile(file);
  };

  const uploadFileAction = async () => {
    if (selectedFile) {
      uploadFileMutation.mutateAsync(selectedFile, {
        onSuccess: () => {
          queryClient.invalidateQueries(["getPhoto"]);
        },
      });
    }
  };

  const deletePhoto = async () =>
    await axios.delete(
      config.serverAddress + config.files.getFiles + userId + "/type/PIC"
    );

  const deletePhotoMutation = useMutation(() => deletePhoto(), {
    onSuccess: () => {
      queryClient.invalidateQueries(["getPhoto"]);
    },
  });

  const deleteProfilePictureAction = () => {
    deletePhotoMutation.mutate(undefined);
  };

  const getPhoto = async () => {
    await axios.get(
      config.serverAddress + config.files.getFiles + userId + "/type/PIC"
    );
  };

  const query = useQuery("getPhoto", getPhoto, { retry: false });

  return (
    <form>
      <Card
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <CardHeader title="Profile picture" sx={{ alignSelf: "center" }} />
        <CardContent sx={{ display: "flex", flexDirection: "column" }}>
          <Avatar
            sx={{ alignSelf: "center", width: 100, height: 100 }}
            src={
              query.isSuccess
                ? `${config.serverAddress}${config.files.getFiles}${userId}/type/PIC`
                : ""
            }
          />
          <Box sx={{ alignSelf: "center", p: 2 }}>
            <Button
              variant={selectedFile ? "contained" : "outlined"}
              component="label"
            >
              {selectedFile ? "Picture Selected" : "Select Picture"}
              <input type="file" hidden onChange={handleFileChange} />
            </Button>
          </Box>
          <CardActions sx={{ display: "flex", justifyContent: "center" }}>
            <Button
              variant="contained"
              disabled={!selectedFile}
              onClick={uploadFileAction}
            >
              Upload
            </Button>
            <Button
              variant="contained"
              disabled={query.status !== "success"}
              onClick={deleteProfilePictureAction}
            >
              Delete
            </Button>
          </CardActions>
          {query.isLoading && <Typography>Loading...</Typography>}
        </CardContent>
      </Card>
    </form>
  );
};

export default ProfilePictureForm;
