import { zodResolver } from "@hookform/resolvers/zod";
import {
  Grid,
  Typography,
  InputLabel,
  TextField,
  Card,
  Button,
} from "@mui/material";
import axios from "axios";
import { FC, useState } from "react";
import { useForm } from "react-hook-form";
import { useQueryClient, useMutation } from "react-query";
import { z } from "zod";
import { UserChangePasswordType } from "../../types/User";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import { SuccessMessage } from "../GeneralComponents/SuccessMessage";
import config from "../../../../common/endponts.json";

interface ChangePasswordProps {
  userId: string;
}

const changePasswordValidationSchema = z.object({
  oldPassword: z.string().nonempty(),
  newPassword: z.string().nonempty(),
  confirmNewPassword: z.string().nonempty(),
});

type ChangePasswordValidationSchema = z.infer<
  typeof changePasswordValidationSchema
>;

const ChangePasswordForm: FC<ChangePasswordProps> = ({ userId }) => {
  const [userNotVerified, setUserNotVerified] = useState(false);
  const [passwordsDontMatch, setPasswordDontMatch] = useState(false);
  const [newPassword, setNewPassword] = useState("");

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<ChangePasswordValidationSchema>({
    resolver: zodResolver(changePasswordValidationSchema),
  });

  const queryClient = useQueryClient();

  const verifyUserPassword = async (newUser: {
    id: string;
    password: string;
  }) => await axios.post(config.serverAddress + config.auth.verify, newUser);

  const tryToChangePassword = useMutation(
    (newUser: UserChangePasswordType) =>
      verifyUserPassword({ id: newUser.id, password: newUser.oldPassword }),
    {
      onSuccess: () => {
        setUserNotVerified(false);
        queryClient.invalidateQueries("getUser");
        changePassword.mutate({ id: userId, password: newPassword });
      },
      onError: () => {
        setUserNotVerified(true);
      },
    }
  );

  const patchUser = async (newUser: { id: string; password: string }) =>
    await axios.patch(config.serverAddress + config.user.user, newUser);
  const changePassword = useMutation(
    (newUser: { id: string; password: string }) => patchUser(newUser),
    {
      onSuccess: () => {
        reset();
      },
    }
  );

  const onSubmit = async (data: ChangePasswordValidationSchema) => {
    if (data.newPassword !== data.confirmNewPassword) {
      setPasswordDontMatch(true);
    } else {
      setNewPassword(data.newPassword);
      setPasswordDontMatch(false);
      tryToChangePassword.mutate({
        id: userId,
        oldPassword: data.oldPassword,
        newPassword: data.newPassword,
      });
    }
  };

  return (
    <form>
      <Card sx={{ p: 2, display: "flex", flexDirection: "column", gap: 1 }}>
        <Typography variant="h5" marginTop={2}>
          Change password
        </Typography>
        <TextField
          {...register("oldPassword")}
          name="oldPassword"
          label="Old Password"
          fullWidth
          type="password"
          error={Boolean(errors.oldPassword)}
          helperText={errors.oldPassword && errors.oldPassword.message}
        />
        <TextField
          {...register("newPassword")}
          name="newPassword"
          type="password"
          label="New Password"
          fullWidth
          error={Boolean(errors.newPassword)}
          helperText={errors.newPassword && errors.newPassword.message}
        />
        <TextField
          {...register("confirmNewPassword")}
          type="password"
          name="confirmNewPassword"
          label="Confirm New Password"
          fullWidth
          error={Boolean(errors.confirmNewPassword)}
          helperText={
            errors.confirmNewPassword && errors.confirmNewPassword.message
          }
        />

        <Button
          variant="contained"
          disabled={changePassword.isLoading || tryToChangePassword.isLoading}
          onClick={handleSubmit(onSubmit)}
        >
          Change password
        </Button>
        <Grid item xs={12}>
          {passwordsDontMatch && <ErrorMessage text="Passwords don't match" />}
          {!passwordsDontMatch &&
            userNotVerified &&
            !tryToChangePassword.isLoading && (
              <ErrorMessage text="Wrong password" />
            )}
          {!passwordsDontMatch &&
            !userNotVerified &&
            tryToChangePassword.isError && (
              <ErrorMessage text="Operation was not succesful" />
            )}
          {(changePassword.isLoading || tryToChangePassword.isLoading) && (
            <Typography>Sending data...</Typography>
          )}
          {tryToChangePassword.isSuccess && changePassword.isSuccess && (
            <SuccessMessage text="Password changed" />
          )}
        </Grid>
      </Card>
    </form>
  );
};

export default ChangePasswordForm;
