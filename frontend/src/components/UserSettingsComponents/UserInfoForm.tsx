import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { FC, useState } from "react";
import { Typography, TextField, Button, Box } from "@mui/material";
import validator from "validator";
import { UserUpdateType } from "../../types/User";
import axios, { AxiosError } from "axios";
import { useMutation, useQueryClient } from "react-query";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";
import { SuccessMessage } from "../GeneralComponents/SuccessMessage";
import config from "../../../../common/endponts.json";

interface UserInfoFormProps {
  user: UserUpdateType;
}

const userInfoValidationSchema = z.object({
  name: z.string().nonempty(),
  surname: z.string().nonempty(),
  number: z
    .string()
    .nonempty()
    .refine((value) => validator.isMobilePhone(value), "Invalid phone number"),
  about: z.string(),
  expectedWage: z.number().positive(),
  expectedHours: z.number().positive(),
  dateBorn: z.date(),
});

type UserInfoValidationSchema = z.infer<typeof userInfoValidationSchema>;

const UserInfoForm: FC<UserInfoFormProps> = ({ user }) => {
  const [dateBorn, setDateBorn] = useState<string>(
    new Date(user.dateBorn).toISOString().split("T")[0]
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UserInfoValidationSchema>({
    resolver: zodResolver(userInfoValidationSchema),
    defaultValues: { ...user } || {},
  });

  const queryClient = useQueryClient();
  const patchUser = async (newUser: UserUpdateType) =>
    await axios.patch(config.serverAddress + config.user.user, newUser);

  const mutation = useMutation(
    (newUser: UserUpdateType) => patchUser(newUser),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["getUser"]);
      },
      onError: (error: AxiosError) => {
        console.log(error.response?.data);
      },
    }
  );

  const onSubmit = async (data: UserInfoValidationSchema) => {
    mutation.mutate({ ...data, id: user.id, email: user.email });
  };

  return (
    <>
      <Typography variant="h5" align="center">
        General information
      </Typography>
      <Box display={"flex"} flexDirection={"column"} gap={2}>
        <TextField
          {...register("name")}
          name="name"
          label="Name"
          fullWidth
          error={Boolean(errors.name)}
          helperText={errors.name && errors.name.message}
        />

        <TextField
          {...register("surname")}
          name="surname"
          label="Surname"
          fullWidth
          error={Boolean(errors.surname)}
          helperText={errors.surname && errors.surname.message}
        />
        <TextField
          {...register("number")}
          name="number"
          label="Phone Number"
          fullWidth
          error={Boolean(errors.number)}
          helperText={errors.number && errors.number.message}
        />

        <TextField
          {...register("about")}
          name="about"
          label="About"
          multiline
          rows={4}
          fullWidth
          error={Boolean(errors.about)}
          helperText={errors.about && errors.about.message}
        />
        <Box>
          <Typography variant="caption" color="initial">
            Expected Wage
          </Typography>
          <TextField
            {...register("expectedWage", { valueAsNumber: true })}
            name="expectedWage"
            type="number"
            fullWidth
            error={Boolean(errors.expectedWage)}
            helperText={errors.expectedWage && errors.expectedWage.message}
          />
        </Box>
        <Box>
          <Typography variant="caption" color="initial">
            Expected Hours
          </Typography>
          <TextField
            {...register("expectedHours", { valueAsNumber: true })}
            name="expectedHours"
            type="number"
            fullWidth
            error={Boolean(errors.expectedHours)}
            helperText={errors.expectedHours && errors.expectedHours.message}
          />
        </Box>
        <TextField
          {...register("dateBorn", {
            setValueAs: (value) => (value ? new Date(value) : undefined),
          })}
          name="dateBorn"
          label="Date Born"
          type="date"
          fullWidth
          error={Boolean(errors.dateBorn)}
          helperText={errors.dateBorn && errors.dateBorn.message}
          value={dateBorn}
          onChange={(e) => setDateBorn(e.target.value)}
        />
        <Button
          variant="contained"
          disabled={mutation.isLoading}
          onClick={handleSubmit(onSubmit)}
        >
          Submit
        </Button>
      </Box>
      {mutation.isError && <ErrorMessage text="Unable to save data" />}
      {mutation.isLoading && <Typography>Sending data...</Typography>}
      {mutation.isSuccess && <SuccessMessage text="Saved" />}
    </>
  );
};

export default UserInfoForm;
