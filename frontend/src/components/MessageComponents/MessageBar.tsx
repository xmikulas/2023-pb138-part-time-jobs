import { Box, TextField, IconButton, Grid } from "@mui/material";
import SendIcon from "@mui/icons-material/Send";
import { useState } from "react";
import { CreateMessage } from "../../types/Message";
import axios from "axios";
import config from "../../../../common/endponts.json";
import { useMutation, useQueryClient } from "react-query";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";

type MessageBarProps = {
  contractId: string;
}

const MessageBar: React.FC<MessageBarProps> = ({contractId}) => {
  const senderId = useRecoilValue(userIdAtom)
  const [message, setMessage] = useState('')

  const queryClient = useQueryClient();
  const createMessage = async (newMessage: CreateMessage) =>
    await axios.post(
      config.serverAddress + config.message.message,
      newMessage
    );

  const mutation = useMutation(
    (newMessage: CreateMessage) => createMessage(newMessage),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["getContractMessages"]);
        setMessage('')
      },
    }
  );

  const sendMessage = () => {
    mutation.mutate({senderId: senderId, contractId: contractId, content: message })
  }
  
  return (
    <>
      <Box borderRadius={5}>
        <Box
          display={"flex"}
          bgcolor="#2a2825"
          borderRadius={3}
          flexDirection={"row"}
          justifyContent={"space-evenly"}
          alignItems={"center"}
          
          p={2}
        >
          <TextField
            id="message-textfield"
            multiline
            value={message}
            onChange={(e) => {setMessage(e.target.value)}}
            rows={2}
            sx={{ bgcolor: "white", width: "90%", borderRadius: 3 }}
          />
          <IconButton
            size="large"
            onClick={sendMessage}
            disabled={message.length < 1}
            sx={{ bgcolor: "#FF8000" }}
          >
            <SendIcon sx={{ color: "black" }} />
          </IconButton>
        </Box>
      </Box>
    </>
  );
};

export default MessageBar;
