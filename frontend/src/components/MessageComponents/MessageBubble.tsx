import { Avatar, Card, CardContent, Typography } from "@mui/material";

interface MessageBubbleProps {
  text: string;
  avatar: string;
  ownMessage: boolean;
}

const MessageBubble: React.FC<MessageBubbleProps> = ({
  text,
  avatar,
  ownMessage,
}) => {
  if (ownMessage) {
    return (
      <Card
        sx={{
          maxWidth: 600,
          display: "flex",
          justifyContent: "space-evenly",
          alignItems: "center",
          p: 2,
        }}
      >
        <CardContent>
          <Typography variant="body1" color="initial">
            {text}
          </Typography>
        </CardContent>
        <Avatar src={avatar}></Avatar>
      </Card>
    );
  }
  return (
    <Card
      sx={{
        maxWidth: 600,
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
        p: 2,
      }}
    >
      <Avatar src={avatar}></Avatar>
      <CardContent>
        <Typography variant="body1" color="initial">
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default MessageBubble;
