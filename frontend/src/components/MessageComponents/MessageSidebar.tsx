import {
  Box,
  Divider,
  Typography,
  TextField,
  Button,
  Card,
  CardHeader,
  Avatar,
  IconButton,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { Contract } from "../../types/Contract";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";
import config from "../../../../common/endponts.json";
import { useMutation, useQuery, useQueryClient } from "react-query";
import axios from "axios";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useState } from "react";

type MessageSideBarProps = {
    contract: Contract;
};

const contractUpdateValidationSchema = z.object({
    id: z.string(),
    settledHours: z.number().optional(),
    settledWage: z.number().optional(),
    approvedEmployee: z.coerce.boolean().optional(),
    approvedEmployer: z.coerce.boolean().optional()
});
type ContractUpdateValidationSchema = z.infer<typeof contractUpdateValidationSchema>;

const MessageSidebar: React.FC<MessageSideBarProps> = ({ contract }) => {
  const queryClient = useQueryClient();

  const currentUserId = useRecoilValue(userIdAtom);
  const navigate = useNavigate();
  const isOwner = currentUserId == contract.offer?.userId;
  const ownerName = contract.user.name + " " + contract.user.surname;
  const recieverId = isOwner ? contract.userId : contract?.offer?.userId;
  const [isClosed, setIsClosed] = useState(false);

  const updateContract = async (data: ContractUpdateValidationSchema) =>
    await axios.patch(config.serverAddress + config.contract.contract,
      { ...data },
      { withCredentials: true });

  const getContract = async () => {
      return await axios.get<{ data: Contract }>(
        config.serverAddress + config.contract.contract + "/" + contract.id
      );
    };

  const query = useQuery("getContract", getContract, {
    staleTime: 2000,
    enabled: !isClosed,
    onSuccess: (data) => {
        setValue("settledHours", data.data.data.settledHours);
        setValue("settledWage", data.data.data.settledWage);
        data.data.data.approvedEmployee && data.data.data.approvedEmployer ? setIsClosed(true) : setIsClosed(false);
    }
    });

  const contractMutation = useMutation((data: ContractUpdateValidationSchema) =>
    updateContract(data)
  );

  const handleContractSubmit = async (data: ContractUpdateValidationSchema) => {
    contractMutation.mutate({...data,
        approvedEmployee: isOwner ? true : undefined,
        approvedEmployer: !isOwner ? true : undefined }, {
      onSettled: () => {
        queryClient.invalidateQueries(['getContract']);
        reset();
      },
    });
  };

  const handleRemoveApproval = async () => {
    contractMutation.mutate({
        id: contract.id,
        approvedEmployee: isOwner ? false : undefined,
        approvedEmployer: !isOwner ? false : undefined
    },{
        onSettled: () => {
          queryClient.invalidateQueries(['getContract']);
          reset();
        },
      });
    };

  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
    setValue
  } = useForm<ContractUpdateValidationSchema>({
    resolver: zodResolver(contractUpdateValidationSchema),
    defaultValues: { ...contract } || {},
  });
  return (
    <>
      {query.isSuccess && (
        <Box
          bgcolor={"whitesmoke"}
          display={"flex"}
          flexDirection={"column"}
          borderRight={5}
        >
          <Card>
            <CardHeader
              avatar={
                <IconButton onClick={() => navigate(`/employee/${recieverId}`)}>
                  <Avatar
                    src={
                      config.serverAddress +
                      config.files.getFiles +
                      recieverId +
                      "/type/PIC"
                    }
                  ></Avatar>
                </IconButton>
              }
              title={isOwner ? ownerName : query.data.data.data.user.name + " " + query.data.data.data.user.surname}
              titleTypographyProps={{ fontSize: 20 }}
            />
          </Card>
          <Divider />
          <Typography variant="h5" color="initial" p={2} align="center">
            Contract
          </Typography>

          <Box p={2} display={"flex"} flexDirection={"column"} gap={2}>
            <TextField
              {...register("settledWage", {
                valueAsNumber: true,
              })}
              id="contract-wage"
              label="Wage"
              disabled={isClosed}
              error={Boolean(errors.settledWage)}
              helperText={errors.settledWage && errors.settledWage.message}
              defaultValue={query.data?.data.data.settledWage}
              sx={{ backgroundColor: "white" }}
            />
            <TextField
              {...register("settledHours", {
                valueAsNumber: true,
              })}
              id="contract-duration"
              label="Duration"
              disabled={isClosed}
              error={Boolean(errors.settledHours)}
              helperText={errors.settledHours && errors.settledHours.message}
              defaultValue={query.data?.data.data.settledHours}
              sx={{ backgroundColor: "white" }}
            />
            <Button
              variant="contained"
              disabled={isClosed}
              onClick={handleSubmit(handleContractSubmit)}
            >
            Approve
            </Button>
            <Button
              variant="contained"
              disabled={isClosed}
              onClick={handleRemoveApproval}
            >
            Remove Approval
            </Button>
          </Box>
          {<Box p={2} display={"flex"} flexDirection={"row"} gap={2}>
            <Typography variant="h6" color="initial">
              Employee:
            </Typography>
            {query.data.data.data.approvedEmployee && 
              <Typography variant="h6" color="green">
                Approved
              </Typography> ||
              <Typography variant="h6" color="blue">
                Open
              </Typography>
            }
          </Box> }
          {<Box p={2} display={"flex"} flexDirection={"row"} gap={2}>
            <Typography variant="h6" color="initial">
              Employer:
            </Typography>
            {query.data.data.data.approvedEmployer && 
              <Typography variant="h6" color="green">
                Approved
              </Typography> ||
              <Typography variant="h6" color="blue">
                Open
              </Typography>
            }
          </Box> }
          </Box>
      )}
    </>
  );
};

export default MessageSidebar;
