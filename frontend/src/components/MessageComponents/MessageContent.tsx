import { Box, List, ListItem } from "@mui/material";
import MessageBubble from "./MessageBubble";
import { useEffect, useRef, useState } from "react";
import { Contract } from "../../types/Contract";
import config from "../../../../common/endponts.json";
import { Message } from "../../types/Message";
import axios from "axios";
import { useQuery } from "react-query";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";

type MessageContentProps = {
  contract: Contract;
};

const MessageContent: React.FC<MessageContentProps> = ({ contract }) => {
  const scrollRef = useRef<HTMLLIElement>(null);
  const currentUserId = useRecoilValue(userIdAtom);

  const getMessages = async () => {
    const queryParams =
      "?contractId=" +
      contract.id +
      "&firstParticipantId=" +
      contract.userId +
      "&secondParticipantId=" +
      contract.offer?.userId;
    return await axios.get<{ array: Message[]; count: number }>(
      config.serverAddress + config.message.message + queryParams
    );
  };
  const [list, setList] = useState<Message[]>();

  const query = useQuery(["getContractMessages"], getMessages, {
    staleTime: 1000,
    onSuccess: () => {
      if (query.data?.data.array) setList(query.data?.data.array);
    },
  });

  useEffect(() => {
    if (scrollRef.current) {
      scrollRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [list]);

  return (
    <>
      <Box
        height={"71vh"}
        borderRadius={3}
        p={5}
        bgcolor="#2a2825"
        sx={{ boxShadow: 10 }}
      >
        <List
          style={{
            maxHeight: "100%",
            overflow: "auto",
            display: "flex",
            flexDirection: "column",
          }}
        >
          {query.isSuccess &&
            query.data?.data.array.map((message, index) => (
              <ListItem
                key={index}
                ref={scrollRef}
                sx={{
                  display: "flex",
                  ...(message.senderId === currentUserId
                    ? { justifyContent: "end" }
                    : { justifyContent: "start" }),
                }}
              >
                <MessageBubble
                  text={message.content}
                  avatar={
                    config.serverAddress +
                    config.files.getFiles +
                    message.senderId +
                    "/type/PIC"
                  }
                  ownMessage={message.senderId === currentUserId}
                />
              </ListItem>
            ))}
        </List>
      </Box>
    </>
  );
};

export default MessageContent;
