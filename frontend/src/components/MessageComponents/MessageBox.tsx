import { Box } from "@mui/material";
import MessageBar from "./MessageBar";
import MessageContent from "./MessageContent";
import { Contract } from "../../types/Contract";

type MessageBoxProps = {
  contract: Contract;
}
const MessageBox : React.FC<MessageBoxProps>= ({contract}) => {
  return (
    <>
      <Box
        flexGrow={1}
        display={"flex"}
        flexDirection={"column"}
        bgcolor={"whitesmoke"}
      >
        <Box
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"space-around"}
          p={1}
          gap={1}
        >
          <MessageContent contract={contract}></MessageContent>
          <MessageBar contractId={contract.id}></MessageBar>
        </Box>
      </Box>
    </>
  );
};

export default MessageBox;
