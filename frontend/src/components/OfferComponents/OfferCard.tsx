import {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  CardContent,
  Typography,
  CardActions,
  Button,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import config from "../../../../common/endponts.json";
import OfferDialog from "./OfferDialog";
import { useState } from "react";
import OfferApplicantsDialog from "./OfferApplicantsDialog";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";
import { useMutation, useQueryClient } from "react-query";
import axios, { AxiosError } from "axios";
import { ContractCreateType } from "../../types/Contract";
import { Offering } from "../../types/Offering";

interface OfferCardProps {
  offer: Offering;
}

const OfferCard: React.FC<OfferCardProps> = ({ offer }) => {
  const navigate = useNavigate();
  const [openDialog, setOpenDialog] = useState(false);
  const [openApplicantsDialog, setOpenApplicantsDialog] = useState(false);
  const currentUserId = useRecoilValue(userIdAtom);
  const isOwner = offer.userId === currentUserId;

  const queryClient = useQueryClient();
  const createOffer = async (newContract: ContractCreateType) =>
    await axios.post(
      config.serverAddress + config.contract.contract,
      newContract
    );

  const createMutation = useMutation(
    (newOffer: ContractCreateType) => createOffer(newOffer),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("getAllOffers");
      },
      onError: (error: AxiosError) => {
        console.log(error.response?.data);
      },
    }
  );

  const handleMessage = () => {
    createMutation.mutate(
      { userId: currentUserId, offerId: offer.id },
      {
        onSuccess: (response) => {
          navigate(`/messages/${response.data.id}`);
        },
      }
    );
  };

  return (
    <>
      <Card
        sx={{
          p: 2,
          display: "flex",
          flexDirection: "column",
          backgroundColor: "var(--thirdColor)",
          border: "2px solid black",
          height: "400px",
        }}
      >
        <CardHeader
          avatar={
            <IconButton onClick={() => navigate(`/employee/${offer.userId}`)}>
              <Avatar
                sx={{ height: "100px", width: "100px" }}
                src={
                  config.serverAddress +
                  config.files.getFiles +
                  offer.userId +
                  "/type/PIC"
                }
              />
            </IconButton>
          }
          title={offer.title}
          titleTypographyProps={{ fontSize: 20 }}
        />
        <CardContent sx={{ flexGrow: 1 }}>
          <Typography
            variant="body2"
            color="initial"
            align="justify"
            sx={{ wordBreak: "break-word" }}
          >
            {offer.description}
          </Typography>
        </CardContent>
        <OfferDialog
          open={openDialog}
          onClose={() => setOpenDialog(false)}
          offer={offer}
        ></OfferDialog>
        <OfferApplicantsDialog
          offer={offer}
          onClose={() => setOpenApplicantsDialog(false)}
          open={openApplicantsDialog}
        />
        <CardActions sx={{ display: "flex", justifyContent: "end" }}>
          {isOwner ? (
            <>
              <Button
                variant="contained"
                onClick={() => setOpenApplicantsDialog(true)}
              >
                View Applicants
              </Button>
              <Button variant="contained" onClick={() => setOpenDialog(true)}>
                Edit
              </Button>
            </>
          ) : (
            <Button variant="contained" onClick={handleMessage}>
              Message
            </Button>
          )}
        </CardActions>
      </Card>
    </>
  );
};

export default OfferCard;
