import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { Offering } from "../../types/Offering";
import config from "../../../../common/endponts.json";

interface OfferApplicantsDialogProps {
  open: boolean;
  onClose: () => void;
  offer: Offering;
}

const OfferApplicantsDialog: React.FC<OfferApplicantsDialogProps> = ({
  open,
  onClose,
  offer,
}) => {
  const navigate = useNavigate();
  console.log(offer);

  return (
    <>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>Viewing Applicants</DialogTitle>
        <DialogContent
          sx={{
            maxHeight: 300,
            overflowY: "auto",
            p: 3,
            display: "flex",
            flexDirection: "column",
            gap: 2,
          }}
        >
          {offer.contract && offer.contract.length > 0 ? (
            offer.contract.map((contract, index) => (
              <Box
                key={index}
                sx={{
                  display: "flex",
                  alignItems: "center",
                  width: 350,
                  justifyContent: "space-between",
                  p: 2,
                  border: "1px solid #ccc",
                }}
              >
                <Avatar
                  src={
                    config.serverAddress +
                    config.files.getFiles +
                    contract.user.id +
                    "/type/PIC"
                  }
                  alt={contract.user.name + " " + contract.user.surname}
                  style={{ width: 50, height: 50 }}
                />
                <Typography variant="h6">
                  {contract.user.name + " " + contract.user.surname}
                </Typography>
                <Button
                  variant="contained"
                  onClick={() => navigate(`/messages/${contract.id}`)}
                >
                  Message
                </Button>
              </Box>
            ))
          ) : (
            <Typography variant="h6" align="center" color={"error"}>
              No applicants yet.
            </Typography>
          )}
        </DialogContent>
        <DialogActions>
          <Button color="error" onClick={onClose}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default OfferApplicantsDialog;
