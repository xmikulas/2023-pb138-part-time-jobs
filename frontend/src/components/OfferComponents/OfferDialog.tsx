import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { Box } from "@mui/material";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import axios, { AxiosError } from "axios";
import { useQueryClient, useMutation } from "react-query";
import config from "../../../../common/endponts.json";
import { useRecoilValue } from "recoil";
import { userIdAtom } from "../../recoilAtoms";
import { CreateOfferType } from "../../types/Offering";
import { useState } from "react";
import { ErrorMessage } from "../GeneralComponents/ErrorMessage";

interface OfferDialogProps {
  open: boolean;
  onClose: () => void;
  offer: {
    id: string;
    title: string;
    description: string;
    userId: string;
  } | null;
}

type UpdateOffer = {
  id: string;
  title: string;
  description: string;
  userId: string;
};
const createOfferValidationSchema = z.object({
  title: z.string().nonempty(),
  description: z.string().nonempty(),
});

type CreateOfferValidationSchema = z.infer<typeof createOfferValidationSchema>;

const OfferDialog: React.FC<OfferDialogProps> = ({ open, onClose, offer }) => {
  const editing = offer !== null;
  const userId = useRecoilValue(userIdAtom);
  const [errorMessage, setErrorMessage] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<CreateOfferValidationSchema>({
    resolver: zodResolver(createOfferValidationSchema),
    defaultValues: { ...offer } || {},
  });

  const queryClient = useQueryClient();
  const createOffer = async (newOffer: CreateOfferType) =>
    await axios.post(config.serverAddress + config.offer.offer, newOffer);

  const createMutation = useMutation(
    (newOffer: CreateOfferType) => createOffer(newOffer),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["getAllOffers"]);
        reset();
        onClose();
      },
      onError: (error: AxiosError) => {
        console.log(error.response?.data);
      },
    }
  );

  const editOffer = async (newOffer: UpdateOffer) =>
    await axios.patch(config.serverAddress + config.offer.offer, newOffer);

  const editMutation = useMutation(
    (newOffer: UpdateOffer) => editOffer(newOffer),
    {
      onSuccess: () => {
        reset();
        queryClient.invalidateQueries("getAllOffers");
        onClose();
      },
      onError: (error: AxiosError) => {
        console.log(error.response?.data);
      },
    }
  );

  const onSubmit = async (data: CreateOfferValidationSchema) => {
    setErrorMessage(false);
    if (!editing) {
      createMutation.mutate(
        { ...data, userId: userId },
        {
          onError: () => {
            setErrorMessage(true);
          },
          onSuccess: () => {
            onClose();
            queryClient.invalidateQueries(["getMyOffers" + userId]);
          },
        }
      );
    } else {
      editMutation.mutate({ ...data, id: offer?.id, userId: userId });
    }
  };

  const deleteOffer = async () =>
    await axios.delete(
      config.serverAddress +
        config.offer.offer +
        "/" +
        offer?.id +
        "/user/" +
        userId
    );

  const deleteMutation = useMutation(deleteOffer, {
    onSuccess: () => {
      queryClient.invalidateQueries(["getAllOffers"]);
    },
  });

  const handleDelete = () => {
    deleteMutation.mutate(undefined, {
      onError: () => {
        setErrorMessage(true);
      },
      onSuccess: () => {
        onClose();
        queryClient.invalidateQueries(["getMyOffers" + userId]);
      },
    });
  };

  return (
    <>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{editing ? "Editing offer" : "Create offer"}</DialogTitle>
        <Box
          display={"flex"}
          flexDirection={"column"}
          width={500}
          p={2}
          gap={2}
          // sx={{ p: 2, width: 500 }}
        >
          <TextField
            {...register("title")}
            id="offer-title"
            label="Title"
            fullWidth
            error={Boolean(errors.title)}
            helperText={errors.title && errors.title.message}
            required
          />
          <TextField
            id="offer-description"
            {...register("description")}
            label="Description"
            multiline
            error={Boolean(errors.description)}
            helperText={errors.description && errors.description.message}
            fullWidth
            required
            inputProps={{ maxLength: 380 }}
            rows={5}
          />
        </Box>
        {!editing && errorMessage && (
          <ErrorMessage text="Operation was not succesful" />
        )}
        <DialogActions>
          <Button onClick={onClose} color="error">
            Cancel
          </Button>
          {editing && (
            <Button variant="contained" color="error" onClick={handleDelete}>
              Delete
            </Button>
          )}
          <Button
            onClick={handleSubmit(onSubmit)}
            variant="contained"
            color="primary"
          >
            {editing ? "Edit" : "Create"}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default OfferDialog;
