# 2023 PB138 Part Time Jobs

## (a.k.a. Jobio)


## Getting started

### Backend
Just pull the project and open it in your vscode in the backend directory (`cd backend`), the .devcontainer will take care of installing all the dependencies to make your development *easy like sunday morning!*

All of the usual npm commands are available inside this devcontainer.
One thing u need to do from outside the container is:
```
cd <YOUR_PATH>/2023-pb138-part-time-jobs/.docker
# and run
docker compose up # this will start the postgres db
# u might need to run migration when changing the db schema:
npx prisma migrate dev --name init
```

### Frontend
Move to the right directory `cd frontend`.
Running `npm i && npm run start` is sufficient enough for starting the fe.

### WHOLE PROJECT

The project is fully dockerized, all you need is running `docker compose-up` alternatively with `--build` in the project root to run *postgres db*, *backend* and *frontend* all together.


Prerequisites are having Docker installed tutorial for: [win](https://docs.docker.com/desktop/install/windows-install/), 
[linux](https://docs.docker.com/desktop/install/linux-install/). Do not hesitate to contact me with any issues!

## Description
We started with a brief brainstorm and defined following criterias:

### Initial db schema
![db schema](docs/db_schema.png "Title")

### FIGMA
![figma](docs/figma.png "Title")
*More of our initial figma layout was discussed and oulined [here](https://www.figma.com/file/GjS6zXv9gszgDncB0tLUtt/Znacky-projekt?type=design&node-id=0%3A1&t=mrMez0PY1HqEwoB7-1)*

## Support
Please contact Adam if any issues with Docker

## Roadmap
Docker -> Prisma and DB layer -> Rest -> FE


## API Reference

### User

#### Create new user

```http
  POST /user
```

| Body parameter | Type     | Description                |
| :------------- | :------- | :------------------------- |
| `name` | `string` | **Required**. First name 
| `surname` | `string` | **Required**. Last name |
| `email` | `string` | **Required** **Unique**. User's email |
| `number` | `string` | **Required**. User's phone number |
| `about` | `string` | **Required**. User's "About me" |
| `role` | `string` | **Required** **Enum**. Defines user's role. Might be 'EMPLOYEE' \| 'EMPLOYER' \| 'ADMIN' |
| `expectedHours` | `number` | **Required**. User's expected time spent working in hours |
| `expectedWage` | `number` | **Required**. User's expected wage |
| `password` | `string` | **Required**. User's password |

*Authentication*: anyone

#### Get user

```http
  GET /user/{userId}
```

| URL parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of user to fetch |

*Authentication*: anyone

#### Update user

```http
  PATCH /user
```

| Body parameter | Type     | Description                |
| :------------- | :------- | :------------------------- |
| `id` | `string` | **Required**. ID of user to update |
| `name` | `string` | **Optional**. First name |
| `surname` | `string` | **Optional**. Surname |
| `email` | `string` | **Optional** **Unique**. User's email |
| `number` | `string` | **Optional**. User's phone number |
| `about` | `string` | **Optional**. User's "About me" |
| `role` | `string` | **Optional** **Enum**. Defines user's role. Might be 'EMPLOYEE' \| 'EMPLOYER' \| 'ADMIN' |
| `expectedHours` | `number` | **Optional**. User's expected time spent working in hours |
| `expectedWage` | `number` | **Optional**. User's expected wage |
| `password` | `string` | **Optional**. User's password |

*Authentication*: user being updated or admin

#### Delete user

```http
  DELETE /user/{userId}
```

| URL parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of user to delete |

*Authentication*: user being deleted or admin

#### Fetch users by wage and hours

```http
  POST /users
```

| Body parameter | Type     | Description                       |
| :------------- | :------- | :-------------------------------- |
| `expectedWage`      | `WageInterval` | **Optional**. Interval of user's expected wage |
| `expectedHours` | `HoursInterval` | **Optional**. Interval of user's expected time spent working in hours |

*Authentication*: anyone  
*Note:* Intervals are types consisting of values 'from' and 'to'. Specifically:
```js
type WageInterval = {
    from: number;
    to: number;
};

type HoursInterval = {
    from: number;
    to: number;
};
```

#### Fetch employees who reacted to employers job offer

```http
  GET /user/offer/{employerId}
```

| URL parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `employerId`      | `string` | **Required**. ID of the employer whose job's reactions to fetch |

*Authentication*: employer whose job's reactions to fetch or admin

### Authentication

#### Log in user

```http
  post /login
```

| URL parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `email`      | `string` | **Required**. Email of the user to log in |
| `password`      | `string` | **Required**. Password of the user to log in |

| Status | Description                |
| :----- | :------------------------- |
| `200` | Login successful |
| `400` | Invalid request body |
| `404` | User not found |
| `401` | Wrong password |

*Authentication*: anyone

#### Log out user

```http
  post /logout
```
*No parameters*  
Log out current user.  
| Status | Description                |
| :----- | :------------------------- |
| `200` | Logged out |

*Authentication*: anyone

#### Get current user

```http
  get /current
```
*No parameters*
Returns currently logged in user. The return is a user object.  
| Status | Description                |
| :----- | :------------------------- |
| `200` | Logged out |
| `400` | No user signed in |

*Authentication*: anyone

### Job

#### Create new job

```http
  POST /job
```

| Body parameter | Type     | Description                |
| :------------- | :------- | :------------------------- |
| `employerId` | `string` | **Required**. Employer who posts the job offer
| `title` | `string` | **Required**. Title/position of the job offer |
| `description` | `string` | **Required**. Description of the job |
| `wage` | `number` | **Required**. Job wage |
| `location` | `string` | **Required**. Geographical position of the office. Usually city/state or home office |
| `numOfHours` | `number` | **Required**. Number of hours the job requires |
| `validTill` | `Date` | **Optional**. Date until the job is invalidated |

*Authentication*: anyone


#### Fetch jobs by id

```http
  GET /job/{jobId}
```
| URL parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `employerId`      | `string` | **Required**. ID of the job to fetch |


*Authentication*: anyone

#### Fetch jobs with filters

```http
  POST /jobs
```
| Body parameter | Type     | Description                       |
| :------------- | :------- | :-------------------------------- |
| `wage`      | `WageInterval` | **Optional**. Interval of expected wage |
| `hours` | `HoursInterval` | **Optional**. Interval of expected time spent working in hours |
|`location`| `string` | **Optional**. Location where the job will take place |
|`title`| `string` | **Optional**. Title of the job |
|`description`| `string` | **Optional**. Description of the job |
|`employerId`| `string` | **Optional**. EmployerId of user who provides the job |
|`pagination`| `PaginationOptions` | **Optional**. Options which provide the pagination |

```js
export type PaginationOptions = {
    skip?: number;
    take?: number;
  }
```

*Authentication*: anyone

#### Update job information

```http
  PATCH /job
```

| Body parameter | Type     | Description                       |
| :------------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of the Job |
| `employerId` | `string` | **Required**. employerId of the user whos providing the job |
|`description`| `string` | **Optional**. New description |
|`wage`| `number` | **Optional**. New wage |
|`location`| `string` | **Optional**. Change in location |
|`numOfHour`| `number` | **Optional**. New numOfHour |
|`validTill`| `Date` | **Optional**. New validTill date |

*Authentication*: user whos providing the job


#### Delete single job


```http
  DELETE /job/{id}
```

| URL parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. ID of the job to delete |

*Authentication*: user whos providing the job


### Delete multiple jobs

```http
  DELETE /job
```

| Body parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `employerId`      | `string` | **Required**. ID of the job to delete |

*Authentication*: user whos providing all the jobs


## Signing

### Sign for Job

```http 
  POST /sign
```
| Body parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of the user to sign |
| `jobId`      | `string` | **Required**. ID of the job to be signed to |

*Authentication*: any user with EMPLOYEE role



### Fetch all signs

```http 
  GET /sign/{userId}
```
| Body parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of the user to sign |

*Authentication*: user with EMPLOYEE role which is currently logged in

## Pictures

#### Upload the avatar
```http
  /photo/{userId}
```
| Body parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of the user to assign photo to |

*Authentication*: user which is currently logged in
*IMPORTANT*: if avatar existed, it will be replaced by the new one

curl example:
`curl --location 'http://localhost:3000/photo/ad2ea2c9-d97d-441e-9262-0955d1a25565' --form 'profilePic=@"docs/use_case.png"'`


#### Deletion of avatar
```http
  DELETE /photo/{userId}
```
| Body parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of the user whos avatar will be deleted |

*Authentication*: user which is currently logged in

#### Fetching the avatar
```http
  GET /photo/{userId}
```
| Body parameter | Type     | Description                       |
| :------------ | :------- | :-------------------------------- |
| `userId`      | `string` | **Required**. ID of the user whos avatar will be fetched |

*Authentication*: anyone

## Team Members
| Authors |
| ----------- |
| Adrián Bindas|
| Richard Červený|
|Boris Engler|
|Adam Mikulášek|


## Project status
This is a semester project being heavily developped at FI MUNI Brno as part of the subject Modern Markup Languages (PB138).

@2023